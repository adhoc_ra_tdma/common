#ifndef __SHARED_WORLD_STATE_HPP__
#define __SHARED_WORLD_STATE_HPP__

#include <state_sharing/shared_state.hpp>


#ifdef debugLevel
#define WAS_DEFINED_DEBUG_LEVEL
#define PREV_DEBUG_LEVEL debugLevel
#undef debugLevel
#endif

//#define debugLevel lInfo
#ifndef debugLevel
#define debugLevel lError
#endif
#include "debug_headers/debug.hpp"



// This is actually included!!! Remove what you did to debug header
#include <debug_headers/clear_debug.hpp>
#ifdef WAS_DEFINED_DEBUG_LEVEL
#define debugLevel PREV_DEBUG_LEVEL
#undef WAS_DEFINED_DEBUG_LEVEL
#undef PREV_DEBUG_LEVEL
#include <debug_headers/debug.hpp>
#endif

#endif
