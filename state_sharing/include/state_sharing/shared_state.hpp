#ifndef __SHARED_STATE_HPP__
#define __SHARED_STATE_HPP__

#include <state_sharing/shared_state_properties.hpp>
#include <chrono>
#include <cstdint>

#include <serialisation/serialisationTools.hpp>
#include <dynamic_map_matrix/square_matrix.hpp>

#include <mutex>

#include <cassert>


namespace common
{

namespace state_sharing
{

template<typename dType, class gType=uint8_t, class lType=gType> class SharedState:
		/* extends */
		/* implements */
		public common::serialisation::SerialisableI
{
	private:

	public:
		using connectivity_matrix_t=dynamic_map_matrix::SquareMatrix<dType,gType>;
		using matrix_line_t=dynamic_map_matrix::map<dType,gType>;
		using buffer_state_t=dynamic_map_matrix::map<gType, uint8_t>;
		using sequence_numbers_t=dynamic_map_matrix::map<gType, uint8_t>;
		using line_time_t=dynamic_map_matrix::map<gType, time_point>;



		SharedState(const gType &my_id, const duration &outdate_time);

		SharedState(const SharedState &other);
		SharedState &operator=(const SharedState &other);

		SharedState(SharedState &&other);
		SharedState &operator=(SharedState &&other);

		virtual ~SharedState();


		const connectivity_matrix_t &get_matrix() const;
		const buffer_state_t &get_buffer_states() const;

		bool is_member(gType gid) const;
		bool is_source(gType gid) const;
		bool is_sink(gType gid) const;

		void cleanup();

		void set(const matrix_line_t &data, uint8_t buffer_state);
		void merge(const SharedState &other);


		virtual void serialise( Container &ret) const override;
		virtual void deserialise(Container &ret) override;

		std::string to_string() const;

	protected:

		std::string _to_string() const;

		void add_member(gType gid);
		void remove_member(gType gid);

		void remove_stale_information();
		void remove_dead_members();

	private:

		gType my_id;

		/** Shared data */
		#warning This is being done!!! Check it!!!
		//sequence_numbers_t sequence_numbers;
		connectivity_matrix_t connectivity_matrix;
		buffer_state_t buffer_states;

		/** Control variables */
		line_time_t lineTime;
		duration outdate_time;
		dynamic_map_matrix::map<gType,bool> source;
		dynamic_map_matrix::map<gType,bool> sink;

		mutable std::mutex access_lock;
};


template<typename dType, class gType, class lType>
std::ostream& operator<<(std::ostream& output, const SharedState<dType, gType, lType> &obj);

}
}


#ifdef debugLevel
#define WAS_DEFINED_DEBUG_LEVEL
#define PREV_DEBUG_LEVEL debugLevel
#undef debugLevel
#endif

#define debugLevel lInfo
#ifndef debugLevel
#define debugLevel lError
#endif
#include "debug_headers/debug.hpp"

namespace common
{

namespace state_sharing
{

#define TEMPLATE_SIG template<typename dType, class gType, class lType>
#define CLASS_SIG SharedState<dType, gType, lType>

TEMPLATE_SIG
CLASS_SIG::SharedState(const gType &my_id, const duration &outdate_time):
	my_id(my_id),
	outdate_time(outdate_time)
{
	add_member(my_id);
}

TEMPLATE_SIG
CLASS_SIG::SharedState(const SharedState &other)
{
	std::unique_lock<std::mutex> my_lock(access_lock, std::defer_lock);
	std::unique_lock<std::mutex> other_lock(other.access_lock, std::defer_lock);
	std::lock(my_lock,other_lock);

	my_id=other.my_id;
	connectivity_matrix=other.connectivity_matrix;
	//sequence_numbers=other.sequence_numbers;
	buffer_states=other.buffer_states;

	lineTime=other.lineTime;
	outdate_time=other.outdate_time;
	source=other.source;
	sink=other.sink;

}

TEMPLATE_SIG
CLASS_SIG &CLASS_SIG::operator=(const SharedState &other)
{
	if(this!=&other)
	{
		std::unique_lock<std::mutex> my_lock(access_lock, std::defer_lock);
		std::unique_lock<std::mutex> other_lock(other.access_lock, std::defer_lock);
		std::lock(my_lock,other_lock);

		my_id=other.my_id;
		connectivity_matrix=other.connectivity_matrix;
		//sequence_numbers=other.sequence_numbers;
		buffer_states=other.buffer_states;

		lineTime=other.lineTime;
		outdate_time=other.outdate_time;
		source=other.source;
		sink=other.sink;


	}
	return *this;
}

TEMPLATE_SIG
CLASS_SIG::SharedState(SharedState &&other)
{
	std::unique_lock<std::mutex> my_lock(access_lock, std::defer_lock);
	std::unique_lock<std::mutex> other_lock(other.access_lock, std::defer_lock);
	std::lock(my_lock,other_lock);

	my_id=std::move(other.my_id);
	connectivity_matrix=std::move(other.connectivity_matrix);
	//sequence_numbers=std::move(other.sequence_numbers);
	buffer_states=std::move(other.buffer_states);

	lineTime=std::move(other.lineTime);
	outdate_time=std::move(other.outdate_time);
	source=std::move(other.source);
	sink=std::move(other.sink);
}

TEMPLATE_SIG
CLASS_SIG &CLASS_SIG::operator=(SharedState &&other)
{
	if(this!=&other)
	{
		std::unique_lock<std::mutex> my_lock(access_lock, std::defer_lock);
		std::unique_lock<std::mutex> other_lock(other.access_lock, std::defer_lock);
		std::lock(my_lock,other_lock);

		my_id=std::move(other.my_id);
		connectivity_matrix=std::move(other.connectivity_matrix);
		//sequence_numbers=std::move(other.sequence_numbers);
		buffer_states=std::move(other.buffer_states);

		lineTime=std::move(other.lineTime);
		outdate_time=std::move(other.outdate_time);
		source=std::move(other.source);
		sink=std::move(other.sink);


	}
	return *this;
}

TEMPLATE_SIG
CLASS_SIG::~SharedState()
{
	std::lock_guard<std::mutex> my_lock(access_lock);
}




TEMPLATE_SIG
bool CLASS_SIG::is_member(gType gid) const
{
	std::lock_guard<std::mutex> my_lock(access_lock);
	bool exists=connectivity_matrix.hasKey(gid);
	//unlock
	return exists;
}


TEMPLATE_SIG
bool CLASS_SIG::is_source(gType gid) const
{
	bool result=false;
	try
	{
		std::lock_guard<std::mutex> my_lock(access_lock);
		if(source.hasKey(gid))
		{
			result=source(gid);
		}
	}
	catch(std::exception &e)
	{
		PERROR(std::cerr,"Exception: "<<e.what());
	}
	catch(...)
	{
		PERROR(std::cerr,"Unknown Exception");
	}
	return result;
}

TEMPLATE_SIG
bool CLASS_SIG::is_sink(gType gid) const
{
	bool result=false;
	try
	{
		std::lock_guard<std::mutex> my_lock(access_lock);
		if(source.hasKey(gid))
		{
			result=sink(gid);
		}
	}
	catch(std::exception &e)
	{
		PERROR(std::cerr,"Exception: "<<e.what());
	}
	catch(...)
	{
		PERROR(std::cerr,"Unknown Exception");
	}
	return result;
}


TEMPLATE_SIG
void CLASS_SIG::cleanup()
{
	std::lock_guard<std::mutex> my_lock(access_lock);
	remove_stale_information();
	remove_dead_members();
	//unlock
}

TEMPLATE_SIG
void CLASS_SIG::set(const matrix_line_t &data, uint8_t buffer_state)
{
	std::lock_guard<std::mutex> my_lock(access_lock);

	for(const typename matrix_line_t::value_type &pair:data)
	{
		add_member(pair.first);
		connectivity_matrix(my_id,pair.first)=pair.second;
	}
	buffer_states(my_id)=buffer_state;
	//sequence_numbers(my_id)+=1;
	lineTime(my_id)=clock::now();
}

TEMPLATE_SIG
void CLASS_SIG::merge(const SharedState &other)
{
	std::unique_lock<std::mutex> my_lock(access_lock, std::defer_lock);
	std::unique_lock<std::mutex> other_lock(other.access_lock, std::defer_lock);
	std::lock(my_lock,other_lock);

	PINFO(std::cout,std::endl<<
		  "Merging "<<std::endl<<_to_string()<<
		  "With matrix : "<<std::endl<<other._to_string());
	time_point time = clock::now();

	std::list<uint8_t> other_gids=other.connectivity_matrix.getKeys();

	uint8_t difference=0;
	for(uint8_t &row_gid:other_gids)
	{
		if(row_gid!=my_id)
		{
			add_member(row_gid);

			PDEBUG(std::cout,
				   "Unit:"<<+row_gid<<std::endl<<
				   "    Local sequence number: "<<(int)sequence_numbers(row_gid)<<std::endl<<
				   "    Recvd sequence number: "<<(int)other.sequence_numbers(row_gid));

			/*if(other.sequence_numbers(row_gid)>sequence_numbers(row_gid))
			{
				difference = other.sequence_numbers(row_gid) - sequence_numbers(row_gid);
			}
			// Overflow
			else if(other.sequence_numbers(row_gid)<sequence_numbers(row_gid))
			{
				difference = 0xFF + other.sequence_numbers(row_gid) - sequence_numbers(row_gid) + 1;
			}
			else
			{
				difference=255;
			}

			if(difference<250)
			{*/
			bool my_data_is_invalid=lineTime(row_gid).time_since_epoch()<duration::zero();
			bool other_data_is_invalid=other.lineTime(row_gid).time_since_epoch()<duration::zero();
			bool other_data_is_fresher = other.lineTime(row_gid)>lineTime(row_gid);
			PINFO(std::cout,!other_data_is_invalid<<" * ("<< my_data_is_invalid<<" + "<< other_data_is_fresher<< ") = "<<(!other_data_is_invalid && (my_data_is_invalid || other_data_is_fresher) ));
			if(!other_data_is_invalid && (my_data_is_invalid || other_data_is_fresher) )
			{
				for(uint8_t &column_gid:connectivity_matrix.getKeys())
				{
					connectivity_matrix(row_gid,column_gid)=0;
				}

				for(uint8_t &column_gid:other_gids)
				{
					add_member(column_gid);

					PDEBUG(std::cout,
						   "matrix("<<+row_gid <<","<< +column_gid<<")="<< "received_matrix("<<+row_gid <<","<< +column_gid<<")="<<+received_matrix(row_gid,column_gid));

					connectivity_matrix(row_gid,column_gid)=other.connectivity_matrix(row_gid,column_gid);
					if((row_gid==column_gid) && (connectivity_matrix(row_gid,column_gid)!=0))
					{
						PERROR(std::cout,"ERROR!!!");
					}
				}
				buffer_states(row_gid)=other.buffer_states(row_gid);
				//sequence_numbers(row_gid)=other.sequence_numbers(row_gid);
				//lineTime(row_gid)=time;
				lineTime(row_gid)=other.lineTime(row_gid);
			}
		}

	}
	PINFO(std::cout,std::endl<<
		  "Result "<<std::endl<<_to_string());


}



TEMPLATE_SIG
const typename CLASS_SIG::connectivity_matrix_t &CLASS_SIG::get_matrix() const
{
	std::lock_guard<std::mutex> my_lock(access_lock);
	return connectivity_matrix;
}

TEMPLATE_SIG
const typename CLASS_SIG::buffer_state_t &CLASS_SIG::get_buffer_states() const
{
	std::lock_guard<std::mutex> my_lock(access_lock);
	return buffer_states;
}


TEMPLATE_SIG
void CLASS_SIG::serialise( Container &ret) const
{

	std::lock_guard<std::mutex> my_lock(access_lock);
	common::serialisation::serialise(ret,connectivity_matrix);

	// If we serialise the other structures directly then the global ids will be
	//	transmitted multiple times!!!!
	std::list<gType> g_ids=lineTime.getKeys();
	for(gType &row_id:g_ids)
	{
		PDEBUG(std::cout,"\tpacking "<<+row_id);
		uint16_t age;
		if(std::chrono::duration_cast<std::chrono::milliseconds>(clock::now()-lineTime(row_id))>std::chrono::milliseconds(65000))
		{
			age=65000;
		}
		else
		{
			age=std::chrono::duration_cast<std::chrono::milliseconds>(clock::now()-lineTime(row_id)).count();
		}
//		std::cout<<(clock::now()-lineTime(row_id)).count()<<std::endl;
//		std::cout<<age<<std::endl;
		common::serialisation::serialise(ret,age);
		//common::serialisation::serialise(ret,sequence_numbers(row_id));
		common::serialisation::serialise(ret,buffer_states(row_id));
		PDEBUG(std::cout,"\tDone");
	}
	//unlock
}

TEMPLATE_SIG
void CLASS_SIG::deserialise(Container &ret)
{
	std::lock_guard<std::mutex> my_lock(access_lock);
	common::serialisation::deserialise(ret,connectivity_matrix);

	// If we serialise the other structures directly then the global ids will be
	//	transmitted multiple times!!!!
	std::list<gType> received_members=connectivity_matrix.getKeys();
	for (gType &member:received_members)
	{
		uint8_t sequence_number;
		uint16_t age;
		uint8_t buffer_status;

		//common::serialisation::deserialise(ret,sequence_number);
		common::serialisation::deserialise(ret,age);
		common::serialisation::deserialise(ret,buffer_status);

		//sequence_numbers.emplace(member, sequence_number);
		buffer_states.emplace(member, buffer_status);

		//lineTime.emplace(member, clock::now());
		// 15 is an upperbound at 1Mbps
		lineTime.emplace(member, clock::now()-std::chrono::milliseconds(age+15));
	}

	//unlock


}
TEMPLATE_SIG
std::string CLASS_SIG::to_string() const
{
	std::lock_guard<std::mutex> my_lock(access_lock);
	return _to_string();
}

TEMPLATE_SIG
std::string CLASS_SIG::_to_string() const
{

	std::stringstream output;
	output<<connectivity_matrix.size()<<"x"<<connectivity_matrix.size()<<std::endl;
	std::list<gType> g_ids=connectivity_matrix.getKeys();
	output<<"\t\t\t\t";

	for(gType &row_id:g_ids)
	{
		if(row_id==my_id)
		{
			output<<"("<<(uint64_t)row_id<<")\t";
		}
		else
		{
			output<<"["<<(uint64_t)row_id<<"]\t";
		}
	}
	output<<std::endl<<"Seq. #\tAge"<<std::endl;
	for(gType &row_id:g_ids)
	{
		output/*<<(int)(sequence_numbers(row_id))*/<<"\t";
		int64_t age=(clock::now()-lineTime(row_id)).count();
		if( (std::ceil(std::log10(std::abs(age)))<=10) || (age==0) )
		{
			output<< " ";
			uint64_t digits = (age==0) ? 1 : ( std::ceil(std::log10(std::abs(age))) );
			for(std::size_t i=digits;i<10;i++)
			{
				output<<" ";
			}
			output<< age <<" ns\t";
		}
		else
		{
			output<< ">9999999999" <<" ns\t";
		}
		if(row_id==my_id)
		{
			output<<"("<<(uint64_t)row_id<<")\t";
		}
		else
		{
			output<<"["<<(uint64_t)row_id<<"]\t";
		}
		for(gType &column_id:g_ids)
		{
			output<<(uint64_t)connectivity_matrix(row_id,column_id)<<"\t";
		}
		output<<std::endl;
	}
	std::string ret = output.str();
	return ret;
}

TEMPLATE_SIG
void CLASS_SIG::add_member(gType gid)
{
	if(!connectivity_matrix.hasKey(gid))
	{
		PERROR(std::cout,"Adding member");

		connectivity_matrix.emplace(gid, 0);
		//sequence_numbers.emplace(gid, gType());
		buffer_states.emplace(gid, 255);

		lineTime.emplace(gid, clock::time_point(std::chrono::nanoseconds::min()));
		source.emplace(gid, false);
		sink.emplace(gid, false);

		assert(connectivity_matrix.hasKey(gid));

		PDEBUG(std::cout,"Add member done...");
	}

	PDEBUG(std::cout,"Member "<<gid<<" ahs order "<<lid<<" from "<<connectivity_matrix.size());
	//unlock
}

TEMPLATE_SIG
void CLASS_SIG::remove_member(gType gid)
{
	PINFO(std::cout,"Removing member"<<+gid);
	try
	{
		connectivity_matrix.erase(gid);
		//sequence_numbers.erase(gid);
		buffer_states.erase(gid);
		lineTime.erase(gid);
		PDEBUG(std::cout,"Removing member done...");
	}
	catch(std::out_of_range &e)
	{
		PERROR(std::cerr,"Exception: "<<e.what()<<std::endl<<"Tried to remove an inexistent member: NOT fatal! Continuing");
	}
	//unlock

}

TEMPLATE_SIG
void CLASS_SIG::remove_stale_information()
{
	PINFO(std::cout,"Removing stale info");
	PINFO(std::cout, "Before: "<<std::endl<<_to_string());
	time_point time = clock::now();
	std::list<gType> g_ids=connectivity_matrix.getKeys();
	for(gType &row_gid:g_ids)
	{
		if((time-lineTime(row_gid)) > outdate_time )
		{
			for(gType &column_gid:g_ids)
			{
				connectivity_matrix(row_gid,column_gid) = dType();
			}
			buffer_states(row_gid)=255;
		}
	}
	PINFO(std::cout, "After: "<<std::endl<<_to_string());
}

TEMPLATE_SIG
void CLASS_SIG::remove_dead_members()
{
	PINFO(std::cout,"Removing dead members");
	PINFO(std::cout, "Before: "<<std::endl<<_to_string());

	std::vector<gType> removeList;
	std::list<gType> g_ids=connectivity_matrix.getKeys();
	for(gType &gid1:g_ids)
	{
		sink(gid1)=false;
		source(gid1)=false;
		for(gType &gid2:g_ids)
		{

			if(connectivity_matrix(gid1,gid2)!=0)
			{
				sink(gid1)=true;
			}
			if(connectivity_matrix(gid2,gid1)!=0)
			{
				source(gid1)=true;
			}
		}
		if(!(sink(gid1)||source(gid1)))
		{
			removeList.push_back(gid1);
		}
	}
	for(gType &gid:removeList)
	{
		if(gid!=my_id)
		{
			PERROR(std::cout,"Removing "<<+gid);
			remove_member(gid);
		}
	}
	PINFO(std::cout, "After: "<<std::endl<<_to_string());

}

TEMPLATE_SIG
std::ostream& operator<<(std::ostream& output, const CLASS_SIG &obj)
{

	return output<<obj.to_string();
}

#undef TEMPLATE_SIG
#undef CLASS_SIG
}
}


// This is actually included!!! Remove what you did to debug header
#include <debug_headers/clear_debug.hpp>
#ifdef WAS_DEFINED_DEBUG_LEVEL
#define debugLevel PREV_DEBUG_LEVEL
#undef WAS_DEFINED_DEBUG_LEVEL
#undef PREV_DEBUG_LEVEL
#include <debug_headers/debug.hpp>
#endif



#endif
