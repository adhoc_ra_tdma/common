#ifndef __SHARED_STATE_PROPERTIES_HPP__
#define __SHARED_STATE_PROPERTIES_HPP__

#include <simulatable_clock/simulatable_clock.hpp>
#include <serialisation/serialisationTools.hpp>

namespace common
{

namespace state_sharing
{



using Container=common::serialisation::Container;

using duration_double = std::chrono::duration<double>;
using milliseconds = std::chrono::milliseconds;
using nanoseconds = std::chrono::nanoseconds;
using clock = common::simulation_interface::simulation_clock;
using time_point = clock::time_point;
using duration = clock::duration;


}
}



#endif

