#ifndef __RTVARIABLE_HPP__
#define __RTVARIABLE_HPP__

/*
 * RTVariable is a class that not only keeps the value of the data but also its age
 *		it implements all basic
 *			arithmetic (++, --, +,-,*,/, ~, &, |, ^)
 *			assingment (=, +=, -=, *=, /=, %=, &=, |=, ^=)
 *			print (<<)
 *			comparison functions (<, >, <=, >=, ==, !=)
 *			logical functions (!, ||, &&,)
 *			provides age comparison (isOlder, isNewer)
 *
 */
#include<chrono>
#include<initializer_list>
#include<iostream>
#include <simulatable_clock/simulatable_clock.hpp>


namespace common
{
namespace relative_localisation
{

template<class T> class RTVariable;

template <class T>
std::ostream& operator<<(std::ostream& output, const RTVariable<T> &obj);


template<class T> class RTVariable
{
	public:
		template<typename _Rep, typename _Period>
		RTVariable(T data,std::chrono::duration<_Rep, _Period> age);


		RTVariable();
		RTVariable( const T &data );
		RTVariable( const RTVariable<T> &other );
		RTVariable( RTVariable<T> &&other );


		virtual ~RTVariable();


		// copy
		virtual RTVariable<T> &operator= (const RTVariable<T> &other);
		virtual RTVariable<T> &operator= (const T &other);

		// move
		virtual RTVariable<T> &operator= (RTVariable<T> &&other);
		virtual RTVariable<T> &operator= (T &&other);


		virtual RTVariable<T> operator-() const;
		virtual RTVariable<T> operator+() const;

		virtual RTVariable<T>& operator++();
		virtual RTVariable<T> operator++(int);

		virtual RTVariable<T>& operator--();
		virtual RTVariable<T> operator--(int);

		virtual RTVariable<T> &operator+=(const RTVariable<T> &other); // compound assignment
		virtual RTVariable<T> &operator+=(const T &other); // compound assignment

		virtual RTVariable<T> &operator-=(const RTVariable<T> &other); // compound assignment
		virtual RTVariable<T> &operator-=(const T &other); // compound assignment

		virtual RTVariable<T> &operator/=(const RTVariable<T> &other); // compound assignment
		virtual RTVariable<T> &operator/=(const T &other); // compound assignment

		virtual RTVariable<T> &operator*=(const RTVariable<T> &other); // compound assignment
		virtual RTVariable<T> &operator*=(const T &other); // compound assignment

		virtual RTVariable<T> &operator%=(const RTVariable<T> &other); // compound assignment
		virtual RTVariable<T> &operator%=(const T &other); // compound assignment

		virtual RTVariable<T> &operator&=(const RTVariable<T> &other); // compound assignment
		virtual RTVariable<T> &operator&=(const T &other); // compound assignment

		virtual RTVariable<T> &operator|=(const RTVariable<T> &other); // compound assignment
		virtual RTVariable<T> &operator|=(const T &other); // compound assignment

		virtual RTVariable<T> &operator ^=(const RTVariable<T> &other); // compound assignment
		virtual RTVariable<T> &operator^=(const T &other); // compound assignment

		virtual bool operator !() const;
		virtual bool operator ~() const;


		virtual void setData(const T &data);
		virtual void setData(const T &data, const simulation_interface::simulation_clock::time_point &creation_time);

		virtual T getData() const;
		virtual T &getData();

		virtual simulation_interface::simulation_clock::time_point getCreationTime() const;
		virtual simulation_interface::simulation_clock::time_point &getCreationTime();
		virtual simulation_interface::simulation_clock::duration getAge() const;

		virtual void getData(T &data, simulation_interface::simulation_clock::duration &age) const;

		virtual inline bool isOlder(RTVariable<T> other);
		virtual inline bool isNewer(RTVariable<T> other);




	private:


		friend std::ostream& operator<< <> (std::ostream& output, const RTVariable<T> &obj);

	protected:
		RTVariable(simulation_interface::simulation_clock::time_point time, T data);

		simulation_interface::simulation_clock::time_point creation_time;
		T data;

};




template <class T> inline RTVariable<T> operator +(RTVariable<T> lhs, const RTVariable<T>& rhs);
template <class T> inline RTVariable<T> operator +(RTVariable<T> lhs, const T& rhs);
template <class T, class U> RTVariable<T> operator +(RTVariable<T> lhs, const U& rhs);
template <class T, class U> inline U operator +(U lhs, const RTVariable<T> &rhs);

template <class T> inline RTVariable<T> operator -(RTVariable<T> lhs, const RTVariable<T>& rhs);
template <class T> inline RTVariable<T> operator -(RTVariable<T> lhs, const T& rhs);
template <class T, class U> RTVariable<T> operator -(RTVariable<T> lhs, const U& rhs);
template <class T, class U> inline U operator -(U lhs, const RTVariable<T> &rhs);

template <class T> inline RTVariable<T> operator /(RTVariable<T> lhs, const RTVariable<T>& rhs);
template <class T> inline RTVariable<T> operator /(RTVariable<T> lhs, const T& rhs);
template <class T, class U> RTVariable<T> operator /(RTVariable<T> lhs, const U& rhs);
template <class T, class U> inline U operator /(U lhs, const RTVariable<T> &rhs);

template <class T> inline RTVariable<T> operator %(RTVariable<T> lhs, const RTVariable<T>& rhs);
template <class T> inline RTVariable<T> operator %(RTVariable<T> lhs, const T& rhs);
template <class T, class U> RTVariable<T> operator %(RTVariable<T> lhs, const U& rhs);
template <class T, class U> inline U operator %(U lhs, const RTVariable<T> &rhs);

template <class T> inline RTVariable<T> operator *(RTVariable<T> lhs, const RTVariable<T>& rhs);
template <class T> inline RTVariable<T> operator *(RTVariable<T> lhs, const T& rhs);
template <class T, class U> RTVariable<T> operator *(RTVariable<T> lhs, const U& rhs);
template <class T, class U> inline U operator *(U lhs, const RTVariable<T> &rhs);





template <class T> inline bool operator < (const RTVariable<T> lhs, const RTVariable<T> &rhs);
template <class T> inline bool operator < (const RTVariable<T> lhs, const T &rhs);
template <class T, class U> inline bool operator < (const RTVariable<T> lhs, const U &rhs);
template <class T, class U> inline bool operator < (const U lhs, const RTVariable<T>  &rhs);

template <class T> inline bool operator > (const RTVariable<T> lhs, const RTVariable<T> &rhs);
template <class T> inline bool operator > (const RTVariable<T> lhs, const T &rhs);
template <class T, class U> inline bool operator > (const RTVariable<T> lhs, const U &rhs);
template <class T, class U> inline bool operator > (const U lhs, const RTVariable<T>  &rhs);

template <class T> inline bool operator <= (const RTVariable<T> lhs, const RTVariable<T> &rhs);
template <class T> inline bool operator <= (const RTVariable<T> lhs, const T &rhs);
template <class T, class U> inline bool operator <= (const RTVariable<T> lhs, const U &rhs);
template <class T, class U> inline bool operator <= (const U lhs, const RTVariable<T>  &rhs);

template <class T> inline bool operator >= (const RTVariable<T> lhs, const RTVariable<T> &rhs);
template <class T> inline bool operator >= (const RTVariable<T> lhs, const T &rhs);
template <class T, class U> inline bool operator >= (const RTVariable<T> lhs, const U &rhs);
template <class T, class U> inline bool operator >= (const U lhs, const RTVariable<T>  &rhs);

template <class T> inline bool operator == (const RTVariable<T> lhs, const RTVariable<T> &rhs);
template <class T> inline bool operator == (const RTVariable<T> lhs, const T &rhs);
template <class T, class U> inline bool operator == (const RTVariable<T> lhs, const U &rhs);
template <class T, class U> inline bool operator == (const U lhs, const RTVariable<T>  &rhs);


template <class T> inline bool operator != (const RTVariable<T> lhs, const RTVariable<T> &rhs);
template <class T> inline bool operator != (const RTVariable<T> lhs, const T &rhs);
template <class T, class U> inline bool operator != (const RTVariable<T> lhs, const U &rhs);
template <class T, class U> inline bool operator != (const U lhs, const RTVariable<T>  &rhs);



template <class T> inline bool operator &&(const RTVariable<T> lhs, const RTVariable<T> &rhs);
template <class T> inline bool operator &&(const RTVariable<T> lhs, const T &rhs);
template <class T, class U> inline bool operator &&(const RTVariable<T> lhs, const U &rhs);
template <class T, class U> inline bool operator &&(const U lhs, const RTVariable<T>  &rhs);

template <class T> inline bool operator ||(const RTVariable<T> lhs, const RTVariable<T> &rhs);
template <class T> inline bool operator ||(const RTVariable<T> lhs, const T &rhs);
template <class T, class U> inline bool operator ||(const RTVariable<T> lhs, const U &rhs);
template <class T, class U> inline bool operator ||(const U lhs, const RTVariable<T>  &rhs);

template <class T> inline bool operator &(const RTVariable<T> lhs, const RTVariable<T> &rhs);
template <class T> inline bool operator &(const RTVariable<T> lhs, const T &rhs);
template <class T, class U> inline bool operator &(const RTVariable<T> lhs, const U &rhs);
template <class T, class U> inline bool operator &(const U lhs, const RTVariable<T>  &rhs);
template <class T> inline bool operator |(const RTVariable<T> lhs, const RTVariable<T> &rhs);
template <class T> inline bool operator |(const RTVariable<T> lhs, const T &rhs);
template <class T, class U> inline bool operator |(const RTVariable<T> lhs, const U &rhs);
template <class T, class U> inline bool operator |(const U lhs, const RTVariable<T>  &rhs);


}
}


#include "../../src/realtime_data/rtvariable.ipp"

#endif
