#ifndef __RTVARIABLE_IPP__
#define __RTVARIABLE_IPP__
#include <realtime_data/rtvariable.hpp>

/*
 * RTVariable is a class that not only keeps the value of the data but also its age
 *		it implements all basic
 *			arithmetic (++, --, +,-,*,/, ~, &, |, ^)
 *			assingment (=, +=, -=, *=, /=, %=, &=, |=, ^=)
 *			print (<<)
 *			comparison functions (<, >, <=, >=, ==, !=)
 *			logical functions (!, ||, &&,)
 *			provides age comparison (isOlder, isNewer)
 *	Whenever it is modified age resets
 *
 */
#include<chrono>
#include<initializer_list>
#include<iostream>
namespace common
{
namespace relative_localisation
{
template<class T>
RTVariable<T>::RTVariable():
	RTVariable(simulation_interface::simulation_clock::time_point::min(),T())
{

}


template<class T>
RTVariable<T>::RTVariable(const T &data):
	RTVariable(simulation_interface::simulation_clock::now(),data)
{

}

template<class T>
RTVariable<T>::RTVariable( const RTVariable<T> &other ):
	RTVariable(other.creation_time, other.data)
{
}

template<class T>
RTVariable<T>::RTVariable(RTVariable<T> &&other):
	RTVariable(other.creation_time,other.data)
{
	other.data=T();
	other.creation_time=simulation_interface::simulation_clock::now();
}

template<class T>
template<typename _Rep, typename _Period>
RTVariable<T>::RTVariable(T data,std::chrono::duration<_Rep, _Period> age):
	creation_time(simulation_interface::simulation_clock::now()-std::chrono::duration_cast<simulation_interface::simulation_clock::duration>(age)),
	data(data)
{

}

template<class T>
RTVariable<T>::~RTVariable()
{
}


// copy
template<class T>
RTVariable<T> &RTVariable<T>::operator= (const RTVariable<T> &other)
{
	if (this != &other)
	{
		data=other.data;
		creation_time=other.creation_time;
	}
	return *this;
}

// move
template<class T>
RTVariable<T> &RTVariable<T>::operator= (RTVariable<T> &&other)
{
	if (this != &other)
	{
		data=other.data;
		creation_time=other.creation_time;
		other.data=T();
		other.creation_time=simulation_interface::simulation_clock::time_point::min();
	}
	return *this;

}

// copy
template<class T>
RTVariable<T> &RTVariable<T>::operator= (const T &other)
{
	data=other;
	creation_time=simulation_interface::simulation_clock::now();
	return *this;
}


// move
template<class T>
RTVariable<T> &RTVariable<T>::operator= (T &&other)
{
	data=other;
	creation_time=simulation_interface::simulation_clock::now();
	other=T();
	return *this;
}

template<class T>
RTVariable<T>& RTVariable<T>::operator++()
{
	++data;
	return *this;
}

template<class T>
RTVariable<T> RTVariable<T>::operator-() const
{
	// age tmp=now
	RTVariable<T> tmp(-data);
	return tmp;
}

template<class T>
RTVariable<T> RTVariable<T>::operator+() const
{
	// age does not change
	RTVariable<T> tmp(*this);
	return tmp;
}

template<class T>
RTVariable<T> RTVariable<T>::operator++(int)
{
	RTVariable<T> tmp(*this);
	operator++();
	return tmp;
}

template<class T>
RTVariable<T>& RTVariable<T>::operator--()
{
	--data;
	return *this;
}

template<class T>
RTVariable<T> RTVariable<T>::operator--(int)
{
	RTVariable<T> tmp(*this);
	operator--();
	return tmp;
}

template<class T>
RTVariable<T> &RTVariable<T>::operator+=(const RTVariable<T> &other) // compound assignment
{
	this->data+=other.data;
	return *this; // return the result by reference
}

template<class T>
RTVariable<T> &RTVariable<T>::operator+=(const T &other) // compound assignment
{
	this->data+=other;
	return *this; // return the result by reference
}

template<class T>
RTVariable<T> &RTVariable<T>::operator-=(const RTVariable<T> &other) // compound assignment
{
	this->data-=other.data;
	return *this; // return the result by reference
}

template<class T>
RTVariable<T> &RTVariable<T>::operator-=(const T &other) // compound assignment
{
	this->data-=other;
	return *this; // return the result by reference
}

template<class T>
RTVariable<T> &RTVariable<T>::operator/=(const RTVariable<T> &other) // compound assignment
{
	this->data/=other.data;
	return *this; // return the result by reference
}

template<class T>
RTVariable<T> &RTVariable<T>::operator/=(const T &other) // compound assignment
{
	this->data/=other;
	return *this; // return the result by reference
}

template<class T>
RTVariable<T> &RTVariable<T>::operator*=(const RTVariable<T> &other) // compound assignment
{
	this->data*=other.data;
	return *this; // return the result by reference
}

template<class T>
RTVariable<T> &RTVariable<T>::operator*=(const T &other) // compound assignment
{
	this->data*=other;
	return *this; // return the result by reference
}


template<class T>
RTVariable<T> &RTVariable<T>::operator%=(const RTVariable<T> &other) // compound assignment
{
	this->data%=other.data;
	return *this; // return the result by reference
}

template<class T>
RTVariable<T> &RTVariable<T>::operator%=(const T &other) // compound assignment
{
	this->data%=other;
	return *this; // return the result by reference
}



template<class T>
RTVariable<T> &RTVariable<T>::operator&=(const RTVariable<T> &other) // compound assignment
{
	this->data&=other.data;
	return *this; // return the result by reference
}

template<class T>
RTVariable<T> &RTVariable<T>::operator&=(const T &other) // compound assignment
{
	this->data&=other;
	return *this; // return the result by reference
}


template<class T>
RTVariable<T> &RTVariable<T>::operator|=(const RTVariable<T> &other) // compound assignment
{
	this->data|=other.data;
	return *this; // return the result by reference
}

template<class T>
RTVariable<T> &RTVariable<T>::operator|=(const T &other) // compound assignment
{
	this->data|=other;
	return *this; // return the result by reference
}


template<class T>
RTVariable<T> &RTVariable<T>::operator ^=(const RTVariable<T> &other) // compound assignment
{
	this->data^=other.data;
	return *this; // return the result by reference
}

template<class T>
RTVariable<T> &RTVariable<T>::operator^=(const T &other) // compound assignment
{
	this->data^=other;
	return *this; // return the result by reference
}
template<class T>
bool RTVariable<T>::operator !() const
{
	return !data;
}

template<class T>
bool RTVariable<T>::operator ~() const
{
	return ~(data);
}

//initializer list ???
//Measurement &operator= (std::initializer_list<T> il)
//{
//
//}

template<class T>
void RTVariable<T>::setData(const T &data)
{
	this->data=data;
}

template<class T>
void RTVariable<T>::setData(const T &data, const simulation_interface::simulation_clock::time_point &creation_time)
{
	this->data=data;
	this->creation_time=creation_time;
}

template<class T>
T RTVariable<T>::getData() const
{
	return this->data;
}

template<class T>
T &RTVariable<T>::getData()
{
	return this->data;
}

template<class T>
simulation_interface::simulation_clock::time_point RTVariable<T>::getCreationTime() const
{
	return this->creation_time;
}

template<class T>
simulation_interface::simulation_clock::time_point &RTVariable<T>::getCreationTime()
{
	return this->creation_time;
}


template<class T>
simulation_interface::simulation_clock::duration RTVariable<T>::getAge() const
{
	return (simulation_interface::simulation_clock::now()-this->creation_time);
}




template<class T>
void RTVariable<T>::getData(T &data, simulation_interface::simulation_clock::duration &age) const
{
	data=this->data;
	age=simulation_interface::simulation_clock::now()-this->creation_time;
}

template<class T>
bool RTVariable<T>::isOlder(RTVariable<T> other)
{
	return creation_time<other.creation_time;
}

template<class T>
inline bool RTVariable<T>::isNewer(RTVariable<T> other)
{
	return !isOlder(other);
}

template<class T>
std::ostream& operator<<(std::ostream& output, const RTVariable<T> &obj)
{
	simulation_interface::simulation_clock::duration age=simulation_interface::simulation_clock::now()-obj.creation_time;
	if(obj.creation_time==simulation_interface::simulation_clock::time_point::min())
	{
		output << "Warning: The data in this variable was created in -infinity: ";
	}
	typename std::conditional<
	std::is_same<std::remove_cv<T>,std::remove_cv<unsigned char> >::value||
	std::is_same<std::remove_cv<T>,std::remove_cv<signed char> >::value||
	std::is_same<std::remove_cv<T>,std::remove_cv<char> >::value
	,int32_t, T &>::type data=obj.data;

	output << "Value: "<< data << " age: " << (std::chrono::duration_cast< std::chrono::microseconds >(age).count()) << "us";
	return output;  // for multiple << operators.
}




template<class T>
RTVariable<T>::RTVariable(simulation_interface::simulation_clock::time_point time, T data):
	creation_time(time),
	data(data)
{

}


template <class T> inline RTVariable<T> operator +(RTVariable<T> lhs, const RTVariable<T>& rhs)
{
	return (lhs+=rhs);
}

template <class T> inline RTVariable<T> operator +(RTVariable<T> lhs, const T& rhs)
{
	return (lhs+=rhs);
}

template <class T, class U> RTVariable<T> operator +(RTVariable<T> lhs, const U& rhs)
{
	lhs.setData(lhs.getData()+rhs);
	return lhs;
}

template <class T, class U> inline U operator +(U lhs, const RTVariable<T> &rhs)
{
	return (lhs=lhs+rhs.getData());
}



template <class T> inline RTVariable<T> operator -(RTVariable<T> lhs, const RTVariable<T>& rhs)
{
	return (lhs-=rhs);
}

template <class T> inline RTVariable<T> operator -(RTVariable<T> lhs, const T& rhs)
{
	return (lhs-=rhs);
}

template <class T, class U> RTVariable<T> operator -(RTVariable<T> lhs, const U& rhs)
{
	lhs.setData(lhs.getData()-rhs);
	return lhs;
}

template <class T, class U> inline U operator -(U lhs, const RTVariable<T> &rhs)
{
	return (lhs=lhs-rhs.getData());
}


template <class T> inline RTVariable<T> operator /(RTVariable<T> lhs, const RTVariable<T>& rhs)
{
	return (lhs/=rhs);
}

template <class T> inline RTVariable<T> operator /(RTVariable<T> lhs, const T& rhs)
{
	return (lhs/=rhs);
}

template <class T, class U> RTVariable<T> operator /(RTVariable<T> lhs, const U& rhs)
{
	lhs.setData(lhs.getData()/rhs);
	return lhs;
}

template <class T, class U> inline U operator /(U lhs, const RTVariable<T> &rhs)
{
	return (lhs=lhs/rhs.getData());
}




template <class T> inline RTVariable<T> operator %(RTVariable<T> lhs, const RTVariable<T>& rhs)
{
	return (lhs%=rhs);
}

template <class T> inline RTVariable<T> operator %(RTVariable<T> lhs, const T& rhs)
{
	return (lhs%=rhs);
}

template <class T, class U> RTVariable<T> operator %(RTVariable<T> lhs, const U& rhs)
{
	lhs.setData(lhs.getData()%rhs);
	return lhs;
}

template <class T, class U> inline U operator %(U lhs, const RTVariable<T> &rhs)
{
	return (lhs=lhs%rhs.getData());
}



template <class T> inline RTVariable<T> operator *(RTVariable<T> lhs, const RTVariable<T>& rhs)
{
	return (lhs*=rhs);
}

template <class T> inline RTVariable<T> operator *(RTVariable<T> lhs, const T& rhs)
{
	return (lhs*=rhs);
}

template <class T, class U> RTVariable<T> operator *(RTVariable<T> lhs, const U& rhs)
{
	lhs.setData(lhs.getData()*rhs);
	return lhs;
}

template <class T, class U> inline U operator *(U lhs, const RTVariable<T> &rhs)
{
	return (lhs=lhs*rhs.getData());
}





template <class T> inline bool operator < (const RTVariable<T> lhs, const RTVariable<T> &rhs)
{
	return(lhs.getData()<rhs.getData());
}
template <class T> inline bool operator < (const RTVariable<T> lhs, const T &rhs)
{
	return(lhs.getData()<rhs);
}

template <class T, class U> inline bool operator < (const RTVariable<T> lhs, const U &rhs)
{
	return(lhs.getData()<rhs);
}

template <class T, class U> inline bool operator < (const U lhs, const RTVariable<T>  &rhs)
{
	return(lhs<rhs.getData());
}

template <class T> inline bool operator > (const RTVariable<T> lhs, const RTVariable<T> &rhs)
{
	return rhs < lhs;
}

template <class T> inline bool operator > (const RTVariable<T> lhs, const T &rhs)
{
	return rhs < lhs;
}

template <class T, class U> inline bool operator > (const RTVariable<T> lhs, const U &rhs)
{
	return rhs < lhs;
}

template <class T, class U> inline bool operator > (const U lhs, const RTVariable<T>  &rhs)
{
	return rhs < lhs;
}



template <class T> inline bool operator <= (const RTVariable<T> lhs, const RTVariable<T> &rhs)
{
	return !(lhs > rhs);

}
template <class T> inline bool operator <= (const RTVariable<T> lhs, const T &rhs)
{
	return !(lhs > rhs);
}

template <class T, class U> inline bool operator <= (const RTVariable<T> lhs, const U &rhs)
{
	return !(lhs > rhs);
}

template <class T, class U> inline bool operator <= (const U lhs, const RTVariable<T>  &rhs)
{
	return !(lhs > rhs);
}

template <class T> inline bool operator >= (const RTVariable<T> lhs, const RTVariable<T> &rhs)
{
	return !(lhs < rhs);
}

template <class T> inline bool operator >= (const RTVariable<T> lhs, const T &rhs)
{
	return !(lhs < rhs);
}

template <class T, class U> inline bool operator >= (const RTVariable<T> lhs, const U &rhs)
{
	return !(lhs < rhs);
}

template <class T, class U> inline bool operator >= (const U lhs, const RTVariable<T>  &rhs)
{
	return !(lhs < rhs);
}


template <class T> inline bool operator == (const RTVariable<T> lhs, const RTVariable<T> &rhs)
{
	return (lhs.getData()==rhs.getData());
}
template <class T> inline bool operator == (const RTVariable<T> lhs, const T &rhs)
{
	return (lhs.getData()==rhs);
}

template <class T, class U> inline bool operator == (const RTVariable<T> lhs, const U &rhs)
{
	return (lhs.getData()==rhs);
}

template <class T, class U> inline bool operator == (const U lhs, const RTVariable<T>  &rhs)
{
	return (lhs==rhs.getData());
}


template <class T> inline bool operator != (const RTVariable<T> lhs, const RTVariable<T> &rhs)
{
	return !(lhs == rhs);
}

template <class T> inline bool operator != (const RTVariable<T> lhs, const T &rhs)
{
	return !(lhs == rhs);
}

template <class T, class U> inline bool operator != (const RTVariable<T> lhs, const U &rhs)
{
	return !(lhs == rhs);
}

template <class T, class U> inline bool operator != (const U lhs, const RTVariable<T>  &rhs)
{
	return !(lhs == rhs);
}



template <class T> inline bool operator &&(const RTVariable<T> lhs, const RTVariable<T> &rhs)
{
	return (lhs.getData() && rhs.getData());
}

template <class T> inline bool operator &&(const RTVariable<T> lhs, const T &rhs)
{
	return (lhs.getData() && rhs);
}
template <class T, class U> inline bool operator &&(const RTVariable<T> lhs, const U &rhs)
{
	return (lhs.getData() && rhs);
}
template <class T, class U> inline bool operator &&(const U lhs, const RTVariable<T>  &rhs)
{
	return (lhs && rhs.getData());
}

template <class T> inline bool operator ||(const RTVariable<T> lhs, const RTVariable<T> &rhs)
{
	return (lhs.getData() || rhs.getData());
}

template <class T> inline bool operator ||(const RTVariable<T> lhs, const T &rhs)
{
	return (lhs.getData() || rhs);
}
template <class T, class U> inline bool operator ||(const RTVariable<T> lhs, const U &rhs)
{
	return (lhs.getData() || rhs);
}
template <class T, class U> inline bool operator ||(const U lhs, const RTVariable<T>  &rhs)
{
	return (lhs || rhs.getData());
}

template <class T> inline bool operator &(const RTVariable<T> lhs, const RTVariable<T> &rhs)
{
	return (lhs.getData() & rhs.getData());
}

template <class T> inline bool operator &(const RTVariable<T> lhs, const T &rhs)
{
	return (lhs.getData() & rhs);
}
template <class T, class U> inline bool operator &(const RTVariable<T> lhs, const U &rhs)
{
	return (lhs.getData() & rhs);
}
template <class T, class U> inline bool operator &(const U lhs, const RTVariable<T>  &rhs)
{
	return (lhs & rhs.getData());
}

template <class T> inline bool operator |(const RTVariable<T> lhs, const RTVariable<T> &rhs)
{
	return (lhs.getData() | rhs.getData());
}

template <class T> inline bool operator |(const RTVariable<T> lhs, const T &rhs)
{
	return (lhs.getData() | rhs);
}
template <class T, class U> inline bool operator |(const RTVariable<T> lhs, const U &rhs)
{
	return (lhs.getData() | rhs);
}
template <class T, class U> inline bool operator |(const U lhs, const RTVariable<T>  &rhs)
{
	return (lhs | rhs.getData());
}


}
}

#endif
