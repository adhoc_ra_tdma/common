cmake_minimum_required(VERSION 2.8.3)
project(relative_localisation)

## Find catkin macros and libraries
## if COMPONENTS list like find_package(catkin REQUIRED COMPONENTS xyz)
## is used, also find other catkin packages
find_package(catkin REQUIRED COMPONENTS
	debug_headers
	realtime_data
	data_filters
	rtdbpp
	rtdb_config
	rtdb_datatypes
	simulatable_clock
	simulatable_timer
	serialisation
	cmake_modules
	shared_buffers
	state_sharing
	dynamic_map_matrix)

find_package(Eigen)
#find_package(Boost REQUIRED COMPONENTS system thread)

###################################
## catkin specific configuration ##
###################################
## The catkin_package macro generates cmake config files for your package
## Declare things to be passed to dependent projects
## LIBRARIES: libraries you create in this project that dependent projects also need
## CATKIN_DEPENDS: catkin_packages dependent projects also need
## DEPENDS: system dependencies of this project that dependent projects also need
catkin_package(
  INCLUDE_DIRS include ${Eigen_INCLUDE_DIRS} ${catkin_INCLUDE_DIRS}
  LIBRARIES ${PROJECT_NAME}
  CATKIN_DEPENDS debug_headers
	realtime_data
	data_filters
	rtdbpp
	rtdb_config
	rtdb_datatypes
	simulatable_clock
	simulatable_timer
	serialisation
	cmake_modules
	shared_buffers
	state_sharing
	dynamic_map_matrix
  DEPENDS eigen
)


###########
## Build ##
###########

include_directories(include ${catkin_INCLUDE_DIRS} ${Eigen_INCLUDE_DIRS} ${Boost_INCLUDE_DIRS})

add_library(${PROJECT_NAME}

	include/${PROJECT_NAME}/connectivity_matrix.hpp
	src/${PROJECT_NAME}/connectivity_matrix.ipp

	include/${PROJECT_NAME}/membership_manager.hpp
	src/${PROJECT_NAME}/membership_manager.ipp

	include/${PROJECT_NAME}/relative_localisation.hpp

	include/${PROJECT_NAME}/connectivity_properties.hpp

	src/${PROJECT_NAME}/dummy.cpp

)
SET_TARGET_PROPERTIES(${PROJECT_NAME} PROPERTIES LINKER_LANGUAGE CXX)

add_dependencies(${PROJECT_NAME} ${catkin_EXPORTED_TARGETS})

#############
## Install ##
#############

# all install targets should use catkin DESTINATION variables
# See http://ros.org/doc/groovy/api/catkin/html/adv_user_guide/variables.html

## Mark executables and/or libraries for installation
install(TARGETS
  ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)


 install(DIRECTORY include/${PROJECT_NAME}/
   DESTINATION ${CATKIN_PACKAGE_INCLUDE_DESTINATION}
   FILES_MATCHING PATTERN "*.h" PATTERN "*.hpp"
   PATTERN ".svn" EXCLUDE
 )

install(DIRECTORY include/${PROJECT_NAME}/../../src/${PROJECT_NAME}
   DESTINATION ${CATKIN_PACKAGE_INCLUDE_DESTINATION}
   FILES_MATCHING PATTERN "*.i" PATTERN "*.ipp"
   PATTERN ".svn" EXCLUDE
)
