#ifndef __RELATIVE_LOCALISATION_PROPERTIES_HPP__
#define __RELATIVE_LOCALISATION_PROPERTIES_HPP__

#include <simulatable_clock/simulatable_clock.hpp>
#include <shared_buffers/shared_buffers.hpp>
#include <serialisation/serialisationTools.hpp>
#include <dynamic_map_matrix/square_matrix.hpp>
namespace common
{
namespace relative_localisation
{


using Container=common::serialisation::Container;

using duration_double = std::chrono::duration<double>;
using milliseconds = std::chrono::milliseconds;
using nanoseconds = std::chrono::nanoseconds;
using clock = common::simulation_interface::simulation_clock;
using time_point = clock::time_point;
using duration = clock::duration;
using shared_buffers=common::buffers::shared_buffers;
template<class dType, class gType> using SquareMatrix=dynamic_map_matrix::SquareMatrix<dType, gType>;
template<class dType, class gType> using map=dynamic_map_matrix::map<dType, gType>;

}
}



#endif

