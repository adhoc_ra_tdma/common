#ifndef CONNECTIVITYMATRIX_HPP
#define CONNECTIVITYMATRIX_HPP

/** When a matrix is received (pushPackedState):
 *	1 - We unpack the matrix
 *	2 - We add a sample to the filter
 *	3 - We merge the received matrix with a temporary matrix representing the state of all the rest of the network!
 **/

/** When a matrix is sent (getPackedState):
 *	1 - We clean up our matrix:
 *		a - remove stale information
 *		b -	grab the filtered information and update our line
 *	2 - We clean up the network temporary matrix:
 *		a - remove stale information
 *		b - remove outdated lines (where sequence number is leq than ours
 *		c - remove our line
 *	3 - We merge the temporary matrix with our matrix!
 **/



#include <vector>
#include <chrono>
#include <mutex>

#include <data_filters/mean_window_filter.hpp>

#include <dynamic_map_matrix/square_matrix.hpp>
#include <dynamic_map_matrix/vector.hpp>

#include <functional>

#include <relative_localisation/connectivity_properties.hpp>

#include <simulatable_timer/simulatable_timer.hpp>


#include <state_sharing/shared_state.hpp>

namespace common
{


namespace relative_localisation
{

template<typename dType, class gType=uint8_t, class lType=gType> class ConnectivityMatrix//:
		/* implements */
		//common::serialisation::SerialisableI
{
	public:
		using state_type=common::state_sharing::SharedState<dType,gType,lType>;

		using connectivity_matrix_t=typename state_type::connectivity_matrix_t;
		using connectivity_matrix_line_t=typename state_type::matrix_line_t;
		using buffer_state_t=typename state_type::buffer_state_t;

		ConnectivityMatrix(gType gid, std::chrono::milliseconds outdateTime);

		ConnectivityMatrix(const ConnectivityMatrix &other);
		ConnectivityMatrix &operator=(const ConnectivityMatrix& other);
		ConnectivityMatrix (ConnectivityMatrix &&other);
		ConnectivityMatrix &operator=(ConnectivityMatrix&& other);


		virtual ~ConnectivityMatrix();

		void pushPackedState(gType gid, dType value, std::vector<uint8_t> &data);
		void getPackedState(std::vector<uint8_t> &data);



		//virtual void serialise( Container &ret) const override;
		//virtual void deserialise(Container &ret) override;


		bool is_member(gType gid);
		inline bool getLocalID(gType gid, lType &lid) const;
		inline bool getGlobalID(lType lid, gType &gid) const;
		std::size_t getNumberOfMembers();


		const state_type &get_state() const;
		const connectivity_matrix_t &get_connectivity_matrix() const;
		const connectivity_matrix_t get_spanning_connectivity_matrix() const;
		const buffer_state_t &get_buffer_state() const;
		std::string toString() const;

	protected:

		ConnectivityMatrix()=delete;

		void update();
		dType sampleNode(gType id);
		void updateMyLine();

		const connectivity_matrix_t spanning_tree(const connectivity_matrix_t &matrix) const;

	private:

		gType myID;

		duration outdate_time;

		mutable std::mutex lock;

		/** Shared data */
		state_type state;
		state_type temp_state;
		/** Shared data */

		/** Control variables */
		map< gType, mean_window_filter<dType,1> > samples;
		shared_buffers * buffer=nullptr;
		/** Control variables */



};







template<typename dType, class gType, class lType>
std::ostream& operator<<(std::ostream& output, const ConnectivityMatrix<dType, gType, lType> &obj);



}
}



#include "../../src/relative_localisation/connectivity_matrix.ipp"

#endif // CONNECTIVITYMATRIX_HPP
