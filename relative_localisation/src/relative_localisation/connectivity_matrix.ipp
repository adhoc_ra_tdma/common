#ifndef CONNECTIVITYMATRIX_IPP
#define CONNECTIVITYMATRIX_IPP

#include <relative_localisation/connectivity_matrix.hpp>

#include <cassert>

#ifdef debugLevel
#define WAS_DEFINED_DEBUG_LEVEL
#define PREV_DEBUG_LEVEL debugLevel
#undef debugLevel
#endif

//#define debugLevel lInfo
#ifndef debugLevel
#define debugLevel lError
#endif
#include "debug_headers/debug.hpp"

#include <set>

namespace common
{


namespace relative_localisation
{

#define TEMPLATE_SIG template<typename dType, class gType, class lType>





//#define OUTDATE_TIME boost::posix_time::millisec(1000)


TEMPLATE_SIG
ConnectivityMatrix<dType, gType, lType>::ConnectivityMatrix(gType gid, std::chrono::milliseconds outdateTime):
	myID(gid),
	outdate_time(std::chrono::duration_cast<duration>(outdateTime)),
	state(gid,outdateTime),
	temp_state(gid,outdateTime)
{

	/** Setup buffers */
	buffer=new common::buffers::shared_buffers(myID);
}
TEMPLATE_SIG
ConnectivityMatrix<dType, gType, lType>::ConnectivityMatrix(const ConnectivityMatrix &other):
	myID(other.myID),
	outdate_time(other.outdate_time),
	state(other.state),
	temp_state(other.temp_state),
	samples(other.samples),
	buffer(new common::buffers::shared_buffers(myID))
{
}

TEMPLATE_SIG
ConnectivityMatrix<dType, gType, lType> &ConnectivityMatrix<dType, gType, lType>::operator=(const ConnectivityMatrix &other)
{
	if(this!=&other)
	{
		myID=other.myID;
		outdate_time=other.outdate_time;
		state=other.state;
		temp_state=other.temp_state;
		samples=other.samples;
		buffer=new common::buffers::shared_buffers(myID);
	}
	return *this;
}

TEMPLATE_SIG
ConnectivityMatrix<dType, gType, lType>::ConnectivityMatrix(ConnectivityMatrix &&other):
	myID(std::move(other.myID)),
	outdate_time(std::move(other.outdate_time)),
	state(other.state),
	temp_state(other.temp_state),
	samples(std::move(other.samples)),
	buffer(std::move(other.buffer))
{

}

TEMPLATE_SIG
ConnectivityMatrix<dType, gType, lType> &ConnectivityMatrix<dType, gType, lType>::operator=(ConnectivityMatrix &&other)
{
	if(this!=&other)
	{
		myID=std::move(other.myID);
		outdate_time=std::move(other.outdate_time);
		state=other.state;
		temp_state=other.temp_state;
		samples=std::move(other.samples);
		buffer=std::move(other.buffer);
	}
	return *this;
}

TEMPLATE_SIG
ConnectivityMatrix<dType, gType, lType>::~ConnectivityMatrix()
{
	if(buffer!=nullptr)
	{
		delete(buffer);
		buffer=nullptr;
	}
}

TEMPLATE_SIG
void ConnectivityMatrix<dType, gType, lType>::pushPackedState(gType gid, dType value, std::vector<uint8_t> &data)
{



	PINFO(std::cout,"Received from "<<+gid<<" with "<< +value);
	try
	{
		std::lock_guard<std::mutex> guard(lock);

		common::serialisation::Container c( data.begin(), data.end() );
		state_type received_state(myID,outdate_time);
		common::serialisation::deserialise(c,received_state);
		data.clear();
		data.insert(data.end(),c.begin(),c.end());

		if(gid==myID)
		{
			std::cout<<"ERROR"<<std::endl;
		}

		if(!samples.hasKey(gid))
		{
			samples.emplace(gid,outdate_time);
		}

		mean_window_filter<dType,1> &sample=samples(gid);
		sample.write(value);

		temp_state.merge(received_state);

		PDEBUG(std::cout,"Received done");
	}
	catch(std::exception &e)
	{
		PERROR(std::cerr,"Exception occured: "<<e.what());
	}
	catch(...)
	{
		PERROR(std::cerr,"Exception occured: unknown");
	}

}

TEMPLATE_SIG
void ConnectivityMatrix<dType, gType, lType>::getPackedState(std::vector<uint8_t> &data)
{

	try
	{
		common::serialisation::Container c;
		std::lock_guard<std::mutex> guard(lock);
		update();
		common::serialisation::serialise(c,state);
		data.insert(data.end(),c.begin(),c.end());
	}
	catch(std::exception &e)
	{
		PERROR(std::cerr,"Exception occured: "<<e.what());
	}
	catch(...)
	{
		PERROR(std::cerr,"Exception occured: unknown");
	}


}



TEMPLATE_SIG
bool ConnectivityMatrix<dType, gType, lType>::is_member(gType gid)
{
	bool ret=false;
	try
	{
		std::lock_guard<std::mutex> guard(lock);
		ret = state.is_member(gid);
	}
	catch(std::exception &e)
	{
		PERROR(std::cerr,"Exception occured: "<<e.what());
	}
	catch(...)
	{
		PERROR(std::cerr,"Exception occured: unknown");
	}
	return ret;
}


TEMPLATE_SIG
bool ConnectivityMatrix<dType, gType, lType>::getLocalID(gType gid, lType &lid) const
{
	std::lock_guard<std::mutex> guard(lock);
	return state.get_matrix().getOrder(gid,lid);
}

TEMPLATE_SIG
bool ConnectivityMatrix<dType, gType, lType>::getGlobalID(lType lid, gType &gid) const
{
	std::lock_guard<std::mutex> guard(lock);
	typename std::list<gType>::iterator g_it=state.get_matrix().getKeys().begin();
	if(lid<state.get_matrix().size())
	{
		std::advance(g_it,lid);
		gid=*g_it;
		return true;
	}
	return false;
}

TEMPLATE_SIG
std::size_t ConnectivityMatrix<dType, gType, lType>::getNumberOfMembers()
{
	std::lock_guard<std::mutex> guard(lock);
	return state.get_matrix().size();
}

TEMPLATE_SIG
const typename ConnectivityMatrix<dType, gType, lType>::state_type &ConnectivityMatrix<dType, gType, lType>::get_state() const
{
	std::lock_guard<std::mutex> guard(lock);
	return state;
}

TEMPLATE_SIG
const typename ConnectivityMatrix<dType, gType, lType>::connectivity_matrix_t &
ConnectivityMatrix<dType, gType, lType>::get_connectivity_matrix() const
{
	std::lock_guard<std::mutex> guard(lock);
	return state.get_matrix();
}

TEMPLATE_SIG
const typename ConnectivityMatrix<dType, gType, lType>::connectivity_matrix_t
ConnectivityMatrix<dType, gType, lType>::get_spanning_connectivity_matrix() const
{
	std::lock_guard<std::mutex> guard(lock);
	return spanning_tree(state.get_matrix());
}

TEMPLATE_SIG
const typename ConnectivityMatrix<dType,gType,lType>::buffer_state_t &ConnectivityMatrix<dType, gType, lType>::get_buffer_state() const
{
	std::lock_guard<std::mutex> guard(lock);
	return state.get_buffer_states();
}

TEMPLATE_SIG
std::ostream& operator<<(std::ostream& output, const ConnectivityMatrix<dType, gType, lType> &obj)
{

	return output<<obj.toString();
}


TEMPLATE_SIG
std::string ConnectivityMatrix<dType, gType, lType>::toString() const
{
	std::lock_guard<std::mutex> guard(lock);
	std::stringstream output;
	output<<state;
	std::string ret = output.str();
	return ret;
}







TEMPLATE_SIG
void ConnectivityMatrix<dType, gType, lType>::update()
{
	PINFO(std::cout,"Cleaning up");

	updateMyLine();
	temp_state.cleanup();
//	std::cout<<"state: "<<state<<std::endl;
//	std::cout<<"state: "<<temp_state<<std::endl;
	state=temp_state;

	std::list<gType> g_ids=samples.getKeys();
	for(gType &column_gid:g_ids)
	{
		if(!state.is_member(column_gid))
		{
			samples.erase(column_gid);
		}
	}

//	std::cout<<"state: "<<state<<std::endl;

}

TEMPLATE_SIG
dType ConnectivityMatrix<dType, gType, lType>::sampleNode(gType gid)
{
	duration age;
	dType data;

	mean_window_filter<dType,1> &sample=samples(gid);
	age = sample.read(data);
	duration zero_duration=clock::duration::zero();



	if((age<outdate_time)&&(age>zero_duration))
	{
		return data;
	}
	else
	{
		return dType();
	}

}


TEMPLATE_SIG
void ConnectivityMatrix<dType, gType, lType>::updateMyLine()
{
	PINFO(std::cout,"Updating line");
	time_point time = clock::now();
	lType lid;

	connectivity_matrix_line_t line;

	std::list<gType> g_ids=samples.getKeys();

	for(gType &column_gid:g_ids)
	{
		if(column_gid!=myID)
		{
			line.emplace(column_gid,sampleNode(column_gid));
		}
		else
		{
			line.emplace(column_gid,dType());
		}
	}

	temp_state.set(line,buffer->size());
}

TEMPLATE_SIG
const typename ConnectivityMatrix<dType, gType, lType>::connectivity_matrix_t
ConnectivityMatrix<dType, gType, lType>::spanning_tree(const connectivity_matrix_t &matrix) const
{

	connectivity_matrix_t tree_matrix;

	std::set<gType> nodes_not_in_tree;
	std::set<gType> nodes_to_process;

	for(const gType &id:matrix.getKeys())
	{
		tree_matrix.emplace(id,0);
		nodes_not_in_tree.emplace(id);
	}

	nodes_to_process.emplace(0);

	while(!nodes_to_process.empty())
	{
		std::size_t node_i=*nodes_to_process.begin();

		nodes_not_in_tree.erase(node_i);

		std::set<gType> cpy_nodes_not_in_tree(nodes_not_in_tree);

		for(const gType &id:nodes_not_in_tree)
		{
			std::size_t node_j=id;
			if(matrix(node_i,node_j)!=0)
			{
				tree_matrix(node_i,node_j)=1;
				tree_matrix(node_j,node_i)=1;

				cpy_nodes_not_in_tree.erase(node_j);

				nodes_to_process.emplace(node_j);
			}
		}

		nodes_not_in_tree=cpy_nodes_not_in_tree;

		nodes_to_process.erase(node_i);

	}
	return tree_matrix;

}


}
}

// This is actually included!!! Remove what you did to debug header
#include <debug_headers/clear_debug.hpp>
#ifdef WAS_DEFINED_DEBUG_LEVEL
#define debugLevel PREV_DEBUG_LEVEL
#undef WAS_DEFINED_DEBUG_LEVEL
#undef PREV_DEBUG_LEVEL
#include <debug_headers/debug.hpp>
#endif


#undef TEMPLATE_SIG

#endif
