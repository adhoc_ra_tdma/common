#ifndef BOOSTCOUNTER_HPP
#define BOOSTCOUNTER_HPP

#include <boost/asio/high_resolution_timer.hpp>

namespace common
{
namespace Timer
{
    class Counter
    {
        public:
            Counter();
            void reset();
			void reset(std::chrono::high_resolution_clock::duration offset);
			std::chrono::high_resolution_clock::duration getTime();
        private:
			std::chrono::high_resolution_clock::time_point init;
    };
}
}
#endif // COUNTER_HPP
