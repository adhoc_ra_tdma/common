#ifndef BOOSTTIMER_HPP
#define BOOSTTIMER_HPP

#include <boost_timer/timer_oneshot.hpp>
#include <boost_timer/timer_periodic.hpp>
#include <boost_timer/counter.hpp>
#endif // TIMER_HPP
