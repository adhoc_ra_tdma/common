#ifndef BOOSTTIMERPERIODIC_HPP
#define BOOSTTIMERPERIODIC_HPP

#include <boost_timer/timer_oneshot.hpp>

namespace common
{
namespace Timer
{
class TimerPeriodic : public TimerOneshot
{
	public:
		TimerPeriodic(std::function<void()> func);
		~TimerPeriodic();

		virtual void start(hrDuration time, hrDuration period);
		virtual void start(hrPoint time, hrDuration period);

		hrPoint expiryTime();
		void expiryTime(hrPoint  expTime);
		void delay(hrDuration delay);


	protected:
		hrDuration period;

		typedef TimerOneshot super;
	private:
		TimerPeriodic()=delete;
		std::function<void()> handlerFunction;

		void oneshotCallback();
};
}
}

#endif // TIMERPERIODIC_HPP
