#ifndef BOOSTTIMERONESHOT_HPP
#define BOOSTTIMERONESHOT_HPP

#include <iostream>
#include <boost/asio/high_resolution_timer.hpp>
#include <functional>
#include <mutex>

using boost::asio::io_service;
using hrDuration=std::chrono::high_resolution_clock::duration;
using hrPoint=std::chrono::high_resolution_clock::time_point;
using doubleDuration=std::chrono::duration<double>;

#include<thread>
namespace common
{
namespace Timer
{
class TimerOneshot
{
	public:
		TimerOneshot(std::function<void()> function);
		~TimerOneshot();

		virtual void start(hrDuration time);
		virtual void start(hrPoint expTime);


		virtual hrPoint expiryTime();
		virtual void expiryTime(hrPoint  expTime);
		virtual void delay(hrDuration delay);

		void stop();

	private:
		boost::asio::io_service io;
		boost::asio::io_service::work work;
		std::thread t;
		boost::asio::high_resolution_timer timer;

		std::function<void(const boost::system::error_code&)> callback;
		std::function<void()> handlerFunction;

		TimerOneshot()=delete;

	protected:


		uint32_t timerID;
		static uint32_t lastID;

		std::mutex lock;



};
}
}
#endif // TIMERONESHOT_HPP
