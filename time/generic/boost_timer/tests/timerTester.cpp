#include <boost_timer/timer_oneshot.hpp>
#include <boost_timer/timer_periodic.hpp>
#include <iostream>
#include <future>

#include <csignal>
using Timer=common::Timer::TimerOneshot;
using TimerPeriodic=common::Timer::TimerPeriodic;

std::promise<void> promised_end;

static void signal_catch(int sig)
{
	promised_end.set_value();
}

int main(int argc, char **argv)
{

	if(signal(SIGINT, signal_catch) == SIG_ERR)
	{
		std::cout<<"signal"<<std::endl;
		return -1;
	}

	auto cb1=[]()
	{
		std::cout<<"Timer 1 fired"<<std::endl;
	};

	auto cb2=[]()
	{
		std::cout<<"Timer 2 fired"<<std::endl;
	};
	Timer t(cb1);
	t.start(std::chrono::seconds(1));

	TimerPeriodic tp(cb2);
	tp.start(std::chrono::milliseconds(500),std::chrono::seconds(1));
	promised_end.get_future().get();


	return 0;
}

