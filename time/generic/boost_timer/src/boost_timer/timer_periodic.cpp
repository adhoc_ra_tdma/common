#include <boost_timer/timer_periodic.hpp>

//#define debugLevel lDebug
#ifndef debugLevel
#define debugLevel lError
#endif

#include "debug_headers/debug.hpp"

using namespace common::Timer;


TimerPeriodic::TimerPeriodic(std::function<void()> func):
	TimerOneshot([this](){this->oneshotCallback();}),
	handlerFunction(func),
	period(hrDuration::max())
{
	PINFO(std::cout, "Creating TimerPeriodic("<<timerID<<")");
}

TimerPeriodic::~TimerPeriodic()
{
	PINFO(std::cout, "Destroying TimerPeriodic("<<timerID<<")");
}

void TimerPeriodic::start(hrDuration time, hrDuration perio)
{
	period=perio;
	super::start(time);
	PDEBUG(std::cout, "Starting TimerPeriodic("<<timerID<<") in "<< (std::chrono::duration_cast<doubleDuration>(time).count()) );
}

void TimerPeriodic::start(hrPoint time, hrDuration perio)
{
	period=perio;
	super::start(time);
	PDEBUG(std::cout, "Starting TimerPeriodic("<<timerID<<")");
}

hrPoint TimerPeriodic::expiryTime()
{
	return super::expiryTime();
}

void TimerPeriodic::expiryTime(hrPoint expTime)
{
	return super::expiryTime(expTime);
}

void TimerPeriodic::delay(hrDuration delay)
{
	return super::delay(delay);
}

void TimerPeriodic::oneshotCallback()
{
		PDEBUG(std::cout,"TimerPeriodicFired("<<timerID<<")");
		super::start(expiryTime() + period);
		handlerFunction();
}

