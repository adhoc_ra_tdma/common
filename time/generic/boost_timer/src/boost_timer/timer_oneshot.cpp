#include "boost_timer/timer_oneshot.hpp"
#include <future>

//#define debugLevel lDebug
#ifndef debugLevel
#define debugLevel lError
#endif

#include "debug_headers/debug.hpp"

using namespace common::Timer;

TimerOneshot::TimerOneshot(std::function<void()> function):
	work(io),
	t([this](){this->io.run();}),
	handlerFunction(function),
	timer(io)
{
	timerID=lastID++;
	callback=[this](const boost::system::error_code &ec)
	{
		if(ec!=boost::asio::error::operation_aborted)
		{
			PINFO(std::cout,"TimerFired("<<timerID<<")");
			handlerFunction();
		}
	};
	PINFO(std::cout, "Creating TimerOneshot("<<timerID<<")");
}


void TimerOneshot::start(hrDuration time)
{
	lock.lock();
	timer.expires_from_now(time);
	timer.async_wait(callback);
	lock.unlock();
	PDEBUG(std::cout, "Starting TimerOneshot("<<timerID<<")");
}

void TimerOneshot::start(hrPoint time)
{
	lock.lock();
	timer.expires_at(time);
	timer.async_wait(callback);
	lock.unlock();
	PDEBUG(std::cout, "Starting TimerOneshot("<<timerID<<")");
}

hrPoint TimerOneshot::expiryTime()
{
	lock.lock();
	hrPoint time=timer.expires_at();
	lock.unlock();
	return time;
}

void TimerOneshot::expiryTime(hrPoint expTime)
{
	lock.lock();
	timer.expires_at(expTime);
	timer.async_wait(callback);
	lock.unlock();
}

void TimerOneshot::delay(hrDuration delay)
{
	lock.lock();
	timer.expires_at(timer.expires_at()+delay);
	timer.async_wait(callback);
	lock.unlock();
}

void TimerOneshot::stop()
{
	lock.lock();
	timer.cancel();
	lock.unlock();
	PDEBUG(std::cout, "Stopped TimerOneshot("<<timerID<<")");
}

TimerOneshot::~TimerOneshot()
{
	PINFO(std::cout,"Destruct TimerOneshot("<<timerID<<")");
	lock.lock();
	//timer->cancel();
	io.stop();
	PDEBUG(std::cout, "io stop");
	while(!io.stopped()) usleep(1);
	t.join();
	PDEBUG(std::cout, "t joined");
	lock.unlock();
	PDEBUG(std::cout,"Destruct Done TimerOneshot("<<timerID<<")");
}



uint32_t TimerOneshot::lastID=0;
