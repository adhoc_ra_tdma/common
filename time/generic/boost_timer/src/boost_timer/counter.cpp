#include <boost_timer/counter.hpp>
#ifndef debugLevel
		#define debugLevel lError
#endif
#include "debug_headers/debug.hpp"

using namespace common::Timer;

Counter::Counter():
	init(std::chrono::high_resolution_clock::now())
{
}
void Counter::reset()
{
	init=std::chrono::high_resolution_clock::now();
}

void Counter::reset(std::chrono::high_resolution_clock::duration offset)
{
	init=std::chrono::high_resolution_clock::now()+offset;
}

std::chrono::high_resolution_clock::duration Counter::getTime()
{
	return std::chrono::high_resolution_clock::now()-init;
}
