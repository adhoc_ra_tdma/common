#ifndef __INTERFACE_TIMER_HPP__
#define __INTERFACE_TIMER_HPP__

#include <functional>
#include <cstdint>
#include <chrono>

namespace common
{
namespace time
{
namespace interfaces
{

using duration_t=std::chrono::nanoseconds;
class TimerI
{

	public:
		virtual ~TimerI()=default;
		virtual void start(std::function<void()> cb, duration_t timeout_ns, duration_t period_ns=duration_t::zero())=0;
		virtual void restart(std::function<void()> cb, duration_t timeout_ns, duration_t period_ns=duration_t::zero())=0;
		virtual void stop()=0;

		virtual duration_t absoluteExpiryTime() const = 0;
		virtual void delay(duration_t delay) = 0;

	protected:


	private:
};

class CounterI
{

	public:
		virtual ~CounterI()=default;
		virtual void reset()=0;
		virtual void reset(duration_t offset)=0;
		virtual duration_t get_time()=0;

	protected:


	private:
};



}
}
}
#endif





