#include <linux_timer/timer.hpp>
#include <iostream>
#include <cassert>

#ifndef debugLevel
#define debugLevel lError
#endif
#include <debug_headers/debug.hpp>

namespace common
{
namespace time
{
namespace linux_implementation
{
namespace timer
{

bool StateMachine::isStopped()
{
	return state==TIMER_STOPPED;
}
bool StateMachine::isRunning()
{
	return state==TIMER_RUNNING;
}
bool StateMachine::isFired()
{
	return state==TIMER_FIRED;
}

bool StateMachine::start()
{
	uint32_t current_status = state.load();
	if(current_status==TIMER_STOPPED)
	{
		if(state.compare_exchange_strong(current_status,TIMER_RUNNING))
		{
			PDEBUG(std::cout, "Starting state machine");
			return true;
		}
	}
	PDEBUG(std::cout, "Failed starting state machine");
	return false;
}

bool StateMachine::stop()
{
	uint32_t current_status = state.load();
	if(current_status!=TIMER_STOPPED)
	{
		if(state.compare_exchange_strong(current_status,TIMER_STOPPED))
		{
			PDEBUG(std::cout, "Stopping state machine");
			return true;
		}
	}
	PDEBUG(std::cout, "Failed stopping state machine");
	return false;
}

bool StateMachine::fire()
{
	uint32_t current_status = state.load();
	if(current_status==TIMER_RUNNING)
	{
		if(state.compare_exchange_strong(current_status,TIMER_FIRED))
		{
			PDEBUG(std::cout, "Firing state machine");
			return true;
		}
	}
	PDEBUG(std::cout, "Failed firing state machine");
	return false;
}

bool StateMachine::rearm()
{
	uint32_t current_status = state.load();
	if(current_status==TIMER_FIRED)
	{
		if(state.compare_exchange_strong(current_status,TIMER_RUNNING))
		{
			PDEBUG(std::cout, "Rearming state machine");
			return true;
		}
	}
	PDEBUG(std::cout, "Failed rearming state machine");
	return false;
}

StateMachine::StateMachine()
{
	state.store(TIMER_STOPPED);
}


Timer::Timer()
{
	PDEBUG(std::cout,"Constructing Timer...");
	createTimer();
	PDEBUG(std::cout,"Timer constructed");
}

Timer::~Timer() noexcept
{
	PDEBUG(std::cout,"Destroying Timer...");
	stop();
	PDEBUG(std::cout,"Timer Destroyed");
}

void Timer::createTimer()
{
	PDEBUG(std::cout,"Creating timer...");

	signalAction.sa_flags = SA_SIGINFO;
	signalAction.sa_sigaction =[](int signal, siginfo_t *info, void *)
	{
		if(signal==SIGALRM)
		{
			if(info->si_code==SI_TIMER)
			{
				Timer *timer=(reinterpret_cast< Timer * >(info->si_value.sival_ptr));

				if(timer->stateMachine.isRunning())
				{
					timer->lock.unlock();
				}

			}


		}
	};


	sigemptyset(&signalAction.sa_mask);
	if (sigaction(SIGALRM, &signalAction, nullptr) == -1)
	{
		PERROR(std::cerr,"sigaction");
		assert(false);
	}

	/* Set and enable alarm */
	timerEvent.sigev_notify = SIGEV_SIGNAL;
	timerEvent.sigev_signo = SIGALRM;
	timerEvent.sigev_value.sival_ptr = reinterpret_cast<void*>(this);
	if(timer_create(CLOCK_REALTIME, &timerEvent, &timerID)<0)
	{
		PERROR(std::cerr,"timer create");
	}

	PDEBUG(std::cout,"Timer created");

}
//bool done=false;
int Timer::startTimer( uint64_t firstFire, uint64_t interval )
{
	PDEBUG(std::cout,"Starting timer");


	//std::this_thread::yield();
	itimerspec timerSpecification;

	timerSpecification.it_value.tv_sec = firstFire/((uint64_t)1E9);
	timerSpecification.it_value.tv_nsec = firstFire%((uint64_t)1E9);
	timerSpecification.it_interval.tv_sec = interval/((uint64_t)1E9);
	timerSpecification.it_interval.tv_nsec = interval%((uint64_t)1E9);
	timer_settime(timerID, 0, &timerSpecification, NULL);


	return(0);
}

int Timer::stopTimer()
{
	itimerspec timerSpecification;
	timerSpecification.it_value.tv_sec = 0;
	timerSpecification.it_value.tv_nsec = 0;
	timerSpecification.it_interval.tv_sec = 0;
	timerSpecification.it_interval.tv_nsec = 0;
	timer_settime(timerID, 0, &timerSpecification, NULL);
	return 0;
}

void Timer::timerCallback()
{
	PDEBUG(std::cout,"Fired");

	do
	{
		lock.lock();
		//std::cout<<std::this_thread::get_id()<<std::endl;

		if(stateMachine.fire())
		{
			if(period>duration_t::zero())
			{
				if(!stateMachine.rearm())
				{
					PERROR(std::cout,"Rearm failed!!!");
				}
			}
			else
			{
				if(stateMachine.stop())
				{
					stopTimer();
				}
				else
				{
					PERROR(std::cout,"Stop failed!!!");
				}
			}
			std::thread t([this](){this->userCallback();}); // this call can change the status
			t.detach();
		}
	} while(!stateMachine.isStopped());
	lock.unlock();
	PDEBUG(std::cout,"Exited timer thread");
}


void Timer::start(std::function<void()> cb, duration_t timeout_ns, duration_t period_ns)
{
	try{
		if(stateMachine.start())
		{
			if(timeout_ns==duration_t::zero()) timeout_ns=duration_t(1); //Ensure it fires, there is a stop method!

			period=period_ns;

			userCallback=cb;


			lock.lock();
			auto tCallback = [this]()
			{
				this->timerCallback();
			};
			if(t.joinable())
			{
				t.join();
			}
			t=std::thread(tCallback);

			startTimer(timeout_ns.count(), period_ns.count());
		}
	}
	catch(std::exception &e)
	{
		PINFO(std::cout,e.what());
	}
}

void Timer::restart(std::function<void ()> cb, duration_t timeout_ns, duration_t period_ns)
{
	try{
		Timer::stop();
		Timer::start(cb,timeout_ns,period_ns);
	}
	catch(std::exception &e)
	{
		PINFO(std::cout,e.what());
	}
}

void Timer::stop()
{
	try
	{
		if(stateMachine.stop())
		{
			stopTimer();
			lock.unlock();
			t.join();
		}
	}
	catch(std::exception &e)
	{
		PINFO(std::cout,e.what());
	}

}


duration_t Timer::absoluteExpiryTime() const
{
	itimerspec timerSpecification;
	duration_t now(clock_t::now().time_since_epoch());
	timer_gettime(timerID, &timerSpecification);
	duration_t time_to_expiration=std::chrono::seconds(timerSpecification.it_value.tv_sec)+std::chrono::nanoseconds(timerSpecification.it_value.tv_nsec);
	return now+time_to_expiration;
}

void Timer::delay(duration_t delay)
{

	itimerspec timerSpecification;
	timer_gettime(timerID, &timerSpecification);
	duration_t time_to_expiration=std::chrono::seconds(timerSpecification.it_value.tv_sec)+std::chrono::nanoseconds(timerSpecification.it_value.tv_nsec);
	if (!(time_to_expiration==duration_t::zero()))
	{
		timerSpecification.it_value.tv_nsec += delay.count();
		timerSpecification.it_value.tv_sec += (timerSpecification.it_value.tv_nsec)/((uint64_t)1E9);
		timerSpecification.it_value.tv_nsec = (timerSpecification.it_value.tv_nsec)%((uint64_t)1E9);
		timer_settime(timerID, 0, &timerSpecification, NULL);

	}
}


}
}
}
}
