
#include <linux_timer/counter.hpp>

namespace common
{
namespace time
{
namespace linux_implementation
{
namespace timer
{
Counter::Counter():
	init(clock_t::now())
{

}

Counter::Counter(const Counter &other):
	init(other.init)
{

}


Counter &Counter::operator=(const Counter &other)
{
	if(this!=&other)
	{
		this->init.operator=(other.init);
	}
	return *this;
}


Counter::Counter(Counter &&other):
	init(other.init)
{
}


Counter &Counter::operator=(Counter &&other)
{
	if(this!=&other)
	{
		this->init.operator=(other.init);
	}
	return *this;
}


void Counter::reset()
{
	init=clock_t::now();
}


void Counter::reset(duration_t offset)
{
	init=clock_t::now()+offset;
}


duration_t Counter::get_time()
{
	return clock_t::now()-init;
}

}
}
}
}
