#ifndef debugLevel
#define debugLevel lDebug
#endif
#include <debug_headers/debug.hpp>
#include <linux_timer/timer.hpp>
#include <iostream>

#include<future>
#include <signal.h>
#include <unistd.h>
std::promise<void> promised_end;

static void signal_catch(int sig)
{
	promised_end.set_value();
}


int main(int argc, char **argv)
{
	std::future<void> end = promised_end.get_future();
	if(signal(SIGINT, signal_catch) == SIG_ERR)
	{
		std::cout<<"error: signal"<<std::endl;
		return -1;
	}

	common::time::linux_implementation::timer::TimerI *t=new common::time::linux_implementation::timer::Timer;
	common::time::linux_implementation::timer::Timer *t2=new common::time::linux_implementation::timer::Timer;
	common::time::linux_implementation::timer::Timer *t3=new common::time::linux_implementation::timer::Timer;

	try
	{
		auto cb =[]()
		{
			std::cout<<"Timer 1 Oneshot!!"<<std::endl;
			//promised_end.set_value();
		};

		auto cb22=[]()
		{
			std::cout<<"Restarted timer 2!!"<<std::endl;
		};

		auto cb21=[&t2,&cb22]()
		{
			std::cout<<"Restarting timer 2!!"<<std::endl;
			t2->restart(cb22,std::chrono::nanoseconds(50000000),std::chrono::nanoseconds(50000000));
		};

		auto cb3=[](){std::cout<<"Periodic Timer!!"<<std::endl;};


		t->start(cb,std::chrono::nanoseconds(3000000000));
		t2->start(cb21,std::chrono::nanoseconds(1500000000),std::chrono::nanoseconds(1500000000));
		t3->start(cb3,std::chrono::nanoseconds(1000000000),std::chrono::nanoseconds(1000000000));

		for(int i=0;i<2;i++)
			sleep(3);


		std::cout<<"Done"<<std::endl;
		//t2.start(cb2,1);
		end.get();

		std::cout<<std::endl<<"Exiting cleanly"<<std::endl;

		delete(t);
		delete(t2);
		delete(t3);
	}
	catch(std::exception &e)
	{
		PDEBUG(std::cout,e.what());
	}
	return 0;

}
