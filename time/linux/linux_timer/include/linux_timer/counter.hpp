#ifndef __LINUX_COUNTER_HPP__
#define __LINUX_COUNTER_HPP__

#include <time_interfaces/timerI.hpp>

#include <ctime>
#include <csignal>
#include <ctime>
#include <cstring>
#include <cerrno>

#include <sys/time.h>

#include <thread>
#include <functional>
#include <mutex>
#include <atomic>


namespace common
{
namespace time
{
namespace linux_implementation
{
namespace timer
{

using common::time::interfaces::TimerI;
using common::time::interfaces::CounterI;
using duration_t=common::time::interfaces::duration_t;
using clock_t=std::chrono::high_resolution_clock;
using timepoint_t=clock_t::time_point;


class Counter:public CounterI
{
	public:


		Counter();

		Counter(const Counter &other);
		Counter &operator=(const Counter &other);

		Counter(Counter &&other);
		Counter &operator=(Counter &&other);

		virtual ~Counter()=default;

		void reset();
		void reset(duration_t offset);
		duration_t get_time();
	private:
		timepoint_t init;


};


}
}
}
}
#endif



