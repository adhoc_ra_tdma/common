#ifndef __LINUX_TIMER_HPP__
#define __LINUX_TIMER_HPP__

#include <time_interfaces/timerI.hpp>

#include <ctime>
#include <csignal>
#include <ctime>
#include <cstring>
#include <cerrno>

#include <sys/time.h>

#include <thread>
#include <functional>
#include <mutex>
#include <atomic>


namespace common
{
namespace time
{
namespace linux_implementation
{
namespace timer
{

class StateMachine
{
	public:

		StateMachine();

		enum states
		{
			TIMER_STOPPED=0,
			TIMER_RUNNING,
			TIMER_FIRED
		};

		std::atomic<uint32_t> state;

		bool isStopped();
		bool isRunning();
		bool isFired();

		bool start();
		bool stop();
		bool fire();
		bool rearm();
};

using common::time::interfaces::TimerI;
using duration_t=common::time::interfaces::duration_t;
using clock_t=std::chrono::high_resolution_clock;
using timepoint_t=clock_t::time_point;

class Timer:public TimerI
{

	public:


		Timer();
        	virtual ~Timer() noexcept;

		virtual void start(std::function<void()> cb, duration_t timeout_ns, duration_t period_ns=duration_t::zero());
		virtual void restart(std::function<void()> cb, duration_t timeout_ns, duration_t period_ns=duration_t::zero());
		virtual void stop();

		virtual duration_t absoluteExpiryTime() const;
		virtual void delay(duration_t delay);

	protected:
		//struct timespec my_wait;

		StateMachine stateMachine;

		struct itimerval it;

		void createTimer();

		int startTimer( uint64_t firstFire, uint64_t interval );
		int stopTimer( );

		sigevent timerEvent;
		struct sigaction signalAction; //there is a function with the same name
		timer_t timerID;

		std::mutex lock;

		std::function<void()> userCallback;
		std::thread t;


		duration_t period;
	private:
		void timerCallback();


};
}
}
}
}

#endif



