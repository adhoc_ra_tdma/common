#ifndef __SHARED_BUFFERS_HPP__
#define __SHARED_BUFFERS_HPP__

#include <sys/sem.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <array>

namespace common
{
namespace buffers
{



class shared_buffers
{
	private:
		constexpr static std::size_t array_size=256;
		constexpr static std::size_t mtu=1500;
	public:

		using data_type=std::array<uint8_t,mtu>;

		struct message_t
		{
			public:
				data_type msg;
				std::size_t size;
				uint8_t destination;
			private:
				uint8_t retrieval_count=0;
				friend class shared_buffers;
		};


		shared_buffers()=delete;
		shared_buffers(std::size_t node_id);

		shared_buffers(const shared_buffers &other);
		shared_buffers &operator=(const shared_buffers &other);

		shared_buffers(shared_buffers &&other);
		shared_buffers &operator=(shared_buffers &&other);

		virtual ~shared_buffers();


		bool push_message(const message_t &msg);
		bool get_message(message_t &msg);
		bool peek_message(message_t &msg);

		std::size_t size() const;
		std::size_t max_size() const;

	private:


		bool pop_message();



		struct shared_data_t
		{
				std::size_t insert_point;
				std::size_t removal_point;
				std::array<message_t, array_size> buffer;
				std::size_t size;
				mutable std::size_t max_size;
				std::size_t counter;
		};

		shared_data_t *data;

		int data_shmid;
		int buffer_shmid;
		int semid;
		const uint32_t semkey=0x0077DEAD;
		void acquire() const;
		void release() const;

		std::size_t id;



};

}
}


#endif
