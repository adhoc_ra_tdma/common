#include <shared_buffers/shared_buffers.hpp>
#include <cassert>

#include <debug_headers/clear_debug.hpp>

#define debugLevel lDebug
#ifndef debugLevel
#define debugLevel lLog
#endif

#include <debug_headers/debug.hpp>




namespace common
{
namespace buffers
{


shared_buffers::shared_buffers(std::size_t node_id)
{
	this->id=node_id;
	/* try create the semaphore */
	assert((semid = semget(semkey+id, 1, 0666 | IPC_CREAT | IPC_EXCL)) != -1  ||  errno == EEXIST);

	if (semid != -1) // I'm the creator
	{
		fprintf(stderr, "I'm the creator\n");

		/* create the shared memory */
		if((data_shmid = shmget(semkey+id, sizeof(shared_data_t), 0666 | IPC_CREAT)) == -1)
		{
			assert(0);
		}

		/* attach shared memory */
		if((data = (shared_data_t *)shmat(data_shmid, 0, 0)) == NULL)
		{
			assert(0);
		}

		/* create the shared memory */
		/*if((buffer_shmid = shmget(semkey+id+1, sizeof(std::array<data_type, array_size>), 0666 | IPC_CREAT)) == -1)
		{
			// detach memory
			shmdt(data);

			/// mark memory to be destroyed
			shmctl(data_shmid, IPC_RMID, NULL);
			assert(0);
		}*/

		// attach shared memory
		/*if((data->buffer = (std::array<data_type, array_size> *)shmat(buffer_shmid, 0, 0)) == NULL)
		{
			// detach memory
			shmdt(data);

			// mark memory to be destroyed
			shmctl(buffer_shmid, IPC_RMID, NULL);
			// mark memory to be destroyed
			shmctl(data_shmid, IPC_RMID, NULL);
			assert(0);
		}*/
		data->counter=1;
		// I want it to fit in 8bits
		data->max_size=255;
		/*data->insert_point=data->buffer->begin();
		data->removal_point=data->buffer->begin();*/
	}
	else // I'm not the creator
	{
		fprintf(stderr, "I'm not the creator\n");

		/* get semaphore id */
		assert((semid = semget(semkey+id, 0, 0)) != -1);

		/* gain mutual exclusion access */
		acquire();

		/* get shared memory */
		if((data_shmid = shmget(semkey+id, 0, 0)) == -1)
		{
			fprintf(stderr, "errno = %d:%s\n",errno,strerror(errno));
			assert(0);
		}

		/* attach shared memory */
		if((data = (shared_data_t *)shmat(data_shmid, 0, 0)) == NULL)
		{
			assert(0);
		}

		/* create the shared memory */
		/*if((buffer_shmid = shmget(semkey+id+1, 0, 0)) == -1)
		{
			// detach memory
			shmdt(data);

			// mark memory to be destroyed
			shmctl(data_shmid, IPC_RMID, NULL);
			assert(0);
		}*/

		/*// attach shared memory
		if((data->buffer = (std::array<data_type, array_size> *)shmat(buffer_shmid, 0, 0)) == NULL)
		{
			// detach memory
			shmdt(data);

			// mark memory to be destroyed
			shmctl(buffer_shmid, IPC_RMID, NULL);
			// mark memory to be destroyed
			shmctl(data_shmid, IPC_RMID, NULL);
			assert(0);
		}*/

		data->counter++;
	}

	/* release access and quit */
	release();



}

shared_buffers::~shared_buffers()
{
	/* gain exclusive access */
	acquire();

	/* decrement process count and save value for future use */
	data->counter--;
	int counter = data->counter;

	/* detach memory */
	//shmdt(data->buffer);

	/* mark memory to be destroyed */
	//shmctl(buffer_shmid, IPC_RMID, NULL);

	fprintf(stderr, "SharedBuffer: I'm detatching from the memory\n");

	/* detach memory */
	struct shmid_ds shmem_status;
	shmdt(data);
	if (shmctl(data_shmid, IPC_STAT, &shmem_status) == -1)
	{
		PERROR(std::cerr,"shmctl");
	}

	if (shmem_status.shm_nattch == 0)
		shmctl(data_shmid, IPC_RMID, NULL);

	/* destroy semaphore if last process */
	if (counter == 0)
	{
		fprintf(stderr, "SharedBuffer: I'm destroying the sempahore\n");
		semctl(semid, 0, IPC_RMID);
	}
	/* otherwise release access */
	else
	{
		release();
	}
}


bool shared_buffers::push_message(const message_t &msg)
{
	bool success=false;
	acquire();
	std::size_t next_insert=data->insert_point+1;
	if(next_insert==array_size)
	{
		next_insert=0;
	}


	if(data->size!=data->max_size)
	{
		std::array<message_t, array_size>::iterator it=data->buffer.begin();
		std::advance(it,data->insert_point);

		*it=msg;

		data->insert_point=next_insert;
		data->size++;
		success=true;
	}
	release();
	return success;

}

bool shared_buffers::pop_message()
{
	bool success=false;
	if(data->size!=0)
	{

		data->removal_point++;
		if(data->removal_point==array_size)
		{
			data->removal_point=0;
		}
		data->size--;
		success=true;
	}
	return success;
}

bool shared_buffers::get_message(message_t &msg)
{
	bool success=false;
	acquire();
	while( (data->size!=0)  && (!success) )
	{

		std::array<message_t, array_size>::iterator it=data->buffer.begin();
		std::advance(it,data->removal_point);

		it->retrieval_count++;
		msg=std::move(*it);
		if(msg.retrieval_count<3)
		{
			success=true;
		}
		else
		{
			PERROR(std::cout, "Timed out a message... Dropping... Buffer size:"<<data->size);
		}
		pop_message();

	}


	release();
	return success;
}

bool shared_buffers::peek_message(message_t &msg)
{
	bool success=false;
	acquire();
	while( (data->size!=0)  && (!success) )
	{

		std::array<message_t, array_size>::iterator it=data->buffer.begin();
		std::advance(it,data->removal_point);
		success=true;

		msg=*it;

	}


	release();
	return success;
}


std::size_t shared_buffers::max_size() const
{
	return data->max_size;
}

std::size_t shared_buffers::size() const
{
	std::size_t s=0;
	acquire();
	s=data->size;
	release();
	return s;
}


void shared_buffers::acquire() const
{
	struct sembuf sb = { 0, -1, 0 };
	assert(semop(semid, &sb, 1) != -1);

}

void shared_buffers::release() const
{
	struct sembuf sb = { 0, +1, 0 };
	assert(semop(semid, &sb, 1) != -1);
}


}
}

