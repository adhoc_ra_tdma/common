#include <omnetpp_clock/omnetpp_clock.hpp>
#include <csimulation.h>

namespace common
{
namespace omnetpp
{
namespace clock
{
omnetpp_clock::time_point omnetpp_clock::now() noexcept
{
	double clock_skew=1.0;

	cSimulation *sim=cSimulation::getActiveSimulation();
	cModule* application=sim->getContextModule();
	bool hasSkew=application->hasPar("clock_skew");
	if(hasSkew)
	{
		clock_skew=application->par("clock_skew").doubleValue();
	}


	duration ret;
	switch(SimTime::getScaleExp())
	{
		case SIMTIME_S:
			ret=std::chrono::seconds((int64_t)(simTime().raw()*clock_skew));
			break;
		case SIMTIME_MS:
			ret=std::chrono::milliseconds((int64_t)(simTime().raw()*clock_skew));
			break;
		case SIMTIME_US:
			ret=std::chrono::microseconds((int64_t)(simTime().raw()*clock_skew));
			break;
		case SIMTIME_NS:
			ret=std::chrono::nanoseconds((int64_t)(simTime().raw()*clock_skew));
			break;
		case SIMTIME_PS:
			ret=std::chrono::duration_cast<duration>(std::chrono::duration<int64_t, std::pico>((int64_t)(simTime().raw()*clock_skew)));
			break;
		case SIMTIME_FS:
			ret=std::chrono::duration_cast<duration>(std::chrono::duration<int64_t, std::femto>((int64_t)(simTime().raw()*clock_skew)));
			break;
		case (-18):
			ret=std::chrono::duration_cast<duration>(std::chrono::duration<int64_t, std::atto>((int64_t)(simTime().raw()*clock_skew)));
			break;
	}
	EV<<"clock_skew = "<<clock_skew<<std::endl;
	return time_point(ret);
}


}
}
}
