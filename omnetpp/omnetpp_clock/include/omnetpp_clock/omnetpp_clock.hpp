#ifndef __OMNETPP_CLOCK_HPP__
#define __OMNETPP_CLOCK_HPP__

#include <chrono>


#include <distrib.h>
/* If the user wants to use a clock skew, this value must be defined as a parameter clock_skew of type double containing its percentage e.g. 1.01 means that for each 1s elapsed in the simulation the use will perceive 1.01s */
namespace common
{
namespace omnetpp
{
namespace clock
{

struct omnetpp_clock
{
		typedef std::chrono::nanoseconds duration;
		typedef duration::rep                       rep;
		typedef duration::period                    period;
		typedef std::chrono::time_point<omnetpp_clock, duration>    time_point;

		static_assert(omnetpp_clock::duration::min()
					  < omnetpp_clock::duration::zero(),
					  "a clock's minimum duration cannot be less than its epoch");

		static constexpr bool is_steady = false;

		static time_point
		now() noexcept;

		// Map to C API
		static std::time_t
		to_time_t(const time_point& __t) noexcept
		{
			return std::time_t(std::chrono::duration_cast<std::chrono::seconds>
							   (__t.time_since_epoch()).count());
		}

		static time_point
		from_time_t(std::time_t __t) noexcept
		{
			typedef std::chrono::time_point<omnetpp_clock, std::chrono::seconds>   __from;
			return std::chrono::time_point_cast<omnetpp_clock::duration>
					(__from(std::chrono::seconds(__t)));
		}

};
}
}
}






#endif
