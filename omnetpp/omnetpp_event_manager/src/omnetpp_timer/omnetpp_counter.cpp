
#include <omnetpp_timer/omnetpp_counter.hpp>

#include <csimulation.h>
#include <event_manager/event_manager.hpp>


//#define debugLevel lDebug
#ifndef debugLevel
#define debugLevel lError
#endif
#include <debug_headers/debug.hpp>

namespace common
{
namespace omnetpp
{
namespace timer
{
Counter::Counter():
	init(duration_t::zero())
{

}

Counter::Counter(const Counter &other):
	init(other.init)
{

	double clock_skew=1.0;
	cSimulation *sim=cSimulation::getActiveSimulation();
	cModule* application=sim->getContextModule();
	cModule* udpApp=application->getParentModule();
	common::omnetpp::event_manager* manager=dynamic_cast<common::omnetpp::event_manager *>(udpApp->getSubmodule("manager"));

	if(manager!=nullptr)
	{

		if(application->hasPar("clock_skew"))
		{
			clock_skew=application->par("clock_skew").doubleValue();
		}
		std::chrono::nanoseconds now((int64_t)(simTime().inUnit(SIMTIME_NS)/clock_skew));
		init=now;

	}

}


Counter &Counter::operator=(const Counter &other)
{
	if(this!=&other)
	{
		this->init.operator=(other.init);
	}
	return *this;
}


Counter::Counter(Counter &&other):
	init(other.init)
{
}


Counter &Counter::operator=(Counter &&other)
{
	if(this!=&other)
	{
		this->init.operator=(other.init);
	}
	return *this;
}


void Counter::reset()
{
	double clock_skew=1.0;
	cSimulation *sim=cSimulation::getActiveSimulation();
	cModule* application=sim->getContextModule();
	cModule* udpApp=application->getParentModule();
	common::omnetpp::event_manager* manager=dynamic_cast<common::omnetpp::event_manager *>(udpApp->getSubmodule("manager"));

	if(manager!=nullptr)
	{

		if(application->hasPar("clock_skew"))
		{
			clock_skew=application->par("clock_skew").doubleValue();
		}
		std::chrono::nanoseconds now((int64_t)(simTime().inUnit(SIMTIME_NS)/clock_skew));
		init=now;

	}
}


void Counter::reset(duration_t offset)
{
	double clock_skew=1.0;
	cSimulation *sim=cSimulation::getActiveSimulation();
	cModule* application=sim->getContextModule();
	cModule* udpApp=application->getParentModule();
	common::omnetpp::event_manager* manager=dynamic_cast<common::omnetpp::event_manager *>(udpApp->getSubmodule("manager"));

	if(manager!=nullptr)
	{

		if(application->hasPar("clock_skew"))
		{
			clock_skew=application->par("clock_skew").doubleValue();
		}
		std::chrono::nanoseconds now((int64_t)(simTime().inUnit(SIMTIME_NS)/clock_skew));
		init=offset;

	}
}


duration_t Counter::get_time()
{
	duration_t ret=duration_t::min();
	double clock_skew=1.0;
	cSimulation *sim=cSimulation::getActiveSimulation();
	cModule* application=sim->getContextModule();
	cModule* udpApp=application->getParentModule();
	common::omnetpp::event_manager* manager=dynamic_cast<common::omnetpp::event_manager *>(udpApp->getSubmodule("manager"));

	if(manager!=nullptr)
	{

		if(application->hasPar("clock_skew"))
		{
			clock_skew=application->par("clock_skew").doubleValue();
		}
		std::chrono::nanoseconds now((int64_t)(simTime().inUnit(SIMTIME_NS)/clock_skew));
		ret=now-init;
	}
	return ret;
}

}
}
}
