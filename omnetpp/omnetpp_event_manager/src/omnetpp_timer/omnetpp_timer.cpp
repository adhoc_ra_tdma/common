#include <omnetpp_timer/omnetpp_timer.hpp>

#include <event_manager/event_manager.hpp>

#include <iostream>
#include <cassert>

#include <sstream>

//#define debugLevel lDebug
#ifndef debugLevel
#define debugLevel lError
#endif
#include <debug_headers/debug.hpp>


namespace common
{
namespace omnetpp
{

namespace timer
{

bool StateMachine::isStopped()
{
	return state==TIMER_STOPPED;
}
bool StateMachine::isRunning()
{
	return state==TIMER_RUNNING;
}
bool StateMachine::isFired()
{
	return state==TIMER_FIRED;
}

bool StateMachine::start()
{
	uint32_t current_status = state.load();
	if(current_status==TIMER_STOPPED)
	{
		if(state.compare_exchange_strong(current_status,TIMER_RUNNING))
		{
			PDEBUG(std::cout, "Starting state machine");
			return true;
		}
	}
	PDEBUG(std::cout, "Failed starting state machine");
	return false;
}

bool StateMachine::stop()
{
	uint32_t current_status = state.load();
	if(current_status!=TIMER_STOPPED)
	{
		if(state.compare_exchange_strong(current_status,TIMER_STOPPED))
		{
			PDEBUG(std::cout, "Stopping state machine");
			return true;
		}
	}
	PDEBUG(std::cout, "Failed stopping state machine");
	return false;
}

bool StateMachine::fire()
{
	uint32_t current_status = state.load();
	if(current_status==TIMER_RUNNING)
	{
		if(state.compare_exchange_strong(current_status,TIMER_FIRED))
		{
			PDEBUG(std::cout, "Firing state machine");
			return true;
		}
	}
	PDEBUG(std::cout, "Failed firing state machine");
	return false;
}

bool StateMachine::rearm()
{
	uint32_t current_status = state.load();
	if(current_status==TIMER_FIRED)
	{
		if(state.compare_exchange_strong(current_status,TIMER_RUNNING))
		{
			PDEBUG(std::cout, "Rearming state machine");
			return true;
		}
	}
	PDEBUG(std::cout, "Failed rearming state machine");
	return false;
}

StateMachine::StateMachine()
{
	state.store(TIMER_STOPPED);
}


uint64_t Timer::idCounter=0;
Timer::Timer()
{
	cSimulation *sim=cSimulation::getActiveSimulation();
	cModule* application=sim->getContextModule();
	creatorContext=application;
	createTimer();
}

Timer::~Timer()
{
	try
	{
		cSimulation *sim=cSimulation::getActiveSimulation();
		cModule* application=sim->getContextModule();
		cModule* udpApp=application->getParentModule();
		common::omnetpp::event_manager* manager=dynamic_cast<common::omnetpp::event_manager *>(udpApp->getSubmodule("manager"));
		if(manager!=nullptr)
		{
			stop();
			manager->deregisterTimer(this);
		}
	}
	catch(std::exception &e)
	{
		cRuntimeError(std::string("Could not find manager. Is your application running within the event manager? "+std::string(e.what())).c_str());
	}
}

void Timer::start(std::function<void ()> cb, duration_t timeout_ns, duration_t period_ns)
{

	if(stateMachine.start())
	{
		if(timeout_ns==duration_t::zero()) timeout_ns=duration_t(1); //Ensure it fires, there is a stop method!
		double clock_skew=1.0;

		cSimulation *sim=cSimulation::getActiveSimulation();
		cModule* application=sim->getContextModule();
		if(application->hasPar("clock_skew"))
		{
			clock_skew=application->par("clock_skew").doubleValue();
		}


		duration_t simtime_timout=duration_t((int64_t)(timeout_ns.count()/clock_skew));
		duration_t simtime_period=duration_t((int64_t)(period_ns.count()/clock_skew));

		period=simtime_period;

		userCallback=cb;
		startTimer(simtime_timout.count(), simtime_period.count());
	}
}

void Timer::restart(std::function<void ()> cb, duration_t timeout_ns, duration_t period_ns)
{
	Timer::stop();
	Timer::start(cb,timeout_ns,period_ns);
}


void Timer::stop()
{
	if(stateMachine.stop())
	{
		stopTimer();
	}

}

int Timer::createTimer()
{
	cSimulation *sim=cSimulation::getActiveSimulation();
	cModule* application=sim->getContextModule();
	cModule* udpApp=application->getParentModule();
	common::omnetpp::event_manager* manager=dynamic_cast<common::omnetpp::event_manager *>(udpApp->getSubmodule("manager"));
	if(manager!=nullptr)
	{
		id=idCounter++;
		if(idCounter==0)
		{
			//overflow
			cRuntimeError("Reached the maximum number of timers... How many timers do you need to do this? 2^64!!!!!");
			return -1;
		}
		std::stringstream ss;
		ss<<id;
		std::string name="timer "+ss.str();
		manager->registerTimer(this,name);

	}
	else
	{
		cRuntimeError("Could not find manager. Is your application running within the event manager?");
	}

	return 0;
}
//bool done=false;
int Timer::startTimer( uint64_t firstFire, uint64_t interval )

{
	period=std::chrono::nanoseconds(interval);
	cSimulation *sim=cSimulation::getActiveSimulation();
	cModule* application=sim->getContextModule();
	cModule* udpApp=application->getParentModule();
	common::omnetpp::event_manager* manager=dynamic_cast<common::omnetpp::event_manager *>(udpApp->getSubmodule("manager"));
	if(manager!=nullptr)
	{
		manager->scheduleTimer(this,std::chrono::nanoseconds(firstFire));
	}

	return(0);
}

int Timer::stopTimer( )
{
	cSimulation *sim=cSimulation::getActiveSimulation();
	cModule* application=sim->getContextModule();
	cModule* udpApp=application->getParentModule();
	common::omnetpp::event_manager* manager=dynamic_cast<common::omnetpp::event_manager *>(udpApp->getSubmodule("manager"));
	if(manager!=nullptr)
	{
		manager->cancelTimer(this);
	}

	return 0;
}

void Timer::timerCallback()
{
	cMethodCallContextSwitcher __ctx(creatorContext); __ctx.methodCall("Timer::timerCallback");
	cSimulation *sim=cSimulation::getActiveSimulation();
	cModule* application=sim->getContextModule();
	cModule* udpApp=application->getParentModule();
	common::omnetpp::event_manager* manager=dynamic_cast<common::omnetpp::event_manager *>(udpApp->getSubmodule("manager"));

	if(stateMachine.fire())
	{
		if(period>duration_t::zero())
		{
			if(stateMachine.rearm())
			{
				if(manager!=nullptr)
				{
					manager->scheduleTimer(this,period);
				}
			}
		}
		else
		{
			if(stateMachine.stop())
			{
				stopTimer();
			}
			else
			{
				PERROR(std::cout,"Stop failed!!!");
			}
		}
		this->userCallback(); // this call can change the status
	}


}



duration_t Timer::absoluteExpiryTime() const
{
	duration_t skewed_ret=duration_t::min();

	cSimulation *sim=cSimulation::getActiveSimulation();
	cModule* application=sim->getContextModule();
	cModule* udpApp=application->getParentModule();
	common::omnetpp::event_manager* manager=dynamic_cast<common::omnetpp::event_manager *>(udpApp->getSubmodule("manager"));
	if(manager!=nullptr)
	{

		double clock_skew=1.0;

		if(application->hasPar("clock_skew"))
		{
			clock_skew=application->par("clock_skew").doubleValue();
		}
		duration_t simtime_ret=manager->getExpiryTime(this);
		PDEBUG(std::cout,"Expiry time "<<simtime_ret.count());
		skewed_ret=duration_t((uint64_t)(simtime_ret.count()*clock_skew));
		PDEBUG(std::cout,"Skewed "<<skewed_ret.count());

	}
	else
	{
		cRuntimeError("Could not find manager. Is your application running within the event manager?");
	}


	return skewed_ret;

}

void Timer::delay(duration_t delay)
{
	cSimulation *sim=cSimulation::getActiveSimulation();
	cModule* application=sim->getContextModule();
	cModule* udpApp=application->getParentModule();
	common::omnetpp::event_manager* manager=dynamic_cast<common::omnetpp::event_manager *>(udpApp->getSubmodule("manager"));

	if(manager!=nullptr)
	{
		double clock_skew=1.0;

		if(application->hasPar("clock_skew"))
		{
			clock_skew=application->par("clock_skew").doubleValue();
		}
		duration_t simtime_ns=manager->getExpiryTime(this);

		PDEBUG(std::cout,"Prev Sim Exp time "<<simtime_ns.count());

		duration_t simtime_delay=duration_t((int64_t)(delay.count()/clock_skew));

		PDEBUG(std::cout,"User delay "<<delay.count());
		PDEBUG(std::cout,"Sim delay "<<simtime_delay.count());
		PDEBUG(std::cout,"New Sim Exp Time "<<(simtime_ns+simtime_delay).count());

		std::chrono::nanoseconds now(simTime().inUnit(SIMTIME_NS));
		manager->cancelTimer(this);
		// This takes a delta!
		manager->scheduleTimer(this,(simtime_ns-now+simtime_delay) );

	}
	else
	{
		cRuntimeError("Could not find manager. Is your application running within the event manager?");
	}


}



}
}
}
