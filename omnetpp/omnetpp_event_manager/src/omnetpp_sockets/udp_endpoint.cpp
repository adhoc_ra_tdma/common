#include <omnetpp_sockets/udp_endpoint.hpp>

namespace common
{
namespace omnetpp
{
namespace sockets
{



udp_endpoint::udp_endpoint()
{
}

udp_endpoint::udp_endpoint(const udp_endpoint &other):
	generic_udp_endpoint(other),
	addr(other.addr)
{
}


udp_endpoint &udp_endpoint::operator=(const udp_endpoint &other)
{
	if(&other!=this)
	{
		setPort(other.getPort());
		addr=other.addr;
	}
	return *this;
}

udp_endpoint::udp_endpoint(udp_endpoint &&other):
	generic_udp_endpoint(std::move(other)),
	addr(other.addr)
{
}

udp_endpoint &udp_endpoint::operator=(udp_endpoint &&other)
{
	if(&other!=this)
	{
		setPort(std::move(other.getPort()));
		addr=other.addr;
	}
	return *this;
}


udp_endpoint::udp_endpoint(std::string ipAddr, uint16_t port)
{
	this->addr.set(ipAddr.c_str());
	this->setPort(port);
}

void udp_endpoint::setBytes(const std::array<uint8_t, 4> &ipAddr)
{
	IPv4Address v4;
	v4.set(ipAddr[0],ipAddr[1],ipAddr[2],ipAddr[3]);
	addr.set(v4);
}

std::array<uint8_t, 4> udp_endpoint::getBytes() const
{
	IPv4Address v4=addr.get4();
	std::array<uint8_t, 4> ret;
	ret[3]=v4.getDByte(3);
	ret[2]=v4.getDByte(2);
	ret[1]=v4.getDByte(1);
	ret[0]=v4.getDByte(0);
	return ret;
}

std::string udp_endpoint::getAddr() const
{
	return addr.str();
}

void udp_endpoint::setAddr(const std::string &ipAddr)
{
	this->addr.set(ipAddr.c_str());
}




}
}
}
