#include <omnetpp_sockets/udp_unicast_socket.hpp>
#include <cassert>
#include <iostream>

//#define debugLevel lInfo
#ifndef debugLevel
#define debugLevel lError
#endif
#include <debug_headers/debug.hpp>

#include <omnetpp_sockets/udp_endpoint.hpp>
#include <event_manager/event_manager.hpp>

#include <fstream>
namespace common
{
namespace omnetpp
{
namespace sockets
{


udp_unicast_socket::udp_unicast_socket(uint16_t port):
	listenPort(port)
{
	cSimulation *sim=cSimulation::getActiveSimulation();
	cModule* application=sim->getContextModule();
	creatorContext=application;
}

udp_unicast_socket::~udp_unicast_socket()
{
	stop();
}

void udp_unicast_socket::start()
{
	open();
	setsockoptions();
}

void udp_unicast_socket::restart()
{
	stop();
	start();
}

void udp_unicast_socket::stop()
{
	close();
}

int16_t udp_unicast_socket::open()
{
	cSimulation *sim=cSimulation::getActiveSimulation();
	cModule* application=sim->getContextModule();
	cModule* udpApp=application->getParentModule();
	event_manager* manager=dynamic_cast<event_manager*>(udpApp->getSubmodule("manager"));
	if(manager!=nullptr)
	{
		manager->registerUnicastSocket(this, listenPort);
	}
	return 0;
}


int16_t udp_unicast_socket::setsockoptions()
{
	return 0;
}


void udp_unicast_socket::close()
{
	cSimulation *sim=cSimulation::getActiveSimulation();
	cModule* application=sim->getContextModule();
	cModule* udpApp=application->getParentModule();
	event_manager* manager=dynamic_cast<event_manager*>(udpApp->getSubmodule("manager"));
	if(manager!=nullptr)
	{
		manager->deregisterSocket(this);
	}
	userCallback.reset();

}


void udp_unicast_socket::async_read(cbType func)
{
	userCallback.reset(new cbType(func));

}

void udp_unicast_socket::async_stop()
{
	userCallback.reset();
}

void udp_unicast_socket::write(const MessageUDP &msg)
{
	write(msg,"UDPPacket");
}


void udp_unicast_socket::write(const MessageUDP &msg, std::string msg_name)
{
	cSimulation *sim=cSimulation::getActiveSimulation();
	cModule* application=sim->getContextModule();
	cModule* udpApp=application->getParentModule();
	event_manager* manager=dynamic_cast<event_manager*>(udpApp->getSubmodule("manager"));
	if(manager!=nullptr)
	{
		manager->sendto(this, msg, msg_name);
	}

	//	return msg.getData().size();

}

void udp_unicast_socket::managerCallback(const MessageUDP &m)
{
	cMethodCallContextSwitcher __ctx(creatorContext); __ctx.methodCall("udp_unicast_socket::managerCallback");

	PINFO(std::cout,"Received from "<<m.getUDPEndpoint().getAddr()<<":"<<m.getUDPEndpoint().getPort());


	if(userCallback!=nullptr)
	{
		(*userCallback)(m);
	}
}




}
}
}

