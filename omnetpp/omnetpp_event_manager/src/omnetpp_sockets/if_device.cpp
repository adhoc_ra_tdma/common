#include <omnetpp_sockets/if_device.hpp>
#include <event_manager/event_manager.hpp>

#ifndef debugLevel
#define debugLevel lDebug
#endif
#include <debug_headers/debug.hpp>

#include <csimulation.h>
#include <networklayer/common/InterfaceTableAccess.h>
#include <applications/common/ApplicationBase.h>
#include <networklayer/ipv4/IPv4InterfaceData.h>
namespace common
{
namespace omnetpp
{
namespace sockets
{

if_device::if_device(std::string ifname):
	name(ifname)
{
	init_device(ifname);

}

if_device::if_device(const if_device &other):
	found(other.found),
	ifIndex(other.ifIndex),
	name(other.name),
	ipAddr(other.ipAddr)
{
}

if_device &if_device::operator=(const if_device &other)
{
	if(&other!=this)
	{
		ifIndex=other.ifIndex;
		ipAddr=other.ipAddr;
		name=other.name;
		found=other.found;
	}
	return *this;
}


if_device::if_device(if_device &&other):
	found(std::move(other.found)),
	ifIndex(std::move(other.ifIndex)),
	ipAddr(std::move(other.ipAddr)),
	name(std::move(other.name))
{
}

if_device &if_device::operator=(if_device &&other)
{
	if(&other!=this)
	{
		ifIndex=std::move(other.ifIndex);
		ipAddr=std::move(other.ipAddr);
		name=std::move(other.name);
		found=std::move(other.found);
	}
	return *this;
}


uint16_t if_device::getIndex() const
{
	return ifIndex;
}


std::string if_device::getAddr() const
{
	return ipAddr;
}

std::string if_device::getName() const
{
	return name;
}

bool if_device::isFound() const
{
	return found;
}


void if_device::init_device(std::string ifname)
{
	cSimulation *sim=cSimulation::getActiveSimulation();
	EV<<"CT: "<<sim->getContextType()<<"\n";
	cModule* application=sim->getContextModule();
	if(application!=nullptr)
	{
		cModule* udpApp=application->getParentModule();
		event_manager* manager=dynamic_cast<event_manager*>(udpApp->getSubmodule("manager"));
		if(manager==nullptr)
		{
			std::cout<<"manager is NULL\n";
		}
		else
		{
			IInterfaceTable *ift = InterfaceTableAccess().get(manager);
			InterfaceEntry *ie = ift->getInterfaceByName(ifname.c_str());
			ifIndex=ie->getInterfaceId();
			IPv4InterfaceData *ipv4data=ie->ipv4Data();
			IPv4Address address = ipv4data->getIPAddress();
			if(address.isUnspecified())
			{
				PERROR(std::cerr,"no address");
				assert(!address.isUnspecified());
			}
			ipAddr=address.str();
			found=true;
		}
	}



}


}
}
}

