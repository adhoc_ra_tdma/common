#include <omnetpp_sockets/udp_multicast_socket.hpp>

#include <cassert>
#include <iostream>

#ifndef debugLevel
#define debugLevel lError
#endif
#include <debug_headers/debug.hpp>

#include <event_manager/event_manager.hpp>
namespace common
{
namespace omnetpp
{
namespace sockets
{



udp_multicast_socket::udp_multicast_socket(const if_device &dev, const udp_endpoint &multicastAddress):
	udp_unicast_socket(multicastAddress.getPort()),
	listenInterface(dev),
	multicastAddress(multicastAddress)
{
	std::cout<<listenInterface.isFound()<<std::endl;
}

int16_t udp_multicast_socket::open()
{
	cSimulation *sim=cSimulation::getActiveSimulation();
	cModule* application=sim->getContextModule();
	cModule* udpApp=application->getParentModule();
	event_manager* manager=dynamic_cast<event_manager*>(udpApp->getSubmodule("manager"));
	if(manager!=nullptr)
	{
		manager->registerMulticastSocket(this, multicastAddress.getAddr(), multicastAddress.getPort(),listenInterface,RECEIVE_OUR_DATA,1);
	}
	return 0;
}



}
}
}

