
#include <network_generics/udp_message.hpp>
#include <event_manager/event_manager.hpp>


//#define debugLevel lDebug
#ifndef debugLevel
#define debugLevel lError
#endif
#include <debug_headers/debug.hpp>


#include <fstream>
#include <networklayer/common/InterfaceTableAccess.h>
#include <applications/common/ApplicationBase.h>
#include <networklayer/ipv4/IPv4InterfaceData.h>

Define_Module(common::omnetpp::event_manager);

namespace common
{
namespace omnetpp
{


event_manager::event_manager()
{
	WATCH_MAP(timers);
	WATCH_MAP(sockets);
}

event_manager::~event_manager()
{
}


void event_manager::registerTimer(timer::Timer *timer, std::string name)
{
	Enter_Method("event_manager::registerTimer()");

	try
	{
		timers.at(timer);
	}
	catch(std::out_of_range &e)
	{
		timers[timer]=new cMessage(name.c_str());
		timers[timer]->setKind(timer->id);
	}
	return;
	cRuntimeError(std::string("Tried to re-register a timer: ").c_str());
}

void event_manager::deregisterTimer(timer::Timer *timer)
{
	Enter_Method("event_manager::deregisterTimer()");

	try
	{
		std::map<timer::Timer *, cMessage *>::iterator it=timers.find(timer);
		if(it!=timers.end())
		{
			cMessage *msg = timers.at(timer);
			cancelAndDelete(msg);
			timers.erase(it);
		}
	}
	catch(std::out_of_range &e)
	{
		// Timer is not registered... No big deal
	}

}

void event_manager::scheduleTimer( timer::Timer *timer, const std::chrono::nanoseconds &time)
{
	Enter_Method("event_manager::scheduleTimer()");
	std::chrono::nanoseconds time_ns(simTime().inUnit(SIMTIME_NS));
	if(time>=std::chrono::nanoseconds::zero())
	{
		time_ns+=time;
	}
	simtime_t time_sim=SimTime(time_ns.count(),SIMTIME_NS);
	try
	{
		cMessage *msg = timers.at(timer);
		if(msg->isScheduled())
		{
			msg=cancelEvent(msg);
		}
		scheduleAt(time_sim,msg);
		timers.at(timer)=msg;
	}
	catch(std::out_of_range &e)
	{
		cRuntimeError((std::string("Tried to schedule a non registered timer:")+e.what()).c_str());
	}
}

void event_manager::cancelTimer(timer::Timer *timer)
{
	Enter_Method("event_manager::scheduleTimer()");
	try
	{
		cMessage *msg = timers.at(timer);
		msg=cancelEvent(msg);
		timers.at(timer)=msg;
	}
	catch(std::out_of_range &e)
	{
		cRuntimeError((std::string("Tried to schedule a non registered timer:")+e.what()).c_str());
	}
}

std::chrono::nanoseconds event_manager::getExpiryTime(const timer::Timer *timer)
{
	Enter_Method("event_manager::getExpiryTime()");
	std::chrono::nanoseconds ret=std::chrono::nanoseconds::min();
	try
	{
		timer::Timer *warning_const=const_cast<timer::Timer *>(timer);
		cMessage *msg = timers.at(warning_const);
		if( (msg->isScheduled()) )
		{
			if( (msg->getArrivalTime()>simTime()) )
			{
				ret=std::chrono::nanoseconds(msg->getArrivalTime().inUnit(SIMTIME_NS));
			}
			else
			{
				PDEBUG(std::cout, "Has already arrived at "<<msg->getArrivalTime().inUnit(SIMTIME_NS)<<" now "<<simTime().inUnit(SIMTIME_NS));
				ret=std::chrono::nanoseconds::min();
			}
		}
		else
		{
			PDEBUG(std::cout, "Message is not scheduled");
			ret=std::chrono::nanoseconds::min();
		}

	}
	catch(std::out_of_range &e)
	{
		cRuntimeError((std::string("Tried to schedule a non registered timer:")+e.what()).c_str());
	}
	return ret;
}

void event_manager::registerUnicastSocket(udp_unicast_socket *sock, uint16_t port)
{
	Enter_Method("event_manager::registerUnicastSocket()");

	try
	{
		sockets.at(sock);
	}
	catch(std::out_of_range &e)
	{
		sockets[sock]=new UDPSocket;
		sockets[sock]->setOutputGate(gate("udpOut"));
		sockets[sock]->bind(port);
	}
	return;
	cRuntimeError(std::string("Tried to re-register a socket: ").c_str());



}

void event_manager::registerMulticastSocket(udp_unicast_socket *sock, std::string multicastAddr, uint16_t port, const sockets::if_device &dev, bool loop, uint8_t ttl)
{
	Enter_Method("event_manager::deregisterUnicastSocket()");

	try
	{
		sockets.at(sock);
	}
	catch(std::out_of_range &e)
	{
		sockets[sock]=new UDPSocket;
		sockets[sock]->setOutputGate(gate("udpOut"));
		sockets[sock]->bind(port);
		sockets[sock]->setReuseAddress(true);
		sockets[sock]->setTimeToLive(ttl);

		if (dev.isFound())
		{
			sockets[sock]->setMulticastOutputInterface(dev.getIndex());
			sockets[sock]->joinMulticastGroup(IPvXAddress(multicastAddr.c_str()), dev.getIndex());
		}
		else
		{
			cRuntimeError("Tryied to open an interface on an unknown device.");
		}
		sockets[sock]->setMulticastLoop(loop);


	}
	return;
}

void event_manager::deregisterSocket(udp_unicast_socket *sock)
{
	Enter_Method("event_manager::deregisterUnicastSocket()");

	try
	{
		UDPSocket *socket = sockets.at(sock);
		socket->close();
		delete(socket);
		sockets.erase(sockets.find(sock));
	}
	catch(std::out_of_range &e)
	{
		cRuntimeError(std::string("Tried to deregister an unknown socket: ").c_str());
	}
	return;
}



static std::ofstream log;
static std::mutex log_mutex;



void event_manager::sendto(udp_unicast_socket *sock, const sockets::MessageUDP &msg, std::string msg_name)
{
	Enter_Method("event_manager::sendTo()");

	try
	{
		UDPSocket *socket = sockets.at(sock);

		UDPSocket_m *udp =new UDPSocket_m(msg_name.c_str());
		udp->setByteLength(msg.getData().size());
		udp->setDataArraySize(msg.getData().size());
		for(std::size_t i=0;i<msg.getData().size();i++)
		{
			udp->setData(i,msg.getData()[i]);
		}
		IPvXAddress destAddr(msg.getUDPEndpoint().getAddr().c_str());

		socket->sendTo(udp,destAddr,msg.getUDPEndpoint().getPort());


		log_mutex.lock();

		if(!log.is_open())
		{
			log.open("raTDMA_omnetpp_log.txt", std::fstream::out | std::fstream::app);
			log<<"event_code;time;node_id;msg_id;source_address;destination_address"<<std::endl;
			log<<"# event codes: M=multicast send"<<std::endl;
			log<<"# event codes: N=multicast receive"<<std::endl;
			log<<"# event codes: U=unicast send"<<std::endl;
			log<<"# event codes: V=unicast receive"<<std::endl;
		}



		cSimulation *sim=cSimulation::getActiveSimulation();
		cModule* application=sim->getContextModule();
		cModule* udpApp=application->getParentModule();

		common::network::generics::ipv4 addr_src;
		common::network::generics::ipv4 addr_dest;
		addr_src.setInt(InterfaceTableAccess().get(this)->getInterfaceByName("wlan0")->ipv4Data()->getIPAddress().getInt());
		addr_dest.setInt(destAddr.get4().getInt());

		if(destAddr.isMulticast())
		{
			log<<"M;";
		}
		else
		{
			log<<"U;";
		}
		log<<SIMTIME_DBL(simTime())<<";"<<+udpApp->getParentModule()->getIndex() <<";"<<udp->getTreeId()<<";"<<addr_src.getAddr()<<";"<<addr_dest.getAddr()<<std::endl;
		log_mutex.unlock();


	}
	catch(std::out_of_range &e)
	{
		cRuntimeError(std::string("Tried to deregister an unknown socket: ").c_str());
	}

}


int event_manager::numInitStages() const
{
	return 1;
}

void event_manager::initialize(int stage)
{
	ApplicationBase::initialize(stage);

	if (stage == 0)
	{
		setOperational(true);
		std::stringstream ss;
		ss << "This is how you get the node index of the client (if needed): "<<(uint16_t)(getParentModule()->getIndex());
		EV<<ss.str();

	}
}

void event_manager::finish()
{
	// Do something

	while(!timers.empty())
	{
		cancelAndDelete((timers.begin())->second);
		timers.erase(timers.begin());
	}

	ApplicationBase::finish();
}





void event_manager::handleMessageWhenUp(cMessage *msg)
{

	if (msg->isSelfMessage())
	{
		EV<<"Is self message"<<std::endl;
		for(std::pair<common::omnetpp::timer::Timer * const, cMessage *> &t:timers)
		{
			if(msg == t.second)
			{
				EV<<"Found timer"<<std::endl;
				t.first->timerCallback();
				break;
			}
		}
		//Do NOT delete timers

	}
	else if (msg->getKind() == UDP_I_DATA)
	{
		EV << "Received UDP: " << UDPSocket::getReceivedPacketInfo(check_and_cast<cPacket *>(msg)) << endl;
		UDPDataIndication *udi=check_and_cast<UDPDataIndication *>(msg->getControlInfo());

		cSimulation *sim=cSimulation::getActiveSimulation();
		cModule* application=sim->getContextModule();
		cModule* udpApp=application->getParentModule();

		log_mutex.lock();
		IPvXAddress dest = udi->getDestAddr();
		IPvXAddress src = udi->getSrcAddr();
		common::network::generics::ipv4 addr_src;
		common::network::generics::ipv4 addr_dest;
		addr_src.setInt(src.get4().getInt());
		addr_dest.setInt(dest.get4().getInt());
		if(dest.isMulticast())
		{
			log<<"N;";
		}
		else
		{
			log<<"V;";
		}
		log<<SIMTIME_DBL(simTime())<<";"<<+udpApp->getParentModule()->getIndex() << ";"<<msg->getTreeId()<<";"<<addr_src.getAddr()<<";"<<addr_dest.getAddr()<<std::endl;
		log_mutex.unlock();



		for(std::pair<common::omnetpp::sockets::udp_unicast_socket* const, UDPSocket *> &s:sockets)
		{
			if(s.second->belongsToSocket(msg))
			{
				UDPSocket_m *pack=check_and_cast<UDPSocket_m*>(msg);
				std::vector<uint8_t> recv_data;

				for(std::size_t i=0;i<pack->getDataArraySize();i++)
				{
					recv_data.push_back(pack->getData(i));
				}
				common::network::generics::MessageUDP m(udi->getSrcAddr().str(),udi->getSrcPort(),recv_data);
				s.first->managerCallback(m);
				break;
			}
		}
		delete(msg);
	}
	else if (msg->getKind() == UDP_I_ERROR)
	{
		EV << "Ignoring UDP error report\n";
		delete(msg);
	}
	else
	{
		error("Unrecognized message (%s)%s", msg->getClassName(), msg->getName());
		delete(msg);
	}

	if (ev.isGUI())
	{
		char buf[500];

		getDisplayString().setTagArg("t", 0, buf);
	}
}

void event_manager::handleMessageWhenDown(cMessage *msg)
{
	if (msg->isSelfMessage())
	{
		EV<<"Is self message"<<std::endl;
		for(std::pair<common::omnetpp::timer::Timer * const, cMessage *> &t:timers)
		{
			if(msg == t.second)
			{
				EV<<"Found timer"<<std::endl;
				t.first->timerCallback();
				break;
			}
		}
		//Do NOT delete timers

	}
	else if (msg->getKind() == UDP_I_DATA)
	{
		delete(msg);
	}
	else if (msg->getKind() == UDP_I_ERROR)
	{
		EV << "Ignoring UDP error report\n";
		delete(msg);
	}
	else
	{
		error("Unrecognized message (%s)%s", msg->getClassName(), msg->getName());
		delete(msg);
	}

	if (ev.isGUI())
	{
		char buf[500];

		getDisplayString().setTagArg("t", 0, buf);
	}
}

bool event_manager::handleNodeStart(IDoneCallback *doneCallback)
{
	return true;
}

bool event_manager::handleNodeShutdown(IDoneCallback *doneCallback)
{
	return true;
}

void event_manager::handleNodeCrash()
{
}

}
}

