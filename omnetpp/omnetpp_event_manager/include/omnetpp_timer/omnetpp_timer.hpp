#ifndef __OMNETPP_TIMER_HPP__
#define __OMNETPP_TIMER_HPP__


#include <functional>


#include <cmessage.h>
#include <time_interfaces/timerI.hpp>
#include <event_manager/event_manager.hpp>

namespace common
{
namespace omnetpp
{

class event_manager;


namespace timer
{

class StateMachine
{
	public:

		StateMachine();

		enum states
		{
			TIMER_STOPPED=0,
			TIMER_RUNNING,
			TIMER_FIRED
		};

		std::atomic<uint32_t> state;

		bool isStopped();
		bool isRunning();
		bool isFired();

		bool start();
		bool stop();
		bool fire();
		bool rearm();
};

using TimerI=common::time::interfaces::TimerI;
using duration_t=common::time::interfaces::duration_t;




class Timer:public TimerI
{
		friend class common::omnetpp::event_manager;

	public:

		Timer();
		virtual ~Timer();

		virtual void start(std::function<void()> cb, duration_t timeout_ns, duration_t period_ns=duration_t::zero());
		virtual void restart(std::function<void()> cb, duration_t timeout_ns, duration_t period_ns=duration_t::zero());
		virtual void stop();

		virtual duration_t absoluteExpiryTime() const;
		virtual void delay(duration_t delay);

	protected:
		//struct timespec my_wait;

		StateMachine stateMachine;

		uint16_t id;

		int createTimer();

		int startTimer( uint64_t firstFire, uint64_t interval );
		int stopTimer( );


		std::function<void()> userCallback;

		duration_t period;

	private:
		static uint64_t idCounter;
		void timerCallback();

		cComponent *creatorContext;



};
}
}
}



#endif
