#ifndef __OMNETPP_COUNTER_HPP__
#define __OMNETPP_COUNTER_HPP__

#include <time_interfaces/timerI.hpp>



namespace common
{
namespace omnetpp
{
namespace timer
{

using common::time::interfaces::TimerI;
using common::time::interfaces::CounterI;
using duration_t=common::time::interfaces::duration_t;

class Counter:public CounterI
{
	public:


		Counter();

		Counter(const Counter &other);
		Counter &operator=(const Counter &other);

		Counter(Counter &&other);
		Counter &operator=(Counter &&other);

		virtual ~Counter()=default;

		void reset() override;
		void reset(duration_t offset) override;
		duration_t get_time() override;
	private:
		duration_t init;


};


}
}
}
#endif



