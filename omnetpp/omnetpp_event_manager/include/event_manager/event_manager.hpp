#ifndef __OMNETPP_SOCKET_MANAGER_HPP__
#define __OMNETPP_SOCKET_MANAGER_HPP__

#include <omnetpp_sockets/udp_unicast_socket.hpp>
#include <omnetpp_sockets/udp_multicast_socket.hpp>
#include <omnetpp_sockets/if_device.hpp>

#include <omnetpp_timer/omnetpp_timer.hpp>


#include <omnetpp_event_manager/omnetpp_event_manager/UDPMessage_m.hpp>
#include <base/INETDefs.h>

#include <applications/common/ApplicationBase.h>
#include <UDPSocket.h>
#include "UDPControlInfo_m.h"


#include <cstdint>
#include <string>
#include <vector>
#include <map>
namespace common
{
namespace omnetpp
{

namespace timer
{
	class Timer;
}



using udp_unicast_socket=common::omnetpp::sockets::udp_unicast_socket;
class INET_API event_manager : public ApplicationBase
{
	protected:

		//std::vector<common::omnetpp::sockets::udp_multicast_socket *> mcastSockets;
		std::map<common::omnetpp::timer::Timer *, cMessage *> timers;

		std::map<common::omnetpp::sockets::udp_unicast_socket *, UDPSocket *> sockets;

	public:
		event_manager();
		~event_manager();




		// These need to change context

		//Timers
		void registerTimer(common::omnetpp::timer::Timer *timer, std::string name);
		void deregisterTimer(common::omnetpp::timer::Timer *timer);
		void scheduleTimer(timer::Timer *timer, const std::chrono::nanoseconds &time);
		void cancelTimer( timer::Timer *timer);
		std::chrono::nanoseconds getExpiryTime(const timer::Timer *timer);

		// Sockets
		void registerUnicastSocket(udp_unicast_socket *sock, uint16_t port);
		void registerMulticastSocket(udp_unicast_socket *sock, std::string multicastAddr, uint16_t port, const sockets::if_device &dev, bool loop, uint8_t ttl);
		void deregisterSocket(udp_unicast_socket *sock);
		void sendto(udp_unicast_socket *sock, const sockets::MessageUDP &msg, std::string msg_name);





	protected:

		virtual int numInitStages() const;
		virtual void initialize(int stage);

		virtual void handleMessageWhenUp(cMessage *msg);
		virtual void handleMessageWhenDown(cMessage *msg);

		virtual void finish();

		virtual bool handleNodeStart(IDoneCallback *doneCallback);
		virtual bool handleNodeShutdown(IDoneCallback *doneCallback);
		virtual void handleNodeCrash();


};
}
}




#endif /* __MYUDP_APP_HPP__ */
