#ifndef __OMNETPP_UDP_UNICAST_SOCKET_HPP__
#define __OMNETPP_UDP_UNICAST_SOCKET_HPP__

#include <network_interfaces/udp_unicast_socketI.hpp>
//#include <linux_sockets/if_device.hpp>
#include <network_generics/udp_message.hpp>
#include <functional>
#include <future>
#include <list>
#include <cassert>
#include <transport/contract/UDPSocket.h>

namespace common
{
namespace omnetpp
{
class event_manager;
namespace sockets
{

using MessageUDP=common::network::generics::MessageUDP;
using udp_callback_socket_interface=common::network::interfaces::udp_callback_socket_interface<MessageUDP>;
class udp_unicast_socket: /* implements */ public virtual udp_callback_socket_interface
{
		friend class common::omnetpp::event_manager;

	protected:
		using cbType=std::function<void(const MessageUDP &)>;
		using cbPointer=std::shared_ptr<cbType>;
	public:

		udp_unicast_socket(uint16_t port);



		virtual ~udp_unicast_socket();


		//startable_stoppable
		virtual void start() override;
		virtual void restart() override;
		virtual void stop() override;

		//async_nonblock_reader
		virtual void async_read(cbType func) override;
		virtual void async_stop() override;

		//async_nonblock_writer
		virtual void write(const MessageUDP &msg) override;
		virtual void write(const MessageUDP &msg, std::string msg_name);





	protected:
		// These need to change context
		void managerCallback(const MessageUDP &m);

		cbPointer userCallback=nullptr;

		uint16_t listenPort;

		std::vector<uint8_t> buffer;

		virtual int16_t open();
		virtual void close();
		virtual int16_t setsockoptions();


	private:
		cComponent *creatorContext;
};

}
}
}


#endif
