#ifndef __OMNETPP_IF_DEVICE_HPP__
#define __OMNETPP_IF_DEVICE_HPP__


#include <network_interfaces/if_deviceI.hpp>
#include <cstring>
#include <sys/socket.h>
#include <linux/if.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <string>
#include <sstream>
#include <cassert>



namespace common
{
namespace omnetpp
{
namespace sockets
{


class if_device: public common::network::interfaces::if_deviceI
{

	public:
		if_device()=delete;

		if_device(std::string ifname);

		if_device(const if_device &other);
		if_device &operator=(const if_device &other);

		if_device(if_device &&other);
		if_device &operator=(if_device &&other);

		virtual ~if_device()=default;

		virtual uint16_t getIndex() const override;
		virtual std::string getAddr() const override;
		virtual std::string getName() const override;
		virtual bool isFound() const override;

	private:
		bool found=false;
		int ifIndex;
		std::string ipAddr;
		std::string name;

		void init_device(std::string ifname);

};

}
}
}
#endif

