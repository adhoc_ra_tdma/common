#ifndef __OMNETPP_UDP_MULTICAST_SOCKET_HPP__
#define __OMNETPP_UDP_MULTICAST_SOCKET_HPP__


#include <network_interfaces/udp_multicast_socketI.hpp>
#include <omnetpp_sockets/udp_unicast_socket.hpp>
#include <omnetpp_sockets/udp_endpoint.hpp>
#include <omnetpp_sockets/if_device.hpp>
#include <network_generics/udp_message.hpp>

#define RECEIVE_OUR_DATA 1

namespace common
{
namespace omnetpp
{
namespace sockets
{

using MessageUDP=common::network::generics::MessageUDP;
using udp_multicast_socket_event_triggered=common::network::interfaces::udp_multicast_socket_event_triggered<MessageUDP>;
class udp_multicast_socket: public udp_unicast_socket, public udp_multicast_socket_event_triggered
{

		using Super=udp_unicast_socket;
	public:

		udp_multicast_socket(const common::omnetpp::sockets::if_device &dev, const common::omnetpp::sockets::udp_endpoint &multicastAddress);
		~udp_multicast_socket()=default;


	protected:

		virtual int16_t open() override;
		if_device listenInterface;
		common::omnetpp::sockets::udp_endpoint multicastAddress;
	private:
};


}
}
}

#endif
