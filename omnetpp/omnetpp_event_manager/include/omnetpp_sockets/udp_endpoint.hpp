#ifndef __OMNETPP_UDP_ENDPOINT_HPP__
#define __OMNETPP_UDP_ENDPOINT_HPP__


#include <network_generics/udp_endpoint.hpp>

#include <cstring>
#include <string>
#include <cstdint>
#include <networklayer/contract/IPvXAddress.h>
namespace common
{

namespace omnetpp
{

namespace sockets
{


using generic_udp_endpoint=common::network::generics::udp_endpoint;
class udp_endpoint: public generic_udp_endpoint
{

	public:
		udp_endpoint();
		udp_endpoint(const udp_endpoint &other);
		udp_endpoint &operator=(const udp_endpoint &other);

		udp_endpoint(udp_endpoint &&other);
		udp_endpoint &operator=(udp_endpoint &&other);

		udp_endpoint(std::string ipAddr, uint16_t port);

		virtual void setBytes(const std::array<uint8_t, 4> &ipAddr) override;
		virtual std::array<uint8_t, 4> getBytes() const override;

		virtual void setAddr(const std::string &ipAddr) override;
		virtual std::string getAddr() const override;
	private:
		IPvXAddress addr;
};
}
}
}
#endif

