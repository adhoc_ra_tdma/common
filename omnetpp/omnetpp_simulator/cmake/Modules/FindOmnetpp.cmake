#.rst:
# FindOmnetpp
# ---------
#
# Find Omnetpp include dirs and libraries
#
# Use this module by invoking find_package with the form::
#
#   find_package(Omnetpp
#     [version] [EXACT]      # Minimum or EXACT version e.g. 4.6
#     [REQUIRED]             # Fail with error if Omnetpp is not found
#     )
#
# This module finds headers and libraries. The results are reported in variables:
#
#   Omnetpp_FOUND						- True if headers and libraries were found
#   Omnetpp_INCLUDE_DIRS				- Omnetpp include directories
#   Omnetpp_LIBRARY_DIRS				- Link directories for Omnetpp libraries
#   Omnetpp_LIBRARY						- Omnetpp libraries to be linked (defult is Omnetpp_LIBRARY_DYNAMIC_DEBUG)
#   Omnetpp_LIBRARY_DYNAMIC_DEBUG		- Omnetpp dynamic libraries with debug symbols
#   Omnetpp_LIBRARY_DYNAMIC_RELEASE		- Omnetpp dynamic libraries without debug symbols
#   Omnetpp_LIBRARY_STATIC_DEBUG		- Omnetpp static libraries with debug symbols
#   Omnetpp_LIBRARY_STATIC_RELEASE		- Omnetpp static libraries with debug symbols
#   Omnetpp_VERSION						- Omnetpp_VERSION string from
#   Omnetpp_MAJOR_VERSION				- Omnetpp major version number (X in X.y)
#   Omnetpp_MINOR_VERSION				- Omnetpp minor version number (Y in x.Y)
#   Omnetpp_SOURCE_FOLDER				- Omnetpp include directories
#
# Omnetpp CMake
# ^^^^^^^^^^^^^
#

#-------------------------------------------------------------------------------
#  FindOmnetpp functions & macros
#

############################################
#
# Check the existence of Omnetpp.
#
############################################

function(_find_omnetpp_location arg1 arg2 )

	set(PFound_var ${arg1})
	set(PRoot_var ${arg2})

	execute_process(
		COMMAND opp_configfilepath
		RESULT_VARIABLE OP_SUCCESS
		OUTPUT_VARIABLE OP_OUTPUT
	)

	if(${OP_SUCCESS} EQUAL 0)
		get_filename_component(OP_OUTPUT ${OP_OUTPUT} PATH)
		set(Root_var ${OP_OUTPUT})
		set(Found_var 1)
	else(${OP_SUCCESS} EQUAL 0)
		set(Root_var} "")
		set(Found_var 0)
	endif(${OP_SUCCESS} EQUAL 0)

	set(${PFound_var} ${Found_var} PARENT_SCOPE)
	set(${PRoot_var} ${Root_var} PARENT_SCOPE)

endfunction(_find_omnetpp_location arg1 arg2)


function(_find_omnetpp_version arg1 arg2 arg3 arg4)

	set(Root_dir ${arg1})
	set(PVersion_var ${arg2})
	set(PVersionMaj_var ${arg3})
	set(PVersionMin_var ${arg4})


	file(STRINGS "${Root_dir}/Version" Version_var REGEX "omnetpp-")
	set(_VERSION_REGEX "([0-9]+)")
	if("${Version_var}" MATCHES "omnetpp-${_VERSION_REGEX}.${_VERSION_REGEX}")
		set(VersionMaj_var "${CMAKE_MATCH_1}")
		set(VersionMin_var "${CMAKE_MATCH_2}")
		set(Version_var "${CMAKE_MATCH_1}.${CMAKE_MATCH_2}")
	endif()


	set(${PVersion_var} ${Version_var} PARENT_SCOPE)
	set(${PVersionMaj_var} ${VersionMaj_var} PARENT_SCOPE)
	set(${PVersionMin_var} ${VersionMin_var} PARENT_SCOPE)


endfunction(_find_omnetpp_version arg1 arg2 arg3 arg4)


_find_omnetpp_location( Omnetpp_FOUND Omnetpp_ROOT )

if(${Omnetpp_FOUND} EQUAL 1)
	_find_omnetpp_version( ${Omnetpp_ROOT} Omnetpp_VERSION Omnetpp_MAJOR_VERSION Omnetpp_MINOR_VERSION )

	message(STATUS "Found Omnetpp-${Omnetpp_VERSION} -> Version ${Omnetpp_MAJOR_VERSION}.${Omnetpp_MINOR_VERSION}")

	if(Omnetpp_FIND_VERSION_EXACT)
		if(NOT "${Omnetpp_VERSION}" VERSION_EQUAL "${Omnetpp_FIND_VERSION}")
			message(SEND_ERROR "Unable to find the requested Omnetpp version. Requested ${Omnetpp_FIND_VERSION}, found ${Omnetpp_VERSION}" )
		endif(NOT "${Omnetpp_VERSION}" VERSION_EQUAL "${Omnetpp_FIND_VERSION}")
	endif(Omnetpp_FIND_VERSION_EXACT)

		set(Omnetpp_SOURCE_FOLDER ${Omnetpp_ROOT} )
		set(Omnetpp_INCLUDE_DIRS  ${Omnetpp_ROOT}/include )


		file(GLOB Omnetpp_LIBRARY_GLOB  ${Omnetpp_ROOT}/lib/*.so)
		foreach(f ${Omnetpp_LIBRARY_GLOB})

			string(REGEX MATCH "lib[a-zA-Z_0-9\\-]*(d).so" result_match ${f})
			if(${CMAKE_MATCH_0} MATCHES "d")
				list(APPEND Omnetpp_LIBRARY_DYNAMIC_DEBUG ${f})
			else()
				list(APPEND Omnetpp_LIBRARY_DYNAMIC_RELEASE ${f})
			endif()

			string(REGEX MATCH "lib[a-zA-Z_0-9\\-]*(d).a" result_match ${f})
			#message(${f})
			if(${CMAKE_MATCH_0} MATCHES "d")
				list(APPEND Omnetpp_LIBRARY_STATIC_DEBUG ${f})
			else()
				list(APPEND Omnetpp_LIBRARY_STATIC_RELEASE ${f})
			endif()
		endforeach()

		set(Omnetpp_LIBRARY_DIRS ${Omnetpp_ROOT}/lib/ )
		set(Omnetpp_LIBRARY ${Omnetpp_LIBRARY_DYNAMIC_DEBUG} )

else(${Omnetpp_FOUND} EQUAL 1)
	if(Omnetpp_FIND_REQUIRED)
		message(SEND_ERROR "Unable to find the requested Omnetpp Check if omnetpp bin directory is in your path.")
	endif(Omnetpp_FIND_REQUIRED)
	message(WARNING "Not found Omnetpp")
endif(${Omnetpp_FOUND} EQUAL 1)


