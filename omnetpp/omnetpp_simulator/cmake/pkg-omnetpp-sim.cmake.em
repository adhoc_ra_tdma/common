# based on genmsg/cmake/pkg-genmsg.cmake.em

@{
import os
import sys

# split incoming variables
sources = sources_str.split(';') if sources_str != '' else []
ignore = ignore_dirs.split(';') if ignore_dirs !='' else []
include_search_paths = include_paths.split(';') if include_paths != '' else []

target_libs = target_libraries.split(';') if target_libraries != '' else []
path_libs = path_libraries.split(';') if path_libraries != '' else []


if not sources:
    print('message(WARNING "Invoking add_omnetpp_simulation() without having added any source files.\n")')

}@




message(STATUS "@(pkg_name): Generating simulation @(sim_name)")




# Copy source to a folder we can modify in the build directory
set(GENERATED_SRC_DIR  ${CMAKE_BINARY_DIR}/${CATKIN_PACKAGE_BIN_DESTINATION}/@(pkg_name)_@(sim_name)/src/)

#define where garbage output will be created
set(GENERATED_OUT_DIR  ${CMAKE_BINARY_DIR}/${CATKIN_PACKAGE_BIN_DESTINATION}/@(pkg_name)_@(sim_name)/out/)
file(MAKE_DIRECTORY ${GENERATED_OUT_DIR})

# Create opp makefile
if( CMAKE_BUILD_TYPE STREQUAL "Release" )
	set(MAKEMAKEMODE release)
ELSE(CMAKE_BUILD_TYPE STREQUAL "Release" )
	set(MAKEMAKEMODE debug)
ENDIF(CMAKE_BUILD_TYPE STREQUAL "Release")




add_custom_target(@(pkg_name)_@(sim_name)_copy_source

	ALL
	COMMAND ${CMAKE_COMMAND} -E make_directory ${GENERATED_SRC_DIR}
	COMMAND ${CMAKE_COMMAND} -E touch ${GENERATED_SRC_DIR}/DIRTY
)


file(WRITE makefrag "LDFLAGS += -Wl,--no-as-needed")



add_custom_target(@(pkg_name)_@(sim_name)_oppmakemake

	ALL

	WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}

	COMMAND opp_makemake
	-f
	-e cpp
	-O ${GENERATED_OUT_DIR}
	-o @(sim_name)
	-M ${MAKEMAKEMODE}
	@(' '.join([" -X %s" % (ign) for ign in ignore]))
	--deep
	-DSIMULATION
	@(' '.join([" -I%s" % (dep) for dep in include_search_paths]))
	-P ${PROJECT_SOURCE_DIR}
	@(' '.join([" %s " % (dep) for dep in target_libs]))
	@(' '.join([" %s " % (dep) for dep in path_libs]))

	DEPENDS @(pkg_name)_@(sim_name)_copy_source

	SOURCES
	@(' '.join([" %s" % (source) for source in sources]))
)

#file(REMOVE makefrag)

# make project
add_custom_target(@(pkg_name)_@(sim_name)_opp_make_exec

	ALL

	WORKING_DIRECTORY ${GENERATED_OUT_DIR}

	COMMAND make -C ${PROJECT_SOURCE_DIR}

	DEPENDS @(pkg_name)_@(sim_name)_oppmakemake

)




# Copy files
set(RUNTIME_SIMULATION_CONFIGS  ${CATKIN_DEVEL_PREFIX}/${CATKIN_PACKAGE_ETC_DESTINATION})
file(MAKE_DIRECTORY ${RUNTIME_SIMULATION_CONFIGS})

file(MAKE_DIRECTORY ${CATKIN_DEVEL_PREFIX}/${CATKIN_PACKAGE_BIN_DESTINATION})
 message("------------------${CATKIN_DEVEL_PREFIX}/${CATKIN_PACKAGE_BIN_DESTINATION}")

add_custom_command(TARGET @(pkg_name)_@(sim_name)_opp_make_exec
	POST_BUILD
	COMMAND ${CMAKE_COMMAND} -E copy ${GENERATED_OUT_DIR}/gcc-${MAKEMAKEMODE}/@(sim_name) ${CATKIN_DEVEL_PREFIX}/${CATKIN_PACKAGE_BIN_DESTINATION}/@(exec_name)
	#COMMAND ${CMAKE_COMMAND} -E copy_directory ${PROJECT_SOURCE_DIR}/config ${RUNTIME_SIMULATION_CONFIGS}/config
	#COMMAND ${CMAKE_COMMAND} -E copy_directory ${PROJECT_SOURCE_DIR}/ned ${RUNTIME_SIMULATION_CONFIGS}/ned
)



# Make clean
add_custom_target(@(pkg_name)_@(sim_name)_clean_oppmake
	ALL
	WORKING_DIRECTORY ${GENERATED_OUT_DIR}
	COMMAND make -C ${PROJECT_SOURCE_DIR} clean
	#COMMAND ${CMAKE_COMMAND} -E remove ${PROJECT_SOURCE_DIR}/Makefile
	DEPENDS @(pkg_name)_@(sim_name)_opp_make_exec

)







add_executable(@(pkg_name)_@(sim_name) IMPORTED)
set_property(TARGET @(pkg_name)_@(sim_name) PROPERTY IMPORTED_LOCATION ${CATKIN_DEVEL_PREFIX}/${CATKIN_PACKAGE_BIN_DESTINATION}/@(exec_name))

add_dependencies(@(pkg_name)_@(sim_name) @(pkg_name)_@(sim_name)_clean_oppmake)

#endif(MAKE_SIMULATION)

#add_custom_target(@(pkg_name)_@(sim_name)_dummy ls
#
#
#	SOURCES
#	${@(pkg_name)_@(sim_name)_CONFIG_FILES}
#	${@(pkg_name)_@(sim_name)_NED_FILES}
#	${${PROJECT_NAME}_MSG_FILES}
#	${${PROJECT_NAME}_SOURCE_FILES}
#
#)











