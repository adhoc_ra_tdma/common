# based on genmsg/cmake/pkg-genmsg.cmake.em

@{
import os
import sys

# split incoming variables
messages = messages_str.split(';') if messages_str != '' else []

dep_search_paths = include_paths_str.split(';') if include_paths_str != '' else []

if not messages:
    print('message(WARNING "Invoking generate_messages() without having added any message or service file before.\nYou should either add add_message_files() and/or add_service_files() calls or remove the invocation of generate_messages().")')

pkg_include_dest_dir=include_dest_dir+'/'+pkg_name+'/'+sim_name

}@
message(STATUS "@(pkg_name) (@(sim_name)): @(len(messages)) messages")

set(MSG_I_FLAGS "@(';'.join(["-I%s" % (dep) for dep in dep_search_paths]))")


#add_custom_target(@(pkg_name)_@(sim_name)_omnetpp_simulator_generate_messages ALL)



file(MAKE_DIRECTORY @(pkg_include_dest_dir))



@[for f in messages]@




	get_filename_component(_filename "@(f)" NAME_WE)

	set(OMNETPP_MSG_GENERATED_NAME ${_filename})
	set(OMNETPP_SRC_EXTENSION "@(msg_suffix).cpp")
	set(OMNETPP_HDR_EXTENSION "@(msg_suffix).hpp")

	set(OMNETPP_SRC_NAME ${OMNETPP_MSG_GENERATED_NAME}${OMNETPP_SRC_EXTENSION})
	set(OMNETPP_HDR_NAME ${OMNETPP_MSG_GENERATED_NAME}${OMNETPP_HDR_EXTENSION})

	set(OMNETPP_SRC_OUTPUT_FILE @(pkg_include_dest_dir)/${OMNETPP_SRC_NAME})
	set(OMNETPP_HDR_OUTPUT_FILE @(pkg_include_dest_dir)/${OMNETPP_HDR_NAME})

	add_custom_command(OUTPUT ${OMNETPP_SRC_OUTPUT_FILE} ${OMNETPP_HDR_OUTPUT_FILE}
		COMMAND opp_msgc
		-s ${OMNETPP_SRC_EXTENSION}
		-t ${OMNETPP_HDR_EXTENSION}
		-h -V
		${MSG_I_FLAGS}
		@f
		WORKING_DIRECTORY @(pkg_include_dest_dir)
	)

	list(APPEND @(pkg_name)_@(sim_name)_OMNETPP_ALL_GEN_OUTPUT_FILES
		${OMNETPP_SRC_OUTPUT_FILE} ${OMNETPP_HDR_OUTPUT_FILE}
	)

@[end for]@# messages

# register target for catkin_package(EXPORTED_TARGETS)


  if(NOT @(pkg_name)_@(sim_name)_omnetpp_APPENDED_INCLUDE_DIRS)
	# make sure we can find generated messages and that they overlay all other includes
	include_directories(BEFORE @(include_dest_dir))
	# pass the include directory to catkin_package()
	list(APPEND ${PROJECT_NAME}_INCLUDE_DIRS @(include_dest_dir))
	set(@(pkg_name)_@(sim_name)_omnetpp_APPENDED_INCLUDE_DIRS TRUE)
  endif()

add_library(@(pkg_name)_@(sim_name)_omnetpp_simulator_generate_messages ${@(pkg_name)_@(sim_name)_OMNETPP_ALL_GEN_OUTPUT_FILES})


list(APPEND ${PROJECT_NAME}_LIBRARIES @(pkg_name)_@(sim_name)_omnetpp_simulator_generate_messages)
message("-- Message generation target complete")
# install generated code
#install(
#	DIRECTORY @(pkg_include_dest_dir)
#	DESTINATION ${CATKIN_PACKAGE_INCLUDE_DESTINATION}
#)












