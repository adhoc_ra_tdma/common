# based on genmsg/cmake/genmsg-extras.cmake.in
# generated from omnet_simulation/cmake/omnetpp-msg-extras.cmake.in

if(_OMNETPP_EXTRAS_INCLUDED_)
  return()
endif()
set(_OMNETPP_EXTRAS_INCLUDED_ TRUE)

include(CMakeParseArguments)



macro(_prepend_path ARG_PATH ARG_FILES ARG_OUTPUT_VAR)
	cmake_parse_arguments(ARG "UNIQUE" "" "" ${ARGN})
	if(ARG_UNPARSED_ARGUMENTS)
		message(FATAL_ERROR "_prepend_path() called with unused arguments: ${ARG_UNPARSED_ARGUMENTS}")
	endif()
	# todo, check for proper path, slasheds, etc
	set(${ARG_OUTPUT_VAR} "")
	foreach(_file ${ARG_FILES})
		set(_value ${ARG_PATH}/${_file})
		list(FIND ${ARG_OUTPUT_VAR} ${_value} _index)
		if(NOT ARG_UNIQUE OR _index EQUAL -1)
			list(APPEND ${ARG_OUTPUT_VAR} ${_value})
		endif()
	endforeach()
endmacro()









macro(__add_omnetpp_msg)

	set(options "")
	set(oneValueArgs SUFFIX NAME)
	set(multiValueArgs FILES)
	cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )


	if(ARG_UNPARSED_ARGUMENTS)
		message(FATAL_ERROR "__add_omnetpp_msg() called with unused arguments: ${ARG_UNPARSED_ARGUMENTS}")
	endif()

	if(NOT ARG_NAME)
		message(FATAL_ERROR "__add_omnetpp_msg() simulation name was not defined.")
	endif()

	if(NOT ARG_SUFFIX)
		set(ARG_SUFFIX "_m")
	endif()

	if(ARG_FILES)



@[if DEVELSPACE]@
	# cmake dir in develspace
	set(omnetpp_simulator_CMAKE_DIR "@(CMAKE_CURRENT_SOURCE_DIR)/cmake")
@[else]@
	# cmake dir in installspace
	set(omnetpp_simulator_CMAKE_DIR "@(PKG_CMAKE_DIR)")
@[end if]@

	# ensure that destination variables are initialized
	catkin_destinations()

	foreach(msg ${ARG_FILES})
		get_filename_component(FILEPATH ${msg} PATH)
		list(APPEND MESSAGE_DIRECTORIES ${FILEPATH})
	endforeach()
	list(REMOVE_DUPLICATES MESSAGE_DIRECTORIES)


	# mark that generate_messages() was called in order to detect wrong order of calling with catkin_python_setup()
	set(${PROJECT_NAME}_OMNETPP_GENERATE TRUE)


	get_directory_property(DIRS_TO_INCLUDE INCLUDE_DIRECTORIES)
	set(OMNETPP_DESTINATION_INCLUDE_GEN_DIR "${CATKIN_DEVEL_PREFIX}/${CATKIN_GLOBAL_INCLUDE_DESTINATION}")
	set(OMNETPP_DESTINATION_LIB_GEN_DIR "${CATKIN_DEVEL_PREFIX}/${CATKIN_GLOBAL_INCLUDE_DESTINATION}")

	em_expand(${omnetpp_simulator_CMAKE_DIR}/pkg-omnetpp-msg.context.in
		${CMAKE_CURRENT_BINARY_DIR}/cmake/${PROJECT_NAME}-${ARG_NAME}-omnetpp-msg-context.py
		${omnetpp_simulator_CMAKE_DIR}/pkg-omnetpp-msg.cmake.em
		${CMAKE_CURRENT_BINARY_DIR}/cmake/${PROJECT_NAME}-${ARG_NAME}-omnetpp-msg.cmake)
	include(${CMAKE_CURRENT_BINARY_DIR}/cmake/${PROJECT_NAME}-${ARG_NAME}-omnetpp-msg.cmake)


	endif()

endmacro()

macro(__add_omnetpp_ned_dir)

	set(options "")
	set(oneValueArgs "")
	set(multiValueArgs DIRECTORIES)
	cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )


	if(ARG_UNPARSED_ARGUMENTS)
		message(FATAL_ERROR "__add_omnetpp_ned_dir() called with unused arguments: ${ARG_UNPARSED_ARGUMENTS}")
	endif()


	if(NOT ARG_DIRECTORIES)
		message(FATAL_ERROR "__add_omnetpp_ned_dir() called without directories defined.")
	endif()


	foreach(dir ${ARG_DIRECTORIES})
		if(NOT IS_ABSOLUTE "${dir}")
			set(dir "${CMAKE_CURRENT_SOURCE_DIR}/${dir}")
		endif()
		if(NOT IS_DIRECTORY ${dir})
			message(FATAL_ERROR "__add_omnetpp_ned_dir() directory not found: ${dir}")
		endif()

		#add directory no ned dir path
		SET(ENV{OMNETPP_NED_PATH} "$ENV{OMNETPP_NED_PATH}::${dir}")

	endforeach()

endmacro()


macro(add_omnetpp_simulation)

	if(${${PROJECT_NAME}_CATKIN_PACKAGE})
		message(FATAL_ERROR "add_omnetpp_simulation() needs to be called before catkin_package()")
	endif()

	# ensure that destination variables are initialized
	catkin_destinations()


	set(options SIMULATION LIBRARY)
	set(oneValueArgs NAME CONFIG )
	set(multiValueArgs SOURCES MESSAGES LIBRARIES NED_PATHS)
	cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )


	if(NOT ARG_NAME)
		set(SIMULATION_NAME __${PROJECT_NAME})
	else()
		set(SIMULATION_NAME __${ARG_NAME})
	endif()


	if(ARG_SIMULATION OR ARG_LIBRARY)
		if(NOT ARG_SOURCES)
			message(FATAL_ERROR "No sources were specified on call to add_omnetpp_simulation()")
		else()
			foreach(source ${ARG_SOURCES})
				set(abs_source "${source}")
				if(NOT IS_ABSOLUTE "${source}")
					set(abs_source "${CMAKE_CURRENT_SOURCE_DIR}/${source}")
				endif()
				list(APPEND SOURCE_FILES ${abs_source})
			endforeach()
		endif()
	endif()



	if(ARG_MESSAGES)
		set(MESSAGE_DIRECTORIES)
		set(ABSOLUTE_MESSAGES)

		foreach(msg ${ARG_MESSAGES})
			get_filename_component(FILEPATH ${msg} PATH)
			if(NOT IS_ABSOLUTE "${FILEPATH}")
				set(FILEPATH "${CMAKE_CURRENT_SOURCE_DIR}/${FILEPATH}")
				set(msg_abs "${CMAKE_CURRENT_SOURCE_DIR}/${msg}")
			endif()
			list(APPEND MESSAGE_DIRECTORIES "${FILEPATH}")
			list(APPEND ABSOLUTE_MESSAGES "${msg_abs}")
		endforeach()

		list(REMOVE_DUPLICATES MESSAGE_DIRECTORIES)


		__add_omnetpp_msg(NAME ${ARG_NAME} FILES ${ABSOLUTE_MESSAGES})

		list(APPEND IGNORE_DIRECTORIES ${MESSAGE_DIRECTORIES})
		message(STATUS "Ignoring the following message directories:")
		foreach(msg ${IGNORE_DIRECTORIES})
			message(STATUS ${msg})
		endforeach(msg ${IGNORE_DIRECTORIES})

		# Dont forget the generated message libraries
		list(APPEND ARG_LIBRARIES ${PROJECT_NAME}_${ARG_NAME}_omnetpp_simulator_generate_messages)
	endif()


	if(ARG_LIBRARIES)

		set(TARGET_LIBRARIES)
		set(PATH_LIBRARIES)

		foreach(lib ${ARG_LIBRARIES})

			if(TARGET ${lib})
				list(APPEND TARGET_LIBRARIES $<TARGET_FILE:${lib}>)

			else(TARGET ${lib})
				get_filename_component(FILENAME ${lib} NAME_WE)
				get_filename_component(FILEPATH ${lib} PATH)
				string(SUBSTRING ${FILENAME} 3 -1 FILENAME)
				list(APPEND PATH_LIBRARIES -L${FILEPATH} -l${FILENAME})

			endif(TARGET ${lib})
		endforeach()

		if(TARGET_LIBRARIES)
			list(REMOVE_DUPLICATES TARGET_LIBRARIES)
		endif()
		if(PATH_LIBRARIES)
			list(REMOVE_DUPLICATES PATH_LIBRARIES)
		endif()
	endif()

	if(ARG_NED_PATHS)
		__add_omnetpp_ned_dir( DIRECTORIES ${ARG_NED_PATHS} )
	endif()

@[if DEVELSPACE]@
	# cmake dir in develspace
	set(omnetpp_simulator_CMAKE_DIR "@(CMAKE_CURRENT_SOURCE_DIR)/cmake")
@[else]@
	# cmake dir in installspace
	set(omnetpp_simulator_CMAKE_DIR "@(PKG_CMAKE_DIR)")
@[end if]@

	if(ARG_UNPARSED_ARGUMENTS)
		message(FATAL_ERROR "add_omnetpp_simulation() called with unused arguments: ${ARG_UNPARSED_ARGUMENTS}")
	endif()

	if(ARG_SIMULATION)
		if(ARG_LIBRARY)
			message(FATAL_ERROR "add_omnetpp_simulation(): Cannot be SIMULATION and LIBRARY simultaneously!")
		endif()
		if(ARG_CONFIG)
			message(STATUS "Creating simulation -- ${PROJECT_NAME}:${ARG_NAME}")
			set(CONFIG_FILE ${ARG_CONFIG})


			get_directory_property(DIRS_TO_INCLUDE INCLUDE_DIRECTORIES)


			em_expand(${omnetpp_simulator_CMAKE_DIR}/pkg-omnetpp-sim.context.in
				${CMAKE_CURRENT_BINARY_DIR}/cmake/${PROJECT_NAME}-${ARG_NAME}-omnetpp-sim-context.py
				${omnetpp_simulator_CMAKE_DIR}/pkg-omnetpp-sim.cmake.em
				${CMAKE_CURRENT_BINARY_DIR}/cmake/${PROJECT_NAME}-${ARG_NAME}-omnetpp-sim.cmake)
			include(${CMAKE_CURRENT_BINARY_DIR}/cmake/${PROJECT_NAME}-${ARG_NAME}-omnetpp-sim.cmake)

			set(NED_PATH "$ENV{OMNETPP_NED_PATH}")

			configure_file(
				${omnetpp_simulator_CMAKE_DIR}/execute_simulation_template.in
				${CATKIN_DEVEL_PREFIX}/${CATKIN_PACKAGE_BIN_DESTINATION}/${ARG_NAME}
					@@ONLY
			)
		else()
			message(FATAL_ERROR "No config file was specified on call to add_omnetpp_simulation()")

		endif()
	endif()

	if(ARG_LIBRARY)



		add_library(${PROJECT_NAME}_${ARG_NAME}_omnetpp_library
			${SOURCE_FILES}
		)
		list(APPEND ${PROJECT_NAME}_LIBRARIES ${PROJECT_NAME}_${ARG_NAME}_omnetpp_library)
		list(APPEND catkin_LIBRARIES ${PROJECT_NAME}_${ARG_NAME}_omnetpp_library)

		if(TARGET ${PROJECT_NAME}_${ARG_NAME}_omnetpp_simulator_generate_messages)
			add_dependencies(${PROJECT_NAME}_${ARG_NAME}_omnetpp_library ${PROJECT_NAME}_${ARG_NAME}_omnetpp_simulator_generate_messages)
		endif()

		#add_dependencies(${PROJECT_NAME}_${ARG_NAME}_omnetpp_library
		#	${ARG_LIBRARIES}
		#)


	endif()

endmacro(add_omnetpp_simulation)





