#ifndef __SIMULATION_TIMER_HPP__
#define __SIMULATION_TIMER_HPP__

#include <time_interfaces/timerI.hpp>
#include <memory>
namespace common
{
namespace simulation_interface
{
class timer
{
	public:

		using timer_interface=common::time::interfaces::TimerI;
		using duration_t=common::time::interfaces::duration_t;

		static std::unique_ptr<timer_interface> new_timer();

	private:
		timer()=delete;

		timer(const timer &other)=delete;
		timer &operator=(const timer &other)=delete;

		timer(timer &&other)=delete;
		timer &operator=(timer &&other)=delete;

		virtual ~timer()=delete;



};

class counter
{
	public:

		using counter_interface=common::time::interfaces::CounterI;
		using duration_t=common::time::interfaces::duration_t;

		static std::unique_ptr<counter_interface> new_counter();

	private:
		counter()=delete;

		counter(const timer &other)=delete;
		counter &operator=(const timer &other)=delete;

		counter(timer &&other)=delete;
		counter &operator=(timer &&other)=delete;

		virtual ~counter()=delete;



};
}
}

#endif
