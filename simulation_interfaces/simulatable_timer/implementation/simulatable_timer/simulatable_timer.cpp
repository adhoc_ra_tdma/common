#include <simulatable_timer/simulatable_timer.hpp>

#ifdef SIMULATION

#include <omnetpp_timer/omnetpp_timer.hpp>
#include <omnetpp_timer/omnetpp_counter.hpp>
#else
#include <linux_timer/timer.hpp>
#include <linux_timer/counter.hpp>
#endif


namespace common
{
namespace simulation_interface
{

std::unique_ptr<timer::timer_interface>
timer::new_timer()
{

#ifdef SIMULATION
	std::unique_ptr<timer::timer_interface> ret(new common::omnetpp::timer::Timer);
#else
	std::unique_ptr<timer::timer_interface> ret(new common::time::linux_implementation::timer::Timer);
#endif
	return ret;
}

std::unique_ptr<counter::counter_interface>
counter::new_counter()
{

#ifdef SIMULATION
	std::unique_ptr<counter::counter_interface> ret(new common::omnetpp::timer::Counter);
#else
	std::unique_ptr<counter::counter_interface> ret(new common::time::linux_implementation::timer::Counter);
#endif
	return ret;
}


}
}
