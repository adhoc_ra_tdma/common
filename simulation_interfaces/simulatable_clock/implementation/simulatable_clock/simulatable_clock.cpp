#include <simulatable_clock/simulatable_clock.hpp>


#include <chrono>
#ifdef SIMULATION
#include <omnetpp_clock/omnetpp_clock.hpp>
#else

#endif


namespace common
{
namespace simulation_interface
{

simulation_clock::time_point simulation_clock::now() noexcept
{

#ifdef SIMULATION
	common::omnetpp::clock::omnetpp_clock::duration other_since_epoch=common::omnetpp::clock::omnetpp_clock::now().time_since_epoch();
	duration my_since_epoch = std::chrono::duration_cast<duration>(other_since_epoch);
	return time_point(my_since_epoch);

#else

	std::chrono::high_resolution_clock::duration other_since_epoch=std::chrono::high_resolution_clock::now().time_since_epoch();
	duration my_since_epoch = std::chrono::duration_cast<duration>(other_since_epoch);
	return time_point(my_since_epoch);

#endif

}

time_t simulation_clock::to_time_t(const time_point &__t) noexcept
{

#ifdef SIMULATION

	duration my_since_epoch=__t.time_since_epoch();
	common::omnetpp::clock::omnetpp_clock::duration other_since_epoch = std::chrono::duration_cast<common::omnetpp::clock::omnetpp_clock::duration>(my_since_epoch);

	return common::omnetpp::clock::omnetpp_clock::to_time_t(common::omnetpp::clock::omnetpp_clock::time_point(other_since_epoch));

#else
	duration my_since_epoch=__t.time_since_epoch();
	std::chrono::high_resolution_clock::duration other_since_epoch = std::chrono::duration_cast<std::chrono::high_resolution_clock::duration>(my_since_epoch);

	return std::chrono::high_resolution_clock::to_time_t(std::chrono::high_resolution_clock::time_point(other_since_epoch));


#endif

}

simulation_clock::time_point simulation_clock::from_time_t(time_t __t) noexcept
{

#ifdef SIMULATION
	common::omnetpp::clock::omnetpp_clock::duration other_since_epoch=common::omnetpp::clock::omnetpp_clock::from_time_t(__t).time_since_epoch();
	duration my_since_epoch = std::chrono::duration_cast<duration>(other_since_epoch);

	return time_point(my_since_epoch);

#else
	std::chrono::high_resolution_clock::duration other_since_epoch=std::chrono::high_resolution_clock::from_time_t(__t).time_since_epoch();
	duration my_since_epoch = std::chrono::duration_cast<duration>(other_since_epoch);

	return time_point(my_since_epoch);

#endif

}




}
}
