#ifndef __SIMULATION_CLOCK_HPP__
#define __SIMULATION_CLOCK_HPP__

#include <chrono>
namespace common
{
namespace simulation_interface
{


struct simulation_clock
{

		typedef std::chrono::nanoseconds								duration;
		typedef duration::rep											rep;
		typedef duration::period										period;
		typedef std::chrono::time_point<simulation_clock, duration> 	time_point;

		static_assert(simulation_clock::duration::min()
					  < simulation_clock::duration::zero(),
					  "a clock's minimum duration cannot be less than its epoch");

		static constexpr bool is_steady = false;

		static time_point now() noexcept;

		// Map to C API
		static std::time_t to_time_t(const time_point& __t) noexcept;

		static time_point from_time_t(std::time_t __t) noexcept;
};


}
}



#endif
