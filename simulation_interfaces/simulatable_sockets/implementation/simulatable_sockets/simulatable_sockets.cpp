#include <simulatable_sockets/simulatable_sockets.hpp>


#ifdef SIMULATION

#include <omnetpp_sockets/udp_unicast_socket.hpp>
#include <omnetpp_sockets/udp_multicast_socket.hpp>
#include <omnetpp_sockets/if_device.hpp>
#include <omnetpp_sockets/udp_endpoint.hpp>
#else
#include <linux_sockets/udp_unicast_socket.hpp>
#include <linux_sockets/udp_multicast_socket.hpp>
#include <linux_sockets/udp_callback_sockets.hpp>
#include <linux_sockets/udp_future_sockets.hpp>
#include <linux_sockets/if_device.hpp>
#include <linux_sockets/udp_endpoint.hpp>
#endif



namespace common
{
namespace simulation_interface
{



std::unique_ptr<udp_sockets::unicast_socket_interface>
udp_sockets::new_udp_unicast_socket(const uint16_t port)
{

#ifdef SIMULATION

	std::unique_ptr<udp_sockets::unicast_socket_interface> ret( new common::omnetpp::sockets::udp_unicast_socket(port));

#else

	using udp_unicast_socket_implementation=common::network::linux_implementation::sockets::udp_unicast_socket_implementation;
	using udp_unicast_socket=common::network::linux_implementation::sockets::udp_callback_socket<udp_unicast_socket_implementation, uint16_t>;

	std::unique_ptr<udp_sockets::unicast_socket_interface> ret( new udp_unicast_socket(port) );

#endif

	return ret;
}

std::unique_ptr<udp_sockets::multicast_socket_interface>
udp_sockets::new_udp_multicast_socket(const std::string &ifname, const common::network::generics::udp_endpoint &multicast_endpoint)
{
#ifdef SIMULATION

	common::omnetpp::sockets::udp_endpoint implementation_endpoint;
	implementation_endpoint.setAddr(multicast_endpoint.getAddr());
	implementation_endpoint.setPort(multicast_endpoint.getPort());
	common::omnetpp::sockets::if_device dev(ifname);
	std::unique_ptr<udp_sockets::multicast_socket_interface> ret(new common::omnetpp::sockets::udp_multicast_socket(dev,implementation_endpoint));

#else

	using udp_multicast_socket_implementation=common::network::linux_implementation::sockets::udp_multicast_socket_implementation;
	using if_device=common::network::linux_implementation::sockets::if_device;
	using udp_endpoint=common::network::linux_implementation::sockets::udp_endpoint;

	using udp_multicast_socket=common::network::linux_implementation::sockets::udp_callback_socket<udp_multicast_socket_implementation, if_device,udp_endpoint>;

	common::network::linux_implementation::sockets::udp_endpoint implementation_endpoint;
	implementation_endpoint.setAddr(multicast_endpoint.getAddr());
	implementation_endpoint.setPort(multicast_endpoint.getPort());

	common::network::linux_implementation::sockets::if_device dev(ifname);

	std::unique_ptr<udp_sockets::multicast_socket_interface> ret(new udp_multicast_socket(dev,implementation_endpoint));

#endif

	return ret;
}

std::unique_ptr<udp_sockets::network_device_interface>
udp_sockets::new_if_device(std::string ifname)
{
#ifdef SIMULATION
	std::unique_ptr<udp_sockets::network_device_interface> ret(new common::omnetpp::sockets::if_device(ifname));
#else
	std::unique_ptr<udp_sockets::network_device_interface> ret(new common::network::linux_implementation::sockets::if_device(ifname));
#endif
	return ret;
}

std::unique_ptr<udp_sockets::udp_endpoint_interface>
udp_sockets::new_udp_endpoint()
{
#ifdef SIMULATION
	std::unique_ptr<udp_sockets::udp_endpoint_interface> ret(new common::omnetpp::sockets::udp_endpoint);
#else
	std::unique_ptr<udp_sockets::udp_endpoint_interface> ret(new common::network::linux_implementation::sockets::udp_endpoint);
#endif
	return ret;
}

}
}
