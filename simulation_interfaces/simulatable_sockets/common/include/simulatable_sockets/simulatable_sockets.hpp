#ifndef __SIMULATABLE_SOCKETS_HPP__
#define __SIMULATABLE_SOCKETS_HPP__

#include <network_interfaces/udp_multicast_socketI.hpp>
#include <network_interfaces/if_deviceI.hpp>
#include <network_generics/udp_message.hpp>

#include <memory>
namespace common
{
namespace simulation_interface
{
class udp_sockets
{
	public:

		using unicast_socket_interface=common::network::interfaces::udp_callback_socket_interface<common::network::generics::MessageUDP>;
		using multicast_socket_interface=common::network::interfaces::udp_callback_socket_interface<common::network::generics::MessageUDP>;
		using network_device_interface=common::network::interfaces::if_deviceI;
		using udp_endpoint_interface=common::network::interfaces::udp_endpointI;

		static std::unique_ptr<unicast_socket_interface> new_udp_unicast_socket(const uint16_t port);
		static std::unique_ptr<multicast_socket_interface> new_udp_multicast_socket(const std::string &ifname, const common::network::generics::udp_endpoint &multicast_endpoint);

		static std::unique_ptr<network_device_interface> new_if_device(std::string ifname);
		static std::unique_ptr<udp_endpoint_interface> new_udp_endpoint();


	private:
		udp_sockets()=delete;

		udp_sockets(const udp_sockets &other)=delete;
		udp_sockets &operator=(const udp_sockets &other)=delete;

		udp_sockets(udp_sockets &&other)=delete;
		udp_sockets &operator=(udp_sockets &&other)=delete;

		virtual ~udp_sockets()=delete;



};
}
}



#endif
