#ifndef __BOOSTSERIALBYTESTREAM_HPP__
#define __BOOSTSERIALBYTESTREAM_HPP__

#include <boost_serial_port/serial_port.hpp>
#include <string>

namespace common
{
namespace SerialPort
{
class serialByteStream:protected serialPort
{
	protected:
		typedef serialPort super;
		
	public:
		typedef super::baud_rate baud_rate;
		typedef super::character_size character_size;
		typedef super::stop_bits stop_bits;
		typedef super::parity parity;
		serialByteStream(const char* pName, baud_rate bRate=baud_rate(115200), character_size bNumber=character_size(8), parity par=parity(parity::none), stop_bits sBits=stop_bits(stop_bits::one));

		~serialByteStream();

		virtual void async_read(std::function<void(uint8_t)> function);
		virtual void read(uint8_t &data);

		virtual void write(const uint8_t sendData);
		virtual std::future<void> async_write(const uint8_t sendData);

		virtual	void async_stop();





	private:

};
}

}

#endif

