#ifndef __BOOSTSERIALPORT_HPP__
#define __BOOSTSERIALPORT_HPP__

#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/function.hpp>
#include <cstdint>
#include <thread>
#include <functional>
#include <future>
using namespace std;
namespace common
{
namespace SerialPort
{
class serialPort
{
	public:

		typedef boost::asio::serial_port_base::baud_rate baud_rate;
		typedef boost::asio::serial_port_base::character_size character_size;
		typedef boost::asio::serial_port_base::stop_bits stop_bits;
		typedef boost::asio::serial_port_base::parity parity;

		serialPort(const char* pName, baud_rate bRate=baud_rate(115200), character_size bNumber=character_size(8), parity par=parity(parity::none), stop_bits sBits=stop_bits(stop_bits::one));
		~serialPort();

		virtual void async_read(std::size_t size, std::function<void( void * )> function);
		virtual void async_read(std::size_t size, std::function<void(void *, bool)> function);
		virtual void read(void *buffer, size_t size);

		virtual void write(const void *data, std::size_t size);
		virtual std::future<void> async_write(const void* data, std::size_t size);
		virtual void async_stop();

		boost::asio::serial_port &getBoostSerialPort();

	private:
		void cb(const boost::system::error_code&, std::size_t);
		void cbWithError(const boost::system::error_code&, std::size_t);

	private:
		std::function<void ( void *, bool )> cbWithErrorFunction;
		std::function<void ( void * )> cbFunction;
		boost::asio::io_service io;
		boost::asio::io_service::work work;
		std::thread t;
		void *localBuffer;

	protected:
		boost::asio::serial_port sp;

};
}
}
#endif
