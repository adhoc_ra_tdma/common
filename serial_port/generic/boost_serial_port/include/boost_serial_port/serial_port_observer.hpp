#ifndef SERIAL_PORT_OBS_TESTING
#warning NOT STABLE
#endif

#ifndef __BOOSTSERIAL_PORT_OBSERVER_HPP__
#define __BOOSTSERIAL_PORT_OBSERVER_HPP__

#include <exception>
#include <map>
#include <string>
#include <functional>
#include <set>


namespace common
{
namespace serial_port
{


class SerialPortManager
{
		public:

};

class ObserverRegistration
{
	public:

		typedef std::set<std::function<void(char)>>::iterator id;

		id attach(const std::string port, std::function<void(char)> callback);
		void dettach(const std::string port, id iterator);

	private:
		std::map<std::string, std::set<std::function<void(char)>>> map_of_observers;

};

}
}

#endif
