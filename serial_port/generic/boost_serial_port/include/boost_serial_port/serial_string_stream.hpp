#ifndef __BOOSTSERIALSTRINGSTREAM_HPP__
#define __BOOSTSERIALSTRINGSTREAM_HPP__

#include <boost_serial_port/serial_byte_stream.hpp>
#include <string>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
using std::string;
namespace common
{
namespace SerialPort
{
class serialStringStream:protected serialByteStream
{


	protected:
		typedef serialByteStream super;

	public:
		typedef super::baud_rate baud_rate;
		typedef super::character_size character_size;
		typedef super::stop_bits stop_bits;
		typedef super::parity parity;
		serialStringStream(const char* pName, baud_rate bRate=baud_rate(115200), character_size bNumber=character_size(8), parity par=parity(parity::none), stop_bits sBits=stop_bits(stop_bits::one));

		~serialStringStream();

		virtual void async_read(std::function<void(std::string)> &function, uint8_t terminator='\n');
		virtual void read(std::string &data, uint8_t terminator='\n');

		virtual void write(const std::string sendData);
		virtual std::future<void> async_write(const std::string sendData);

		virtual	void async_stop();

	private:

		std::string str_buffer;

};
}
}
#endif
