#define SERIAL_PORT_OBS_TESTING
#include <boost_serial_port/serial_port_observer.hpp>
#undef SERIAL_PORT_OBS_TESTING


#ifndef debugLevel
#define debugLevel lInfo
#endif
#include <debug_headers/debug.hpp>

namespace common
{
namespace serial_port
{




	ObserverRegistration::id ObserverRegistration::attach(const std::string port, std::function<void(char)> callback)
	{
		std::pair<std::set<std::function<void(char)>>::iterator,bool> ret;

		try
		{
			std::set<std::function<void(char)>> &list_of_callbacks=map_of_observers.at(port);
			//ret=list_of_callbacks.insert(callback);
			if(ret.second==false)
			{
				PINFO(std::cout, "Callback already attached, doing nothing..."<<std::endl);
			}

		}
		catch(std::out_of_range)
		{
			PINFO(std::cout, "Port "<<port<<" was not initialised, you may not receve information..."<<std::endl);
			std::set<std::function<void(char)>> list_of_callbacks;
			//ret=list_of_callbacks.insert(callback);
		//	map_of_observers.insert(port, list_of_callbacks);
		}
		return ret.first;
	}

	void ObserverRegistration::dettach(const std::string port, ObserverRegistration::id iterator)
	{
	}


}
}
