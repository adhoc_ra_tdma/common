#include <boost_serial_port/serial_byte_stream.hpp>
#ifndef debugLevel
#define debugLevel lError
#endif
#include <debug_headers/debug.hpp>

namespace common
{
namespace SerialPort
{

serialByteStream::serialByteStream(const char* pName, baud_rate bRate, character_size bNumber, parity par, stop_bits sBits):
	serialPort(pName, bRate, bNumber, par, sBits)
{
}

serialByteStream::~serialByteStream()
{
	async_stop();
}

void serialByteStream::async_read(std::function<void( uint8_t )> function)
{

	std::function<void(void *, bool)> fun=[function](void *data, bool success)
	{
		if(success)
		{
			uint8_t data_received=*(uint8_t *)data;
			function(data_received);
		}
	};
	super::async_read(1,fun);
}

void serialByteStream::read(uint8_t &data)
{
	async_stop();
	return super::read(&data,1);
}

void serialByteStream::write(const uint8_t sendData)
{
	return super::write(&sendData, 1);

}

std::future<void> serialByteStream::async_write(const uint8_t sendData)
{
	return serialPort::async_write(&sendData, 1);
}


void serialByteStream::async_stop()
{
	return serialPort::async_stop();
}

}
}
