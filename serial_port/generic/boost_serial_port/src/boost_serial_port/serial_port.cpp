#include "boost_serial_port/serial_port.hpp"
#include <iostream>


#ifndef debugLevel
#define debugLevel lError
#endif
#include <debug_headers/debug.hpp>

using namespace std;
using std::cout;
using std::endl;


namespace common
{
namespace SerialPort
{



serialPort::serialPort(const char* pName, baud_rate bRate, character_size bNumber, parity par, stop_bits sBits):
	work(io),
	t([this](){this->io.run();}),
	localBuffer(nullptr),
	sp(io)
{
	boost::system::error_code ec;
	// boost::asio::serial_port_base::baud_rate @n
	sp.set_option( bRate, ec);
	// boost::asio::serial_port_base::flow_control @n
	// always none...
	//sp.set_option( fCtrl, ec);
	// boost::asio::serial_port_base::character_size
	sp.set_option( bNumber, ec);
	// boost::asio::serial_port_base::parity @n
	sp.set_option( par, ec );
	// boost::asio::serial_port_base::stop_bits @n
	sp.set_option( sBits, ec );
	sp.open(pName, ec);
	//if(ec.value()!=)
}

serialPort::~serialPort()
{
	PDEBUG(std::cout, "async_stop");
	async_stop();
	io.stop();
	PDEBUG(std::cout, "io stop");
	while(!io.stopped()) usleep(1);
	t.join();
	PDEBUG(std::cout, "t joined");
}



void serialPort::async_stop()
{
	sp.cancel();
	if(localBuffer!=nullptr)
	{
		free(localBuffer);
		localBuffer=nullptr;
	}
}

void serialPort::async_read(std::size_t size, std::function<void(void *)> function)
{
	cbFunction=function;
	async_stop();
	localBuffer=malloc(size);
	auto cb_fun=[this](const boost::system::error_code &ec , size_t size){this->cb(ec, size);};
	sp.async_read_some(boost::asio::buffer(localBuffer,size), cb_fun);
}

void serialPort::async_read(std::size_t size, std::function<void( void * , bool )> function)
{
	cbWithErrorFunction=function;
	sp.cancel();
	if(localBuffer!=nullptr)
	{
		free(localBuffer);
		localBuffer=nullptr;
	}
	localBuffer=malloc(size);
	auto cb_fun=[this](const boost::system::error_code &ec , size_t size){this->cbWithError(ec, size);};
	sp.async_read_some(boost::asio::buffer(localBuffer,size), cb_fun);
}

void serialPort::read(void *buffer, size_t size)
{
	sp.cancel();
	boost::system::error_code ec;
	sp.read_some(boost::asio::buffer(buffer,size),ec);
	if(ec.value()!=boost::asio::error::operation_aborted)
	{
		PERROR(std::cout,"Reading serial port: "<<ec.message());
	}
	else
	{
		PINFO(std::cout,"Reading serial port: "<<ec.message());
	}
}

void serialPort::write(const void *data, size_t size)
{
	sp.write_some(boost::asio::buffer(data, size));
}

std::future<void> serialPort::async_write(const void *data, size_t size)
{
	std::promise<void> prom;
	std::future<void> future_promised=prom.get_future();
	auto func=[this](const void *data, size_t size, std::promise<void> p)
	{
		this->write(data,size);
		p.set_value();
	};
	std::thread tmp(func, data, size, std::move(prom));
	tmp.detach();
	//tmp.join();
	return future_promised;
}


boost::asio::serial_port &serialPort::getBoostSerialPort()
{
	return sp;
}

void serialPort::cb(const boost::system::error_code &ec , size_t size)
{
	if(ec == boost::system::errc::success)
	{
		if(localBuffer!=nullptr)
		{
			cbFunction( localBuffer );
		}
	}
	if(ec != boost::asio::error::operation_aborted)
	{
		auto cb_fun=[this](const boost::system::error_code &ec , size_t size){this->cb(ec, size);};
		sp.async_read_some(boost::asio::buffer(localBuffer,size), cb_fun);
	}

}

void serialPort::cbWithError(const boost::system::error_code &ec, size_t size)
{
	if(ec == boost::system::errc::success)
	{
		PDEBUG(std::cout, "success received");
		if(localBuffer!=nullptr)
		{
			cbWithErrorFunction( localBuffer , true);
		}
	}
	else
	{
		cbWithErrorFunction( nullptr, false);
	}
	if(ec != boost::asio::error::operation_aborted)
	{
		auto cb_fun=[this](const boost::system::error_code &ec , size_t size){this->cbWithError(ec, size);};
		sp.async_read_some(boost::asio::buffer(localBuffer,size), cb_fun);
	}
	else
	{
		PDEBUG(std::cout, "interrupted");
	}
}

}

}



