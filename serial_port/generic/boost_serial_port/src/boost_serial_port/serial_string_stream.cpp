#include <boost_serial_port/serial_string_stream.hpp>

#ifndef debugLevel
#define debugLevel lError
#endif
#include <debug_headers/debug.hpp>
namespace common
{
namespace SerialPort
{

serialStringStream::serialStringStream(const char* pName, baud_rate bRate, character_size bNumber, parity par, stop_bits sBits):
	serialByteStream(pName, bRate, bNumber, par, sBits),
	str_buffer("")
{
}

serialStringStream::~serialStringStream()
{
	async_stop();
}


void serialStringStream::async_read(std::function<void(std::string)> &function, uint8_t terminator)
{

	std::function<void(uint8_t)> fun=[function, terminator](uint8_t receivedData)
	{
		static std::string str_buffer="";
		str_buffer+=(char)receivedData;
		if(receivedData==terminator)
		{
			std::string buffer_copy="";
			std::swap(str_buffer, buffer_copy);
			function(buffer_copy);
		}
	};
	super::async_read(fun);
}

void serialStringStream::read(std::string &data, uint8_t terminator)
{
	async_stop();
	uint8_t receivedData;
	std::string str_buffer="";
	do
	{
		super::read(receivedData);
		str_buffer+=(char)receivedData;
	}
	while(receivedData!=terminator);
	std::swap(data,	str_buffer);
}

void serialStringStream::write(const std::string sendData)
{
	return super::super::write(sendData.c_str(), sendData.size());

}

std::future<void> serialStringStream::async_write(const std::string sendData)
{
	return super::super::async_write(sendData.c_str(), sendData.size());
}


void serialStringStream::async_stop()
{
	return super::super::async_stop();
}

}

}
