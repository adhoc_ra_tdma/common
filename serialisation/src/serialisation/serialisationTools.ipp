#ifndef __ENDIENESS_TOOLS_IPP__
#define __ENDIENESS_TOOLS_IPP__

#include <serialisation/serialisationTools.hpp>

#include <endian.h>

#if __BYTE_ORDER == __BIG_ENDIAN
/* The host byte order is the same as network byte order,
   so these functions are all just identity.  */
#	define ntoh64(x)	(x)
#	define ntoh32(x)	(x)
#	define ntoh16(x)	(x)
#	define hton64(x)	(x)
#	define hton32(x)	(x)
#	define hton16(x)	(x)
#elif __BYTE_ORDER == __LITTLE_ENDIAN
#	define ntoh64(x)	__bswap_64 (x)
#	define ntoh32(x)	__bswap_32 (x)
#	define ntoh16(x)	__bswap_16 (x)
#	define hton64(x)	__bswap_64 (x)
#	define hton32(x)	__bswap_32 (x)
#	define hton16(x)	__bswap_16 (x)
#endif




#define TEMPLATE_SIG template <class Type>
#define CLASS_SIG serialisation_tools<Type>

namespace common
{

namespace serialisation
{
/*
TEMPLATE_SIG
template <class... T> void CLASS_SIG::serialise(T &...)
{
	not_implemented ex;
	throw(ex);
}

TEMPLATE_SIG
template <class... T> void CLASS_SIG::deserialise(T &...)
{
	not_implemented ex;
	throw(ex);
}
*/

TEMPLATE_SIG
template <class T>
		typename std::enable_if<sizeof(T)==8 && std::is_arithmetic<T>::value ,void>::type CLASS_SIG::serialise(Container &ret, const T &host64)
{
	std::back_insert_iterator<Container> bi=std::back_inserter(ret);
	uint64_t huint64=*(uint64_t *)&host64;
	uint64_t nuint64=hton64(huint64);
	uint8_t *ptr=(uint8_t*) &nuint64;
	for(std::size_t i=0;i<sizeof(uint64_t);i++)
	{
		bi=ptr[i];
	}
}

TEMPLATE_SIG
template <class T>
		typename std::enable_if<sizeof(T)==4 && std::is_arithmetic<T>::value ,void>::type  CLASS_SIG::serialise(Container &ret, const T &host32)
{
	std::back_insert_iterator<Container> bi=std::back_inserter(ret);
	uint32_t huint32=*(uint64_t *)&host32;
	uint32_t nuint32=hton32(huint32);
	uint8_t *ptr=(uint8_t*) &nuint32;
	for(std::size_t i=0;i<sizeof(uint32_t);i++)
	{
		bi=ptr[i];
	}
}

TEMPLATE_SIG
template <class T>
		typename std::enable_if<sizeof(T)==2 && std::is_arithmetic<T>::value ,void>::type  CLASS_SIG::serialise(Container &ret, const T &host16)
{
	std::back_insert_iterator<Container> bi=std::back_inserter(ret);
	uint16_t huint16=*(uint16_t *)&host16;
	uint16_t nuint16=hton16(huint16);	uint8_t *ptr=(uint8_t*) &nuint16;
	for(std::size_t i=0;i<sizeof(uint16_t);i++)
	{
		bi=ptr[i];
	}
}

TEMPLATE_SIG
template <class T>
		typename std::enable_if<sizeof(T)==1 && std::is_arithmetic<T>::value ,void>::type  CLASS_SIG::serialise(Container &ret, const T &host8)
{
	std::back_insert_iterator<Container> bi=std::back_inserter(ret);
	uint8_t huint8=*(uint8_t *)&host8;
	bi=huint8;
}

TEMPLATE_SIG
template <class T>
		typename std::enable_if<std::is_class<T>::value && std::is_base_of<SerialisableI,T>::value,void>::type CLASS_SIG::serialise(Container &ret, const SerialisableI &hostdata)
{
	return hostdata.serialise(ret);
}
//		static void deserialise(typename Container::const_iterator &networklonglong, uint64_t &hostlonglong);


TEMPLATE_SIG
template <class T>
		typename std::enable_if<(std::is_class<T>::value && !std::is_base_of<SerialisableI,T>::value) && std::is_same<typename T::iterator::value_type, typename T::value_type>::value,void>::type CLASS_SIG::serialise(Container &ret, const T &hostiter)
{
	uint64_t s=hostiter.size();
	common::serialisation::serialise(ret,s);
	for(const typename T::value_type &elem:hostiter)
	{
		common::serialisation::serialise(ret,elem);
	}
	return;
}

TEMPLATE_SIG
template <class T>
		typename std::enable_if<sizeof(T)==8 && std::is_arithmetic<T>::value ,void>::type CLASS_SIG::deserialise(Container &ret, T &host64)
{

	uint64_t nuint64;
	uint8_t *ptr=(uint8_t*) &nuint64;

	for(std::size_t i=0;i<sizeof(uint64_t);i++)
	{
		ptr[i]=ret.front();
		ret.pop_front();
	}
	uint64_t huint64=ntoh64(nuint64);
	host64=*(T *)&huint64;
}

TEMPLATE_SIG
template <class T>
		typename std::enable_if<sizeof(T)==4 && std::is_arithmetic<T>::value ,void>::type  CLASS_SIG::deserialise(Container &ret, T &host32)
{

	uint32_t nuint32;
	uint8_t *ptr=(uint8_t*) &nuint32;

	for(std::size_t i=0;i<sizeof(uint32_t);i++)
	{
		ptr[i]=ret.front();
		ret.pop_front();
	}
	uint32_t huint32=ntoh32(nuint32);
	host32=*(T *)&huint32;
}

TEMPLATE_SIG
template <class T>
		typename std::enable_if<sizeof(T)==2 && std::is_arithmetic<T>::value ,void>::type  CLASS_SIG::deserialise(Container &ret, T &host16)
{

	uint16_t nuint16;
	uint8_t *ptr=(uint8_t*) &nuint16;

	for(std::size_t i=0;i<sizeof(uint16_t);i++)
	{
		ptr[i]=ret.front();
		ret.pop_front();
	}
	uint16_t huint16=ntoh16(nuint16);
	host16=*(T *)&huint16;
}

TEMPLATE_SIG
template <class T>
		typename std::enable_if<sizeof(T)==1 && std::is_arithmetic<T>::value ,void>::type  CLASS_SIG::deserialise(Container &ret, T &host8)
{
	uint8_t nuint8;

	nuint8=ret.front();
	ret.pop_front();
	host8=*(T *)&nuint8;
}

TEMPLATE_SIG
template <class T>
		typename std::enable_if<std::is_class<T>::value && std::is_base_of<SerialisableI,T>::value,void>::type CLASS_SIG::deserialise(Container &ret,SerialisableI &hostdata)
{
	return hostdata.deserialise(ret);
}

TEMPLATE_SIG
template <class T>
		typename std::enable_if<std::is_class<T>::value && (!std::is_base_of<SerialisableI,T>::value) && std::is_same<typename T::iterator::value_type, typename T::value_type>::value,void>::type CLASS_SIG::deserialise(Container &ret, T &hostiter)
{
	uint64_t s;
	common::serialisation::deserialise(ret,s);
	std::back_insert_iterator<T> it=std::back_inserter(hostiter);
	for(std::size_t i=0;i<s;i++)
	{
		typename T::value_type elem;
		common::serialisation::deserialise(ret,elem);
		*it=elem;
	}
	return;
}


}


}

#undef TEMPLATE_SIG
#undef CLASS_SIG

#endif
