#ifndef __SERIALISABLE_INTERFACE_HPP__
#define __SERIALISABLE_INTERFACE_HPP__

#include <iterator>
#include <queue>
#include <type_traits>
#include <typeinfo>

namespace common
{

namespace serialisation
{
using Container=std::deque<uint8_t>;

class SerialisableI
{

	private:






	public:


		virtual void serialise( Container &ret) const = 0;
		virtual void deserialise(Container &ret) = 0;
		/*virtual void deserialise(cIterator &ret) final
		{
			deserialise((ccIterator &)ret);
		}*/


	private:


};

namespace
{
template<typename _Container>
class __is_serialisable_helper
{

		template<class _Container1, typename = typename std::enable_if<std::is_arithmetic<_Container1>::value, int>::type>
		static std::true_type __test(int);

		template<class _Container1, class _Container2=_Container1, typename = typename std::enable_if<std::is_base_of<SerialisableI,_Container1>::value, int>::type>
		static std::true_type __test(int);

		template<class _Container1, class _Container2=_Container1, class _Container3=_Container2, typename = typename std::enable_if<(!std::is_base_of<SerialisableI, _Container1>::value) &&
																	   std::is_same<typename _Container1::iterator::value_type, typename _Container1::value_type>::value, int>::type>
		static std::true_type __test(int);

		template<typename> static std::false_type __test(...);

	public:
		typedef decltype(__test<_Container>(0)) type;
};
}

template<typename _Container>
struct is_serialisable
		: public __is_serialisable_helper<_Container>::type
{ };

}
}
#endif

