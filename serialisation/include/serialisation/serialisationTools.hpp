#ifndef __ENDIENESS_TOOLS_HPP__
#define __ENDIENESS_TOOLS_HPP__

#include <serialisation/serialisableI.hpp>

#include <chrono>

#include <deque>
#include <vector>
#include <cstdint>

#include <type_traits>

#include <iterator>
#include <limits>
namespace common
{

namespace serialisation
{

class not_implemented:public std::exception
{
	public:
		virtual const char* what() const noexcept override
		{
			return "Serialisation is not implemented.";
		}


};
template <class Type> class serialisation_tools
{

	private:
		serialisation_tools()=delete;

		template<typename _Container>
		class __is_iteratable_helper
		{

				template<class _Container1, typename = typename std::enable_if< std::is_class<_Container1>::value, std::back_insert_iterator<_Container1>  >::type >
				static std::true_type __test(int);

				template<typename> static std::false_type __test(...);

			public:
				typedef decltype(__test<_Container>(0)) type;
		};


		template<typename _Container>
		struct is_iteratable
				: public __is_iteratable_helper<_Container>::type
		{ };



	public:

		static_assert(is_iteratable<Container>::value,"Parameter to SerialisationTools must be an iteratable container");
		static_assert(sizeof(typename Container::iterator::value_type)==1,"TODO: For now parameter to SerialisationTools must be a container of 1 byte elements");
		static_assert(std::numeric_limits<float>::is_iec559,"TODO: float format is unknown... Cannot serialise");
		static_assert(std::numeric_limits<double>::is_iec559,"TODO: double format is unknown... Cannot serialise");
		static_assert(std::numeric_limits<long double>::is_iec559,"TODO: long double format is unknown... Cannot serialise");

		/** Accept all ints and floats */
		template <class T=Type> static typename std::enable_if<sizeof(T)==8 && std::is_arithmetic<T>::value ,void>::type  serialise(Container &ret, const T &host64);
		template <class T=Type> static typename std::enable_if<sizeof(T)==4 && std::is_arithmetic<T>::value ,void>::type  serialise(Container &ret, const T &host32);
		template <class T=Type> static typename std::enable_if<sizeof(T)==2 && std::is_arithmetic<T>::value ,void>::type  serialise(Container &ret, const T &host16);
		template <class T=Type> static typename std::enable_if<sizeof(T)==1 && std::is_arithmetic<T>::value ,void>::type  serialise(Container &ret, const T &host8);
		/** Accept all classes that implement SerialisableI */
		template <class T=Type> static typename std::enable_if<std::is_class<T>::value && std::is_base_of<SerialisableI,T>::value,void>::type serialise(Container &ret, const SerialisableI &hostdata);
		/** Accept all classes that do not implement SerialisableI but have an iterator (STL) */
		template <class T=Type> static typename std::enable_if<std::is_class<T>::value && (!std::is_base_of<SerialisableI,T>::value) && std::is_same<typename T::iterator::value_type, typename T::value_type>::value,void>::type serialise(Container &ret, const T &hostiter);

		/** Accept all ints and floats */
		template <class T=Type> static typename std::enable_if<sizeof(T)==8 && std::is_arithmetic<T>::value ,void>::type  deserialise(Container &ret, T &host64);
		template <class T=Type> static typename std::enable_if<sizeof(T)==4 && std::is_arithmetic<T>::value ,void>::type  deserialise(Container &ret, T &host32);
		template <class T=Type> static typename std::enable_if<sizeof(T)==2 && std::is_arithmetic<T>::value ,void>::type  deserialise(Container &ret, T &host16);
		template <class T=Type> static typename std::enable_if<sizeof(T)==1 && std::is_arithmetic<T>::value ,void>::type  deserialise(Container &ret, T &host8);
		/** Accept all classes that implement SerialisableI */
		template <class T=Type> static typename std::enable_if<std::is_class<T>::value && std::is_base_of<SerialisableI,T>::value ,void>::type deserialise(Container &ret,SerialisableI &hostdata);
		/** Accept all classes that do not implement SerialisableI but have an iterator (STL) */
		template <class T=Type> static typename std::enable_if<std::is_class<T>::value && (!std::is_base_of<SerialisableI,T>::value) && std::is_same<typename T::iterator::value_type, typename T::value_type>::value,void>::type deserialise(Container &ret, T &hostiter);

		// TODO: This should have a default not implemented interface, but I could not do it... So we have unimplemented stuff!
		/*template <class... T> static void serialise(T &...);
		template <class... T> static void deserialise(T &...);*/
		template <class U, class V> static void serialise(Container &c, const std::chrono::time_point<U,V> &time_point){}
		template <class U, class V> static void deserialise(Container &c, std::chrono::time_point<U,V> &time_point){}

		template <class U, class V> static void serialise(Container &c, const std::chrono::duration<U,V> &duration){}
		template <class U, class V> static void deserialise(Container &c, std::chrono::duration<U,V> &duration){}

};


template <class Type> void serialise(Container &ret, const Type &data)
{
	serialisation_tools<Type>::serialise(ret,data);
}

template <class Type> void deserialise(Container &c, Type &data)
{
	serialisation_tools<Type>::deserialise(c,data);
}


}

}
#include "../../src/serialisation/serialisationTools.ipp"
#endif
