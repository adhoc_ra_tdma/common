#include <serialisation/serialisationTools.hpp>


#include <iostream>
#include <cstdint>
#include <vector>
#include <list>
#include <deque>
int main()
{
	uint8_t ui8=0x31;
	uint16_t ui16=2;
	uint32_t ui32=3;
	uint64_t ui64=4;
	int8_t i8=0x32;
	int16_t i16=-2;
	int32_t i32=-3;
	int64_t i64=-4;

	float f=1.0;
	double d=-1.0;


	std::vector<uint16_t> v({10,9,8,7,6,5,4,3,2,1,0});
	std::list<uint32_t> l({110,9,8,7,6,5,4,3,2,1,0});
	std::deque<uint64_t> de({1110,9,8,7,6,5,4,3,2,1,0});

	common::serialisation::Container data;
	common::serialisation::serialise(data,ui8);
	common::serialisation::serialise(data,ui16);
	common::serialisation::serialise(data,ui32);
	common::serialisation::serialise(data,ui64);
	common::serialisation::serialise(data,i8);
	common::serialisation::serialise(data,i16);
	common::serialisation::serialise(data,i32);
	common::serialisation::serialise(data,i64);
	common::serialisation::serialise(data,f);
	common::serialisation::serialise(data,d);
	common::serialisation::serialise(data,v);
	common::serialisation::serialise(data,l);
	common::serialisation::serialise(data,de);

	uint8_t after_ui8;
	uint16_t after_ui16;
	uint32_t after_ui32;
	uint64_t after_ui64;
	int8_t after_i8;
	int16_t after_i16;
	int32_t after_i32;
	int64_t after_i64;
	float after_f;
	double after_d;
	std::vector<uint16_t> after_v;
	std::list<uint32_t> after_l;
	std::deque<uint64_t> after_de;


	common::serialisation::deserialise(data,after_ui8);
	common::serialisation::deserialise(data,after_ui16);
	common::serialisation::deserialise(data,after_ui32);
	common::serialisation::deserialise(data,after_ui64);
	common::serialisation::deserialise(data,after_i8);
	common::serialisation::deserialise(data,after_i16);
	common::serialisation::deserialise(data,after_i32);
	common::serialisation::deserialise(data,after_i64);
	common::serialisation::deserialise(data,after_f);
	common::serialisation::deserialise(data,after_d);
	common::serialisation::deserialise(data,after_v);
	common::serialisation::deserialise(data,after_l);
	common::serialisation::deserialise(data,after_de);

#define PRINT(var) std::cout<< var <<std::endl<< after_##var<<std::endl
#define PRINT_ITER(var) \
	for(auto &e:var) \
	std::cout<<e<<" ";\
	std::cout<<std::endl;\
	for(auto &e: after_##var)\
	std::cout<<e<<" ";\
	std::cout<<std::endl;

	PRINT(ui8);
	PRINT(ui16);
	PRINT(ui32);
	PRINT(ui64);
	PRINT(i8);
	PRINT(i16);
	PRINT(i32);
	PRINT(i64);
	PRINT(f);
	PRINT(d);
PRINT_ITER(v);
PRINT_ITER(l);
PRINT_ITER(de);
	return 0;
}
