#ifndef __MEAN_FILTER_HPP__
#define __MEAN_FILTER_HPP__
#include <iostream>
#include <list>
#include <algorithm>
#include <chrono>

#include <simulatable_clock/simulatable_clock.hpp>

#include <data_filters/generic_filter.hpp>
#include <realtime_data/rtvariable.hpp>

#include <type_traits>
#include <functional>
#include <mutex>
namespace common
{
namespace relative_localisation
{


template <class T, std::size_t n_samples> class mean_window_filter;

template <class T, std::size_t n_samples>
std::ostream& operator<<(std::ostream& output, const mean_window_filter<T,n_samples> &obj);



template <class T, std::size_t n_samples> class mean_window_filter:public virtual generic_filter<T>, public std::vector<int>
{
		static_assert( !std::is_same<T,bool>::value, "You cannot use a boolean type when using mean_window_filter");
		static_assert( std::is_arithmetic<T>::value, "mean_window_filter uses arithmetic types");
		//static_assert( std:: <T,bool>::value, "You cannot use a boolean type when using mean_window_filter");
	public:

		mean_window_filter(std::chrono::nanoseconds sample_validity=std::chrono::nanoseconds::max());

		mean_window_filter(const mean_window_filter<T, n_samples> &other);
		mean_window_filter &operator=(const mean_window_filter<T, n_samples> &other);

		mean_window_filter(mean_window_filter<T, n_samples> &&other);
		mean_window_filter &operator=(mean_window_filter<T, n_samples> &&other);

		virtual simulation_interface::simulation_clock::duration read(T &data) override;

		virtual void write(const T &new_sample) override;


	private:

		bool dirty=true;
		void filter(void);

		std::chrono::nanoseconds sample_validity;


		std::function<bool (RTVariable<T> &element)> delFunction;

		friend std::ostream& operator<< <> (std::ostream& output, const mean_window_filter<T,n_samples> &obj);


	protected:
		std::list<RTVariable<T>> data;
		RTVariable<T> filtered_data;
		mutable std::mutex lock;

};


}
}

#include "../../src/data_filters/mean_window_filter.ipp"

#endif
