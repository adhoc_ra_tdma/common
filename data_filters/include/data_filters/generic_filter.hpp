#ifndef __GENERIC_FILTER_HPP__
#define __GENERIC_FILTER_HPP__

#include <chrono>
namespace common
{
namespace relative_localisation
{

template <class T>class generic_filter
{
	public:
		virtual std::chrono::nanoseconds read(T &data)=0;
		virtual void write(const T &new_sample)=0;

	private:
};

}
}

#include "../../src/data_filters/generic_filter.ipp"

#endif
