#ifndef __MEAN_FILTER_IPP__
#define __MEAN_FILTER_IPP__
#include <data_filters/mean_window_filter.hpp>


#ifdef debugLevel
#define WAS_DEFINED_DEBUG_LEVEL
#define PREV_DEBUG_LEVEL debugLevel
#undef debugLevel
#endif

//#define debugLevel lDebug
#ifndef debugLevel
#define debugLevel lError
#endif
#include <debug_headers/debug.hpp>

#define TEMPLATE_SIG template <class T, std::size_t n_samples>
#define CLASS_SIG mean_window_filter<T, n_samples>
namespace common
{
namespace relative_localisation
{


template<class T, std::size_t n_samples>
std::ostream& operator<<(std::ostream& output, const mean_window_filter<T,n_samples> &obj)
{
	obj.lock.lock();
	for(const RTVariable<T> &sample:obj.data)
	{
		output<<sample<<std::endl;
	}
	output<<"Mean = "<<obj.filtered_data;
	obj.lock.unlock();
	return output;  // for multiple << operators.
}


TEMPLATE_SIG
CLASS_SIG::mean_window_filter(std::chrono::nanoseconds sample_validity):
	dirty(true),
	sample_validity(sample_validity),
	delFunction([this](RTVariable<T> &element)
	{
		if (element.getAge()>=this->sample_validity)
		{
			dirty=true;
			return true;
		}
		return false;
		//return (element.getAge()>=this->sample_validity)?(dirty=true):false;
	})
{


}

TEMPLATE_SIG
CLASS_SIG::mean_window_filter(const CLASS_SIG &other):
	dirty(other.dirty),
	sample_validity(other.sample_validity),
	delFunction(other.delFunction),
	data(other.data),
	filtered_data(other.filtered_data)
{
}

TEMPLATE_SIG
CLASS_SIG &CLASS_SIG::operator=(const CLASS_SIG &other)
{
	if(this!=&other)
	{
		data=other.data;
		sample_validity=other.sample_validity;
		delFunction=other.delFunction;
		dirty=other.dirty;
		filtered_data=other.filtered_data;
	}
	return *this;
}


TEMPLATE_SIG
CLASS_SIG::mean_window_filter(CLASS_SIG &&other):
	dirty(std::move(other.dirty)),
	sample_validity(std::move(other.sample_validity)),
	delFunction(std::move(other.delFunction)),
	data(std::move(other.data)),
	filtered_data(std::move(other.filtered_data))
{
}

TEMPLATE_SIG
CLASS_SIG &CLASS_SIG::operator=(CLASS_SIG &&other)
{
	if(this!=&other)
	{
		data=std::move(other.data);
		sample_validity=std::move(other.sample_validity);
		delFunction=std::move(other.delFunction);
		dirty=std::move(other.dirty);
		filtered_data=std::move(other.filtered_data);
	}
	return *this;
}

TEMPLATE_SIG
simulation_interface::simulation_clock::duration CLASS_SIG::read(T &datum)
{
	PDEBUG(std::cout,"Reading filter");

	lock.lock();
	data.remove_if([this](RTVariable<T> &element)
	{
		if (element.getAge()>=this->sample_validity)
		{
			dirty=true;
			return true;
		}
		return false;
		//return (element.getAge()>=this->sample_validity)?(dirty=true):false;
	});
	if(dirty)
	{
		PDEBUG(std::cout,"Dirty reading");
		filter();
	}
	datum=filtered_data.getData();
	simulation_interface::simulation_clock::duration age=filtered_data.getAge();
	lock.unlock();
	return age;
}

TEMPLATE_SIG
void CLASS_SIG::write(const T &new_sample)
{
	PDEBUG(std::cout,"pushing "<<(REMOVE_CHAR_TYPE(const T))(new_sample));

	lock.lock();

	data.push_back(new_sample);

	if(data.size()>n_samples)
	{
		data.pop_front();
	}
	data.remove_if([this](RTVariable<T> &element)
	{
		if (element.getAge()>=this->sample_validity)
		{
			dirty=true;
			return true;
		}
		return false;
		//return (element.getAge()>=this->sample_validity)?(dirty=true):false;
	});
	filter();
	lock.unlock();return;
}


TEMPLATE_SIG
void CLASS_SIG::filter(void)
{

	if(data.size()==0)
	{
		RTVariable<T> sum;
		filtered_data=sum;
		PDEBUG(std::cout,"No samples");
	}
	else
	{
		T sum=T();
		for(RTVariable<T> &element: data)
		{
			sum=sum+element;
			PDEBUG(std::cout,"filtering "<<(REMOVE_CHAR_TYPE(RTVariable<T>))(element));
		}
		sum/=data.size();
		filtered_data.setData(sum, simulation_interface::simulation_clock::now());
		PDEBUG(std::cout,"sum "<<(REMOVE_CHAR_TYPE(RTVariable<T>))filtered_data);
	}
	dirty=false;
	return;

}



}
}

// This is actually included!!! Remove what you did to debug header
#include <debug_headers/clear_debug.hpp>
#ifdef WAS_DEFINED_DEBUG_LEVEL
#define debugLevel PREV_DEBUG_LEVEL
#undef WAS_DEFINED_DEBUG_LEVEL
#undef PREV_DEBUG_LEVEL
#include <debug_headers/debug.hpp>
#endif

#undef TEMPLATE_SIG
#undef CLASS_SIG

#endif
