#ifndef DEBUG_HPP
#define DEBUG_HPP
#include <string.h>
#include <iostream>
#include <type_traits>
#include <functional>

#define REMOVE_CHAR_TYPE(in_type)	typename std::conditional<\
	std::is_same<std::remove_cv<in_type>,std::remove_cv<unsigned char> >::value||\
	std::is_same<std::remove_cv<in_type>,std::remove_cv<signed char> >::value||\
	std::is_same<std::remove_cv<in_type>,std::remove_cv<char> >::value\
	,int32_t, in_type &>::type

#define IF_CHAR_PRINT_NUMBER(data)	((typename std::conditional<\
	std::is_same<std::remove_cv<decltype(data)>,std::remove_cv<unsigned char> >::value||\
	std::is_same<std::remove_cv<decltype(data)>,std::remove_cv<signed char> >::value||\
	std::is_same<std::remove_cv<decltype(data)>,std::remove_cv<char> >::value\
	,int32_t, decltype(data)>::type)(data))

#define lDebugPlus 0
#define lDebug 1
#define lInfo 2
#define lWarning 3
#define lLog 4
#define lRuntime 5
#define lError 6

#ifndef debugLevel
	#define debugLevel lRuntime
    #warning No definition of 'debugLevel' detected. Must be defined before including file
	#warning Defaulted to lRuntime
#endif
/* */

#if (debugLevel!=lDebug) && (debugLevel!=lInfo) && (debugLevel!=lWarning) && (debugLevel!=lLog) && (debugLevel!=lError) && (debugLevel!=lRuntime)
    #error Incorrect debug level defined usage: #define debugLevel <option>
    #error Options: \
        lDebug\
        lInfo\
        lWarning\
		lLog\
		lRuntime\
		lError
#endif

namespace common
{
namespace debug_headers
{
}
}


#if debugLevel<=lDebugPlus
	#define PDEBUG_VERBATIM(OSTREAM,...) OSTREAM<<__VA_ARGS__
#else
	#define PDEBUG_VERBATIM(OSTREAM,...)

#endif
#if debugLevel<=lDebug
	#define PDEBUG(OSTREAM,...) OSTREAM<<std::endl<<basename(__FILE__)<<":"<<__FUNCTION__<< "("<< __LINE__ << ") "<<"DEBUG: "<<__VA_ARGS__<<std::endl
	#define DEBUG_FUNCTION for(std::function<void()> f;false;f()) f=[&]()
#else
    #define PDEBUG(OSTREAM,...)
	#define DEBUG_FUNCTION while(false) std::function<void()> f=[&]()
#endif
#if debugLevel<=lInfo
	#define PINFO(OSTREAM,...) OSTREAM<<std::endl<<basename(__FILE__)<<":"<<__FUNCTION__<< "("<< __LINE__ << ") "<<"INFO: "<<__VA_ARGS__<<std::endl
	#define INFO_FUNCTION for(std::function<void()> f;false;f()) f=[&]()
#else
    #define PINFO(OSTREAM,...)
	#define INFO_FUNCTION while(false) std::function<void()> f=[&]()
#endif
#if debugLevel<=lWarning
	#define PWARNING(OSTREAM,...) OSTREAM<<std::endl<<basename(__FILE__)<<":"<<__FUNCTION__<< "("<< __LINE__ << ") "<<"WARNING: "<<__VA_ARGS__<<std::endl
	#define WARNING_FUNCTION for(std::function<void()> f;false;f()) f=[&]()
#else
    #define PWARNING(OSTREAM,...)
	#define WARNING_FUNCTION while(false) std::function<void()> f=[&]()
#endif
#if debugLevel<=lError
	#define PERROR(OSTREAM,...) OSTREAM<<std::endl<<basename(__FILE__)<<":"<<__FUNCTION__<< "("<< __LINE__ << ") "<<"ERROR: "<<__VA_ARGS__<<std::endl
	#define ERROR_FUNCTION for(std::function<void()> f;false;f()) f=[&]()
#else
    #define PERROR(OSTREAM,...)
	#define ERROR_FUNCTION while(false) std::function<void()> f=[&]()
#endif

#if debugLevel<=lLog
	#define PLOG(LOG_FILE,...) LOG_FILE<<__VA_ARGS__<<std::endl
	#define LOG_FUNCTION for(std::function<void()> f;!f;f()) f=[&]()
#else
	#define PLOG(LOG_FILE,...)
	#define LOG_FUNCTION if(false) std::function<void()> f[&]()
#endif

#if debugLevel<=lRuntime
	#define PRUNTIME(OSTREAM,...) OSTREAM<<__VA_ARGS__
#else
	#define PRUNTIME(OSTREAM,...)
#endif



#else

	#warning Debug was previously defined: include clear_debug to remove all debug configs

#endif // DEBUG_HPP
