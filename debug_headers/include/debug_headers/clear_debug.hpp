#ifdef DEBUG_HPP
#undef DEBUG_HPP

#undef debugLevel
#undef lDebug
#undef lInfo
#undef lWarning
#undef lLog
#undef lError



#undef PDEBUG
#undef PINFO
#undef PWARNING
#undef PERROR
#undef PLOG
#undef PRUNTIME

#undef DEBUG_FUNCTION
#undef INFO_FUNCTION
#undef WARNING_FUNCTION
#undef ERROR_FUNCTION
#undef LOG_FUNCTION



#undef REMOVE_CHAR_TYPE
#undef IF_CHAR_PRINT_NUMBER
#endif // DEBUG_HPP
