#include <logging/logging.hpp>


namespace common
{
namespace Logging
{
LogStream::LogStream(std::string path):
	std::ofstream()
{
	open(path.c_str());
}

LogStream::~LogStream()
{
	close();
}

}

}
