#ifndef LOGGING_HPP
#define LOGGING_HPP



#include <ostream>
#include <fstream>

namespace common
{
namespace Logging
{
class LogStream:public std::ofstream
{

	public:
		LogStream(std::string path);

		~LogStream();

	protected:

	private:
		LogStream();
};
}
}
#endif // LOGGING_HPP
