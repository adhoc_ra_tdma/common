#ifndef TESTER_OF_GENERIC_INTERFACES
#warning THIS IS NOT STABLE
#endif

#ifndef __TASK_INTERFACE_HPP__
#define __TASK_INTERFACE_HPP__

#include <generic_interfaces/job.hpp>
#include <simulatable_timer/simulatable_timer.hpp>

#include <vector>
#include <list>
#include <functional>
#include <mutex>

#include <tuple>
namespace common
{
namespace GenericInterfaces
{
/* <C;T> */
class TaskProperties: std::tuple<std::chrono::microseconds,std::chrono::microseconds>
{
	private:
		using Super=std::tuple<std::chrono::microseconds,std::chrono::microseconds>;
	public:
		inline std::chrono::microseconds getC(){return std::get<0>(*this);}
		inline std::chrono::microseconds getT(){return std::get<1>(*this);}
};


template<typename _Signature>
class Task;

template<typename Return, typename... arguments>
class Task<Return(arguments...)>
{
	public:


		Task()=default;

		Task(Job<void()> &job, std::chrono::microseconds execution_rate):
			job(job),
			execution_period(execution_rate)
		{
			initialise();
		}

		Task(const Task &other)=delete;

		Task(Task &&other):
			job(std::move(other.job)),
			execution_period(std::move(other.execution_period))
		{
			initialise();
		}

		Task &operator=(Task &&other)
		{
			if(this!=&other)
			{
				job=std::move(other.job);
				execution_period=std::move(other.execution_period);
				initialise();
			}
			return *this;
		}

		virtual ~Task()
		{
			task.stop();
		}

		Return release_job(arguments... args)
		{
			return job(args...);
		}


	protected:

		void initialise()
		{
			job_wrapper=[this]()
			{
				lock.lock();
				job();
				lock.unlock();
			};
			task.start(job, execution_period, execution_period);
		}

	private:

		Task &operator=(const Task &other){}


		std::mutex lock;
		std::chrono::microseconds execution_period;
		common::simulation_interface::timer::timer_interface &task=common::simulation_interface::timer::new_timer();

		std::function<void()> job_wrapper;
		Job<void()> job;

};

}
}

#endif
