#ifndef TESTER_OF_GENERIC_INTERFACES
#warning THIS IS NOT STABLE
#endif

#ifndef __SYNCH_TASK_INTERFACE_HPP__
#define __SYNCH_TASK_INTERFACE_HPP__

#include <generic_interfaces/task.hpp>
#include <simulatable_timer/simulatable_timer.hpp>
#include <functional>

#include <chrono>
/*
namespace common
{
namespace GenericInterfaces
{
template<class Typ,typename... arguments> class SynchTask:protected Task<Typ,arguments...>
{

	public:
		template <typename _Rep, typename _Period> SynchTask(std::chrono::duration<_Rep,_Period> C,
															 std::chrono::duration<_Rep,_Period> T,
															 std::chrono::duration<_Rep,_Period> D,
															 GenericJob<Typ,arguments...> job):
			Super(job)
		{
			this->C=std::chrono::duration_cast<std::chrono::high_resolution_clock::duration>(C);
			this->T=std::chrono::duration_cast<std::chrono::high_resolution_clock::duration>(T);
			this->D=std::chrono::duration_cast<std::chrono::high_resolution_clock::duration>(D);

		}

		template <typename _Rep, typename _Period> SynchTask(std::chrono::duration<_Rep,_Period> C,
															 std::chrono::duration<_Rep,_Period> T,
															 std::chrono::duration<_Rep,_Period> D,
															 std::vector<GenericJob<Typ,arguments...> > jobList):
			Super(jobList)
		{
			this->C=std::chrono::duration_cast<std::chrono::high_resolution_clock::duration>(C);
			this->T=std::chrono::duration_cast<std::chrono::high_resolution_clock::duration>(T);
			this->D=std::chrono::duration_cast<std::chrono::high_resolution_clock::duration>(D);

		}

		template <typename _Rep, typename _Period> SynchTask(std::chrono::duration<_Rep,_Period> C,
															 std::chrono::duration<_Rep,_Period> T,
															 std::chrono::duration<_Rep,_Period> D,
															 std::list<GenericJob<Typ,arguments...> > jobList):
			Super(jobList)
		{
			this->C=std::chrono::duration_cast<std::chrono::high_resolution_clock::duration>(C);
			this->T=std::chrono::duration_cast<std::chrono::high_resolution_clock::duration>(T);
			this->D=std::chrono::duration_cast<std::chrono::high_resolution_clock::duration>(D);

		}

		void start()
		{

		}

		void stop()
		{

		}

	protected:


	private:
		typedef Task<Typ,arguments...> Super;
		std::chrono::high_resolution_clock::duration T;
		std::chrono::high_resolution_clock::duration C;
		std::chrono::high_resolution_clock::duration D;

		//common::Timer::TimerPeriodic timerPeriodic;
};
}
}*/

#endif
