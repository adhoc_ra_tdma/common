#ifndef TESTER_OF_GENERIC_INTERFACES
#warning THIS IS NOT STABLE
#endif

#ifndef __JOB_HPP__
#define __JOB_HPP__

#include <functional>
#include <chrono>
namespace common
{
namespace GenericInterfaces
{

template<typename _Signature>
class Job;

/**
  *  @brief Primary class template for GenericJob.
  *  @ingroup functors
  *
  *  Polymorphic GenericJob wrapper.
  */
template<class Return,typename... arguments>
class Job<Return(arguments...)>
{

	public:

		Job():
			C(std::chrono::microseconds::zero()),
			implementation([](){})
		{

		}

		Job(const Job &other)=delete;


		Job(Job &&other):
			C(std::move(other.C)),
			implementation(std::move(other.implementation))
		{

		}

		Job &operator=(Job &&other)
		{
			if(this!=&other)
			{
				implementation=std::move(other.implementation);
				C=std::move(other.C);
			}
			return *this;
		}

		virtual ~Job()=default;


		Return operator() (arguments... parameters)
		{
			return implementation(parameters...);
		}

		std::chrono::microseconds getCapacity()
		{
			return C;
		}

	protected:
		// Not being used
		std::chrono::microseconds C;
		std::function<Return(arguments... parameters)> implementation;

	private:
		Job &operator=(const Job &other){}


};

}
}

#endif
