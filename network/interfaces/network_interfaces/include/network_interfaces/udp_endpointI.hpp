#ifndef __INTERFACE_UDP_ENDPOINT_HPP__
#define __INTERFACE_UDP_ENDPOINT_HPP__

#include <string>
#include <cstdint>

#include <network_interfaces/ip_v4I.hpp>
namespace common
{
namespace network
{
namespace interfaces
{



class udp_endpointI: virtual public ip_v4I
{
	public:

		virtual ~udp_endpointI()=default;

		virtual void setPort(uint16_t port)=0;
		virtual uint16_t getPort() const=0;
};


}
}
}
#endif

