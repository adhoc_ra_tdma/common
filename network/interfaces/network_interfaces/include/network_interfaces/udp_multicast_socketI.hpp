#ifndef __INTERFACE_UDP_MULTICAST_SOCKET_HPP__
#define __INTERFACE_UDP_MULTICAST_SOCKET_HPP__

#include <network_interfaces/udp_unicast_socketI.hpp>



namespace common
{
namespace network
{
namespace interfaces
{


template <class MessageType> class udp_multicast_socket_event_triggered:
		public virtual startable_stoppable_interface,
		public virtual udp_callback_reader_interface<MessageType>,
		public virtual udp_writer_interface<MessageType>
{

	public:

		virtual ~udp_multicast_socket_event_triggered()=default;

};


template <class MessageType> class udp_multicast_socket_blockable:
		public virtual udp_future_reader_interface<MessageType>,
		public virtual udp_future_writer_interface<MessageType>
{

	public:

		virtual ~udp_multicast_socket_blockable()=default;

};

}
}
}
#endif



