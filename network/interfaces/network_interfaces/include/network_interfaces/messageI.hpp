#ifndef __INTERFACE_MESSAGE_HPP__
#define __INTERFACE_MESSAGE_HPP__


#include <vector>
#include <string>
#include <cstdint>

namespace common
{
namespace network
{
namespace interfaces
{


class MessageI
{

	public:
		using dType=std::uint8_t;
		using pType=std::vector<dType>;

		MessageI()=default;
		virtual ~MessageI()=default;

		virtual void setData(pType data) = 0;
		virtual pType &getData() = 0;
		virtual const pType &getData() const = 0;



};


}
}
}

#endif
