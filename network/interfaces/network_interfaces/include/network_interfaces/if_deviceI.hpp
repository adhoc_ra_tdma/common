#ifndef __INTERFACE_IF_DEVICE_HPP__
#define __INTERFACE_IF_DEVICE_HPP__

#include <string>
#include <cstdint>
namespace common
{
namespace network
{
namespace interfaces
{

class if_deviceI
{
	public:

		virtual ~if_deviceI()=default;


		virtual uint16_t getIndex() const = 0;
		virtual std::string getAddr() const = 0;
		virtual std::string getName() const = 0;
		virtual bool isFound() const = 0;
};


}
}
}
#endif


