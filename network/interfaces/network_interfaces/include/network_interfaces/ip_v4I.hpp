#ifndef __INTERFACE_IP_V4_HPP__
#define __INTERFACE_IP_V4_HPP__

#include <string>
#include <array>
#include <cstdint>
namespace common
{
namespace network
{
namespace interfaces
{

class ip_v4I
{
	public:

		virtual ~ip_v4I()=default;

		virtual void setAddr(const std::string &ipAddr) = 0;
		virtual std::string getAddr() const = 0;

		virtual void setBytes(const std::array<uint8_t,4> &ipAddr) = 0;
		virtual std::array<uint8_t,4> getBytes() const = 0;

		virtual void setInt(const uint32_t &ipAddr) = 0;
		virtual uint32_t getInt() const = 0;

};



}
}
}
#endif

