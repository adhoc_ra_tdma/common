#ifndef __INTERFACE_UDP_UNICAST_SOCKET_HPP__
#define __INTERFACE_UDP_UNICAST_SOCKET_HPP__

#include <functional>
#include <future>
#include <vector>
#include <cstdint>
#include <arpa/inet.h>

#include <network_interfaces/udp_endpointI.hpp>
#include <stdio.h>
#include <cstring>
#include <network_interfaces/udp_messageI.hpp>


namespace common
{
namespace network
{
namespace interfaces
{


template <class MessageType> class udp_socket_implementation_interface
{
	public:
		static_assert(std::is_base_of<MessageUDPI,MessageType>::value,"async_nonblock_reader: MessageType must extend MessageUDPI");

		virtual int16_t open() = 0;
		virtual void close() = 0;
		virtual int16_t setsockoptions() = 0;


		virtual void read(MessageType &message) = 0;
		virtual void write(const MessageType &message) = 0;
};


template <class MessageType> class udp_callback_reader_interface
{
		static_assert(std::is_base_of<MessageUDPI,MessageType>::value,"async_nonblock_reader: MessageType must extend MessageUDPI");

	public:

		virtual ~udp_callback_reader_interface()=default;

		virtual void async_read(std::function<void(const MessageType &msg)> func)=0;
		virtual void async_stop()=0;


};

template <class MessageType> class udp_writer_interface
{

		static_assert(std::is_base_of<MessageUDPI,MessageType>::value,"async_nonblock_writer: MessageType must extend MessageUDPI");

	public:

		virtual ~udp_writer_interface()=default;

		virtual void  write(const MessageType &msg)=0;


};

template <class MessageType> class udp_future_reader_interface
{
		static_assert(std::is_base_of<MessageUDPI,MessageType>::value,"async_future_reader: MessageType must extend MessageUDPI");

	public:

		virtual ~udp_future_reader_interface()=default;

		virtual std::future<MessageType> future_read()=0;
};

template <class MessageType> class udp_future_writer_interface
{
		static_assert(std::is_base_of<MessageUDPI,MessageType>::value,"async_future_writer: MessageType must extend MessageUDPI");

	public:

		virtual ~udp_future_writer_interface()=default;

		virtual std::future<void> future_write(const MessageType &msg)=0;
};

class startable_stoppable_interface
{

	public:

		virtual ~startable_stoppable_interface()=default;

		virtual void start()=0;
		virtual void restart()=0;
		virtual void stop()=0;
};


template <class MessageType> class udp_callback_socket_interface:
		public virtual startable_stoppable_interface,
		public virtual udp_callback_reader_interface<MessageType>,
		public virtual udp_writer_interface<MessageType>
{

	public:

		virtual ~udp_callback_socket_interface()=default;

};

template <class MessageType> class udp_future_socket_interface:
		public virtual udp_future_reader_interface<MessageType>,
		public virtual udp_future_writer_interface<MessageType>
{

	public:

		virtual ~udp_future_socket_interface()=default;

};

}
}
}
#endif



