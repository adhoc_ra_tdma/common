#ifndef __INTERFACE_UDP_MESSAGE_HPP__
#define __INTERFACE_UDP_MESSAGE_HPP__


#include <vector>
#include <string>
#include <cstdint>
#include <network_interfaces/messageI.hpp>
#include <network_interfaces/udp_endpointI.hpp>
namespace common
{
namespace network
{
namespace interfaces
{


class MessageUDPI:virtual public MessageI
{

	public:
		MessageUDPI()=default;
		virtual ~MessageUDPI()=default;

		virtual void setUDPEndpoint(const udp_endpointI &endpoint) = 0;
		virtual const udp_endpointI &getUDPEndpoint() const = 0;
};

}
}
}

#endif
