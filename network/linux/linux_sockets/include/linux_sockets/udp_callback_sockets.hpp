#ifndef __UDP_CALLBACK_SOCKETS_HPP__
#define __UDP_CALLBACK_SOCKETS_HPP__


#include <network_generics/udp_message.hpp>
#include <network_interfaces/udp_unicast_socketI.hpp>

namespace common
{
namespace network
{
namespace linux_implementation
{
namespace sockets
{

using MessageUDP=common::network::generics::MessageUDP;
using udp_socket_implementation_interface=common::network::interfaces::udp_socket_implementation_interface<MessageUDP>;
using udp_callback_socket_interface=common::network::interfaces::udp_callback_socket_interface<MessageUDP>;

template <class udp_socket_implementation, typename... Args>
class udp_callback_socket:
	/* extends */
	/* implements */
	public virtual udp_callback_socket_interface
{

		static_assert(std::is_base_of<udp_socket_implementation_interface,udp_socket_implementation>::value,"udp_callback_socket: udp_socket_implementation must extend udp_socket_implementation_interface");

	protected:
		using cbType=std::function<void(const MessageUDP &)>;
		using cbPointer=std::shared_ptr<cbType>;

	public:
		udp_callback_socket(Args... args, std::size_t buffer_size=65536);

		virtual ~udp_callback_socket() noexcept;

		//startable_stoppable
		virtual void start() override;
		virtual void restart() override;
		virtual void stop() override;

		//async_nonblock_reader
		virtual void async_read(cbType func) override;
		virtual void async_stop() override;

		//async_nonblock_writer
		virtual void write(const MessageUDP &msg) override;

	protected:

		std::size_t buffer_size;


		cbPointer userCallback=nullptr;

		bool end;
		std::thread t;
		std::function<void()> thread_function;

	private:
		udp_socket_implementation socket;

};



}
}
}
}


/* Implementation */

#include <linux_sockets/udp_callback_sockets.hpp>
#include <iostream>


#ifdef debugLevel
#define WAS_DEFINED_DEBUG_LEVEL
#define PREV_DEBUG_LEVEL debugLevel
#undef debugLevel
#endif

//#define debugLevel lDebug
#ifndef debugLevel
#define debugLevel lError
#endif
#include <debug_headers/debug.hpp>

namespace common
{
namespace network
{
namespace linux_implementation
{
namespace sockets
{

#define TEMPLATE_SIGNATURE template <class udp_socket_implementation, typename... Args>
#define CLASS_SIGNATURE udp_callback_socket<udp_socket_implementation, Args...>

TEMPLATE_SIGNATURE
CLASS_SIGNATURE::udp_callback_socket(Args... args, std::size_t buffer_size):
	buffer_size(buffer_size),
	socket(args...)
{
	std::cout<<"Creating Unicast Socket"<<std::endl;
	end=false;
	thread_function=[this]()
	{
		MessageUDP buffer;
		while( (!end) )
		{
			buffer.getData().clear();

			buffer.getData().resize(this->buffer_size);
			socket.read(buffer);
			if((buffer.getUDPEndpoint().getInt()!=0)||(buffer.getUDPEndpoint().getPort()!=0))
			{
				cbPointer ucb=userCallback;
				if(ucb!=nullptr)
				{
					(*ucb)(buffer);
				}
			}
		}

	};


}

TEMPLATE_SIGNATURE
CLASS_SIGNATURE::~udp_callback_socket()  noexcept
{
	stop();
}

TEMPLATE_SIGNATURE
void CLASS_SIGNATURE::start()
{
	socket.open();
	socket.setsockoptions();
	t=std::thread(thread_function);
}

TEMPLATE_SIGNATURE
void CLASS_SIGNATURE::restart()
{
	stop();
	start();
}

TEMPLATE_SIGNATURE
void CLASS_SIGNATURE::stop()
{
	end=true;
	socket.close();
	if(t.joinable())
	{
		t.join();
	}
	userCallback=cbPointer(nullptr);
}

TEMPLATE_SIGNATURE
void CLASS_SIGNATURE::async_read(cbType func)
{
	userCallback.reset(new cbType(func));

}

TEMPLATE_SIGNATURE
void CLASS_SIGNATURE::async_stop()
{
	userCallback.reset();
}

TEMPLATE_SIGNATURE
void  CLASS_SIGNATURE::write(const MessageUDP &msg)
{
	socket.write(msg);
}




}
}
}
}



// This is actually included!!! Remove what you did to debug header
#include <debug_headers/clear_debug.hpp>
#ifdef WAS_DEFINED_DEBUG_LEVEL
#define debugLevel PREV_DEBUG_LEVEL
#undef WAS_DEFINED_DEBUG_LEVEL
#undef PREV_DEBUG_LEVEL
#include <debug_headers/debug.hpp>
#endif

#undef TEMPLATE_SIGNATURE
#undef CLASS_SIGNATURE

#endif
