#ifndef __LINUX_UDP_MULTICAST_SOCKET_HPP__
#define __LINUX_UDP_MULTICAST_SOCKET_HPP__

#include <linux_sockets/udp_unicast_socket.hpp>
#include <linux_sockets/udp_endpoint.hpp>
#include <linux_sockets/if_device.hpp>
#include <linux_sockets/udp_callback_sockets.hpp>
#define RECEIVE_OUR_DATA 1

namespace common
{
namespace network
{
namespace linux_implementation
{
namespace sockets
{


class udp_multicast_socket_implementation: public udp_unicast_socket_implementation
{
		using if_device=common::network::linux_implementation::sockets::if_device;
		using udp_endpoint=common::network::generics::udp_endpoint;
		using super=udp_unicast_socket_implementation;
	public:

		udp_multicast_socket_implementation(const if_device &dev, const udp_endpoint &multicastAddress);

		virtual ~udp_multicast_socket_implementation() noexcept;


		virtual int16_t setsockoptions() override;

	protected:

		if_device listenInterface;
		udp_endpoint multicastAddress;

	private:
};


}
}
}
}
#endif
