#ifndef __LINUX_UDP_ENDPOINT_HPP__
#define __LINUX_UDP_ENDPOINT_HPP__


#include <network_generics/udp_endpoint.hpp>

#include <cstring>
#include <string>
#include <cstdint>
#include <unistd.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
namespace common
{

namespace network
{

namespace linux_implementation
{

namespace sockets
{



using generic_udp_endpoint=common::network::generics::udp_endpoint;
class udp_endpoint: public generic_udp_endpoint
{

	public:
		udp_endpoint();

		udp_endpoint(const udp_endpoint &other);
		udp_endpoint &operator=(const udp_endpoint &other);

		udp_endpoint(const generic_udp_endpoint &other);
		udp_endpoint &operator=(const generic_udp_endpoint &other);

		udp_endpoint(udp_endpoint &&other);
		udp_endpoint &operator=(udp_endpoint &&other);

		udp_endpoint(std::string ipAddr, uint16_t port);

		using native_endpoint=struct sockaddr_in;

		native_endpoint getLinuxEndpoint();
		void setLinuxEndpoint(const native_endpoint &endpoint);



	private:

};
}
}
}
}
#endif

