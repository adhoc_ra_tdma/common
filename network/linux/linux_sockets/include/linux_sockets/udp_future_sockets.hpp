#ifndef __UDP_FUTURE_SOCKETS_HPP__
#define __UDP_FUTURE_SOCKETS_HPP__


#include <network_generics/udp_message.hpp>
#include <network_interfaces/udp_unicast_socketI.hpp>

namespace common
{
namespace network
{
namespace linux_implementation
{
namespace sockets
{

using MessageUDP=common::network::generics::MessageUDP;
using udp_socket_implementation_interface=common::network::interfaces::udp_socket_implementation_interface<MessageUDP>;
using udp_future_socket_interface=common::network::interfaces::udp_future_socket_interface<MessageUDP>;

template <class udp_socket_implementation, typename... Args>
class udp_future_socket:
	/* extends */
	/* implements */
	public virtual udp_future_socket_interface
{

		static_assert(std::is_base_of<udp_socket_implementation_interface,udp_socket_implementation>::value,"udp_future_socket: udp_socket_implementation must extend udp_socket_implementation_interface");

	protected:
		using receiveFunction=std::function<MessageUDP()>;
		using sendFunction=std::function<void(MessageUDP)>;

	public:
		udp_future_socket(Args... args, std::size_t buffer_size=65536);

		virtual ~udp_future_socket() noexcept;

		//async_future_reader
		virtual std::future<MessageUDP> future_read() override;
		//async_future_writer
		virtual std::future<void> future_write(const MessageUDP &msg) override;



	protected:

		std::size_t buffer_size;

		receiveFunction rFunction;
		sendFunction sFunction;

	private:
		udp_socket_implementation socket;
};



}
}
}
}


/* Implementation */

#include <linux_sockets/udp_callback_sockets.hpp>
#include <iostream>

#ifdef debugLevel
#define WAS_DEFINED_DEBUG_LEVEL
#define PREV_DEBUG_LEVEL debugLevel
#undef debugLevel
#endif

//#define debugLevel lDebug
#ifndef debugLevel
#define debugLevel lError
#endif
#include <debug_headers/debug.hpp>


namespace common
{
namespace network
{
namespace linux_implementation
{
namespace sockets
{

#define TEMPLATE_SIGNATURE template <class udp_socket_implementation, typename... Args>
#define CLASS_SIGNATURE udp_future_socket<udp_socket_implementation, Args...>

TEMPLATE_SIGNATURE
CLASS_SIGNATURE::udp_future_socket(Args... args, std::size_t buffer_size):
	buffer_size(buffer_size),
	socket(args...)
{
	std::cout<<"Creating udp_future_socket"<<std::endl;
	if(socket.open()<0)
	{
		PERROR(std::cerr,"Failed to open socket");
	}
	if(socket.setsockoptions()<0)
	{
		PERROR(std::cerr,"Failed to set socket options... Closing socket");
		socket.close();
	}

	rFunction=[this]()
	{
		MessageUDP buffer;

		buffer.getData().resize(this->buffer_size);
		do
		{
			socket.read(buffer);
		}
		while((buffer.getUDPEndpoint().getInt()==0)&&(buffer.getUDPEndpoint().getPort()==0));
		return buffer;
	};

	sFunction=[this](MessageUDP msg)
	{
		socket.write(msg);
	};
}

TEMPLATE_SIGNATURE
CLASS_SIGNATURE::~udp_future_socket() noexcept
{
	socket.close();
}

TEMPLATE_SIGNATURE
std::future<MessageUDP> CLASS_SIGNATURE::future_read()
{
	return std::async(std::launch::async|std::launch::deferred,rFunction);

}

TEMPLATE_SIGNATURE
std::future<void> CLASS_SIGNATURE::future_write(const MessageUDP &msg)
{
	return std::async(std::launch::async|std::launch::deferred,sFunction,msg);

}



}
}
}
}

// This is actually included!!! Remove what you did to debug header
#include <debug_headers/clear_debug.hpp>
#ifdef WAS_DEFINED_DEBUG_LEVEL
#define debugLevel PREV_DEBUG_LEVEL
#undef WAS_DEFINED_DEBUG_LEVEL
#undef PREV_DEBUG_LEVEL
#include <debug_headers/debug.hpp>
#endif

#undef TEMPLATE_SIGNATURE
#undef CLASS_SIGNATURE


#endif



