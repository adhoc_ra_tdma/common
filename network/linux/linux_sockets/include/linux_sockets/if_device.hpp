#ifndef __LINUX_IF_DEVICE_HPP__
#define __LINUX_IF_DEVICE_HPP__


#include <network_interfaces/if_deviceI.hpp>
#include <cstring>
#include <sys/socket.h>
#include <linux/if.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <string>
#include <sstream>
#include <cassert>



namespace common
{
namespace network
{
namespace linux_implementation
{
namespace sockets
{
using if_deviceI=common::network::interfaces::if_deviceI;
class if_device: public if_deviceI
{

	public:
		if_device()=delete;

		if_device(std::string ifname);

		if_device(const if_deviceI &other);
		if_device &operator=(const if_deviceI &other);

		if_device(const if_device &other);
		if_device &operator=(const if_device &other);

		if_device(if_device &&other);
		if_device &operator=(if_device &&other);


		~if_device() noexcept;



		virtual uint16_t getIndex() const override;
		virtual std::string getAddr() const override;
		virtual std::string getName() const override;
		virtual bool isFound() const override;

	private:
		uint16_t ifIndex;
		std::string ipAddr;
		std::string name;
		bool found=false;

		void init_device(std::string ifname);

};

}
}
}
}
#endif

