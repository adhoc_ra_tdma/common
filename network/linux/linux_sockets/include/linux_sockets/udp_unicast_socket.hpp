#ifndef __LINUX_UDP_UNICAST_SOCKET_HPP__
#define __LINUX_UDP_UNICAST_SOCKET_HPP__

#include <network_interfaces/udp_unicast_socketI.hpp>
#include <network_generics/udp_message.hpp>
#include <list>
#include <cassert>
#include <thread>

namespace common
{
namespace network
{
namespace linux_implementation
{
namespace sockets
{

using MessageUDP=common::network::generics::MessageUDP;
using udp_socket_implementation_interface=common::network::interfaces::udp_socket_implementation_interface<MessageUDP>;

class udp_unicast_socket_implementation:public udp_socket_implementation_interface
{
	public:

		udp_unicast_socket_implementation(uint16_t port);

		virtual ~udp_unicast_socket_implementation() noexcept;


		virtual int16_t open();
		virtual void close();
		virtual int16_t setsockoptions();


		virtual void read(MessageUDP &message) override;
		virtual	void write(const MessageUDP &message) override;


	protected:

		int sock=-1;
		uint16_t listenPort;

	private:
};













}
}
}
}


#endif
