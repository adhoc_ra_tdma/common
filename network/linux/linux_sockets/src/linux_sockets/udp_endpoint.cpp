#include <linux_sockets/udp_endpoint.hpp>

namespace common
{
namespace network
{
namespace linux_implementation
{
namespace sockets
{



udp_endpoint::udp_endpoint()
{
}

udp_endpoint::udp_endpoint(const udp_endpoint &other):
	generic_udp_endpoint(other)
{
}

udp_endpoint &udp_endpoint::operator=(const udp_endpoint &other)
{
	if(this!=&other)
	{
		generic_udp_endpoint::operator =(other);
	}
	return *this;
}

udp_endpoint::udp_endpoint(const generic_udp_endpoint &other):
	generic_udp_endpoint(other)
{

}

udp_endpoint &udp_endpoint::operator=(const generic_udp_endpoint &other)
{
	if(this!=&other)
	{
		generic_udp_endpoint::operator =(other);
	}
	return *this;
}

udp_endpoint::udp_endpoint(udp_endpoint &&other):
	generic_udp_endpoint(std::move(other))
{
}

udp_endpoint &udp_endpoint::operator=(udp_endpoint &&other)
{
	if(this!=&other)
	{
		generic_udp_endpoint::operator =(std::move(other));
	}
	return *this;
}


udp_endpoint::udp_endpoint(std::string ipAddr, uint16_t port):
	generic_udp_endpoint(ipAddr, port)
{
}


udp_endpoint::native_endpoint udp_endpoint::getLinuxEndpoint()
{
	struct sockaddr_in endpoint;
	bzero(&endpoint, sizeof(struct sockaddr_in));
	endpoint.sin_family = AF_INET;
	endpoint.sin_port = htons(getPort());
	endpoint.sin_addr.s_addr = inet_addr(getAddr().c_str());
	return endpoint;
}

void udp_endpoint::setLinuxEndpoint(const native_endpoint &endpoint)
{
	setPort(ntohs(endpoint.sin_port));
	setAddr(std::string(inet_ntoa(endpoint.sin_addr)));
}

}
}
}
}
