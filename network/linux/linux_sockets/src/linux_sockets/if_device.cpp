#include <linux_sockets/if_device.hpp>

#ifndef debugLevel
#define debugLevel lDebug
#endif
#include <debug_headers/debug.hpp>

namespace common
{
namespace network
{
namespace linux_implementation
{
namespace sockets
{

if_device::~if_device()
{

}

if_device::if_device(std::string ifname):
	name(ifname)
{
	init_device(ifname);
}

if_device::if_device(const network::interfaces::if_deviceI &other):
	ifIndex(other.getIndex()),
	ipAddr(other.getAddr()),
	name(other.getName()),
	found(other.isFound())
{

}

if_device &if_device::operator=(const network::interfaces::if_deviceI &other)
{
	if(&other!=this)
	{
		this->ifIndex=other.getIndex();
		this->ipAddr=other.getAddr();
		this->name=other.getName();
		this->found=other.isFound();
	}
	return *this;
}

if_device::if_device(const if_device &other):
	ifIndex(other.ifIndex),
	ipAddr(other.ipAddr),
	name(other.name),
	found(other.found)
{
}

if_device &if_device::operator=(const if_device &other)
{
	if(&other!=this)
	{
		this->ifIndex=other.ifIndex;
		this->ipAddr=other.ipAddr;
		this->name=other.name;
		this->found=other.found;
	}
	return *this;
}

if_device::if_device(if_device &&other):
	ifIndex(std::move(other.ifIndex)),
	ipAddr(std::move(other.ipAddr)),
	name(std::move(other.name)),
	found(std::move(other.found))
{
}

if_device &if_device::operator=(if_device &&other)
{
	if(&other!=this)
	{
		this->ifIndex=std::move(other.ifIndex);
		this->ipAddr=std::move(other.ipAddr);
		this->name=std::move(other.name);
		this->found=std::move(other.found);
	}
	return *this;
}

uint16_t if_device::getIndex() const
{
	return ifIndex;
}


std::string if_device::getAddr() const
{
	return ipAddr;
}

std::string if_device::getName() const
{
	return name;
}

bool if_device::isFound() const
{
	return found;
}


void if_device::init_device(std::string ifname)
{
	int fd;
	int ret;
	struct ifreq if_info;

	memset(&if_info, 0, sizeof(if_info));
	strncpy(if_info.ifr_name, ifname.c_str(), IFNAMSIZ-1);

	if ((fd=socket(AF_INET, SOCK_DGRAM, 0)) == -1)
	{
		assert(fd!=-1);
		PERROR(std::cerr,"socket");
	}

	if ((ret=ioctl(fd, SIOCGIFINDEX, &if_info)) == -1)
	{
		PERROR(std::cerr,"ioctl");
		close(fd);
		assert(ret!=-1);
	}
	ifIndex = if_info.ifr_ifindex;

	if (ioctl(fd, SIOCGIFADDR, &if_info) == -1)
	{
		PERROR(std::cerr,"ioctl");
		close(fd);
	}
	close(fd);

	std::stringstream ss;
	ss<<(int) ((unsigned char *) if_info.ifr_hwaddr.sa_data)[2]<<"."<<
																 (int) ((unsigned char *) if_info.ifr_hwaddr.sa_data)[3]<<"."<<
																 (int) ((unsigned char *) if_info.ifr_hwaddr.sa_data)[4]<<"."<<
																 (int) ((unsigned char *) if_info.ifr_hwaddr.sa_data)[5];
	ipAddr=ss.str();
	found=true;
}


}
}
}
}
