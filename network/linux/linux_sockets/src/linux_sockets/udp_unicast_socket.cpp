#include <linux_sockets/udp_unicast_socket.hpp>
#include <linux_sockets/udp_endpoint.hpp>
#include <cassert>
#include <iostream>

//#define debugLevel lInfo
#ifndef debugLevel
#define debugLevel lError
#endif
#include <debug_headers/debug.hpp>



namespace common
{
namespace network
{
namespace linux_implementation
{
namespace sockets
{



udp_unicast_socket_implementation::udp_unicast_socket_implementation(uint16_t port):
	listenPort(port)
{
	std::cout<<"Creating Unicast Socket"<<std::endl;
}

udp_unicast_socket_implementation::~udp_unicast_socket_implementation()
{
	close();
}

int16_t udp_unicast_socket_implementation::open()
{
	if((sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
	{
		PERROR(std::cerr,"Opening udp_unicast_blockable_socket");
	}
	return sock;
}


int16_t udp_unicast_socket_implementation::setsockoptions()
{
	std::cout<<"Setting Unicast options..."<<std::endl;
	udp_endpoint bindEndpoint("0.0.0.0",listenPort);
	udp_endpoint::native_endpoint endp=bindEndpoint.getLinuxEndpoint();
	if(bind(sock, (struct sockaddr *) &endp, sizeof(struct sockaddr_in)) < 0)
	{
		PERROR(std::cerr,"Binding udp_unicast_blockable_socket");
		return (-1);
	}
	return 0;

}

void udp_unicast_socket_implementation::read(MessageUDP &message)
{
	udp_endpoint other_node;
	udp_endpoint::native_endpoint endpoint;

	socklen_t endpoint_length=sizeof(udp_endpoint::native_endpoint);

	memset(&endpoint,0,endpoint_length);

	int64_t rcvd_size;
	MessageUDP::pType &data=message.getData();
	while((rcvd_size=recvfrom(sock, data.data(), data.size(), 0, (struct sockaddr *)(&endpoint), &endpoint_length)) == -1)
	{
		if(errno==EINTR)
		{
			PERROR(std::cout, "An unknown error occured when receiving, not fatal continuing!");
		}
		else
		{
			PERROR(std::cout, "Error "<<errno<<" while receiving from socket... Could not receive any information:" <<strerror(errno)<<"!");
			message=std::move(MessageUDP("0.0.0.0",0,std::vector<uint8_t>(0)));
			return;
		}
	}
	message.getData().resize(rcvd_size);
	other_node.setLinuxEndpoint(endpoint);
	message.setUDPEndpoint(other_node);
	PINFO(std::cout,"Received from "<<other_node.getAddr()<<":"<<other_node.getPort());

	return;

}

void udp_unicast_socket_implementation::write(const network::generics::MessageUDP &message)
{
	udp_endpoint endpoint(message.getUDPEndpoint().getAddr(),message.getUDPEndpoint().getPort());
	udp_endpoint::native_endpoint native_endpoint=endpoint.getLinuxEndpoint();
	uint64_t sent_bytes;
	if((sent_bytes=sendto(sock, message.getData().data(), message.getData().size(), 0, (struct sockaddr *)&native_endpoint, sizeof (udp_endpoint::native_endpoint)))!=message.getData().size())
	{
		PERROR(std::cerr,"Not all bytes were sent ("<<sent_bytes<<" from "<<message.getData().size()<<") "<<strerror(errno));
	}
}


void udp_unicast_socket_implementation::close()
{
	if(sock != -1)
	{
		shutdown(sock, SHUT_RDWR);
	}

}


}
}
}
}
