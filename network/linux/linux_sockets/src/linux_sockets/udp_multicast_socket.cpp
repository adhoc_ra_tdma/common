#include <linux_sockets/udp_multicast_socket.hpp>

#include <cassert>
#include <iostream>

#ifndef debugLevel
#define debugLevel lError
#endif
#include <debug_headers/debug.hpp>

namespace common
{
namespace network
{
namespace linux_implementation
{
namespace sockets
{


udp_multicast_socket_implementation::udp_multicast_socket_implementation(const if_device &dev, const network::generics::udp_endpoint &multicastAddress):
	udp_unicast_socket_implementation(multicastAddress.getPort()),
	listenInterface(dev),
	multicastAddress(multicastAddress)
{

}

udp_multicast_socket_implementation::~udp_multicast_socket_implementation()
{

}

int16_t udp_multicast_socket_implementation::setsockoptions()
{
	std::cout<<"Setting Multicast options..."<<std::endl;
	struct ip_mreqn mreqn;

	int opt = 1;
	if((setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt))) < 0)
	{
		PERROR(std::cerr,"setsockopt");
		return (-1);
	}

	super::setsockoptions();

	memset((void *) &mreqn, 0, sizeof(mreqn));
	mreqn.imr_ifindex=listenInterface.getIndex();
	mreqn.imr_multiaddr.s_addr=inet_addr(multicastAddress.getAddr().c_str());
	mreqn.imr_address.s_addr=inet_addr(listenInterface.getAddr().c_str());

	if((setsockopt(sock, IPPROTO_IP, IP_MULTICAST_IF, &mreqn, sizeof(mreqn))) == -1)
	{
		PERROR(std::cerr,"set interface");
		return (-1);
	}


	if((setsockopt(sock, IPPROTO_IP, IP_ADD_MEMBERSHIP, &mreqn, sizeof(mreqn))) == -1)
	{
		PERROR(std::cerr,"join multicast");
		return (-1);
	}


	opt = RECEIVE_OUR_DATA;
	if((setsockopt(sock, IPPROTO_IP, IP_MULTICAST_LOOP, &opt, sizeof(opt))) == -1)
	{
		PERROR(std::cerr,"loop back");
		return (-1);
	}

	return 0;
}




}
}
}
}
