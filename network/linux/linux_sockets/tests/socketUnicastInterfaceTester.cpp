#include <linux_sockets/udp_unicast_socket.hpp>
#include <linux_sockets/udp_callback_sockets.hpp>
#include <linux_sockets/udp_future_sockets.hpp>

#include <linux_sockets/if_device.hpp>

#include <iostream>

#include <future>

#include <signal.h>

std::promise<void> promised_end;

static void signal_catch(int sig)
{
	promised_end.set_value();
}
using MessageUDP=common::network::generics::MessageUDP;

using udp_unicast_socket=common::network::linux_implementation::sockets::udp_unicast_socket_implementation;

using udp_callback_socket=common::network::linux_implementation::sockets::udp_callback_socket<udp_unicast_socket, uint16_t>;
using udp_future_socket=common::network::linux_implementation::sockets::udp_future_socket<udp_unicast_socket, uint16_t>;

using network_device_interface=common::network::linux_implementation::sockets::if_device;

int main(int argc, char **argv)
{
	int app=atoi(argv[2]);
	std::string iface=std::string(argv[1]);
	std::future<void> end = promised_end.get_future();
	if(signal(SIGINT, signal_catch) == SIG_ERR)
	{
		std::cout<<"error: signal"<<std::endl;
		return -1;
	}


	udp_callback_socket *sock;
	udp_future_socket *future_sock;


	std::function<void(const MessageUDP &)> cb=[&sock](const MessageUDP &msg)
	{
		std::cout<<"Received data from "<<msg.getUDPEndpoint().getAddr()<<":"<<msg.getUDPEndpoint().getPort()<<" containing:"<<std::endl;
		for (const uint8_t &d:msg.getData())
		{
			std::cout<<(uint16_t)d<<" ";
		}
		std::cout<<std::endl;
	};

	std::cout<<"Listening to interface: "<<iface<<std::endl;
	network_device_interface interface(iface);
	std::cout<<"Listening to interface: "<<interface.getAddr()<<std::endl;


	std::cout<<"Started"<<std::endl;

	if(app==1)
	{

		sock=new udp_callback_socket(12345);
		sock->start();
		sock->async_read(cb);
		end.get();
		delete(sock);

	}
	else if(app==2)
	{
		sock=new udp_callback_socket(20001);
		sock->start();
		sock->async_read(cb);
		std::cout<<"Sending data..."<<std::endl;
		std::vector<uint8_t> data={1,2,3,4,5,6,7,8,};
		MessageUDP msg("192.168.104.39",12345,data);
		sock->write(msg);
	}
	else if(app==3)
	{
		sock=new udp_callback_socket(50001);
		sock->start();
		sock->async_read([&sock](const MessageUDP &msg)
		{
			MessageUDP msg_cb;
			msg_cb.setData(msg.getData());
			msg_cb.getUDPEndpoint().setAddr("127.0.0.1");
			msg_cb.getUDPEndpoint().setPort(50003);
			std::cout<<"Sending data..."<<std::endl;
			sock->write(msg_cb);
		});
		end.get();
		delete(sock);

	}
	else if(app==4)
	{
		future_sock=new udp_future_socket(12345);
		std::future<MessageUDP> future_msg=future_sock->future_read();
		MessageUDP msg=future_msg.get();
		std::cout<<"Received data from "<<msg.getUDPEndpoint().getAddr()<<":"<<msg.getUDPEndpoint().getPort()<<" containing:"<<std::endl;
		for (const uint8_t &d:msg.getData())
		{
			std::cout<<(uint16_t)d<<" ";
		}
		std::cout<<std::endl;

		end.get();
		delete(future_sock);

	}
	else if(app==5)
	{
		future_sock=new udp_future_socket(12345);
		std::cout<<"Sending data..."<<std::endl;
		std::vector<uint8_t> data={1,2,3,4,5,6,7,8,};
		MessageUDP msg("192.168.104.39",12345,data);
		std::future<void> future_write=future_sock->future_write(msg);
		future_write.get();
		delete(future_sock);
	}
	std::cout << "Exiting cleanly"<<std::endl;

	return 0;
}

