#include <linux_sockets/udp_multicast_socket.hpp>
#include <linux_sockets/udp_callback_sockets.hpp>
#include <linux_sockets/udp_future_sockets.hpp>

#include <iostream>

#include <future>

#include <signal.h>

std::promise<void> promised_end;

static void signal_catch(int sig)
{
	promised_end.set_value();
}


using MessageUDP=common::network::generics::MessageUDP;

using udp_multicast_socket_implementation=common::network::linux_implementation::sockets::udp_multicast_socket_implementation;


using network_device_interface=common::network::linux_implementation::sockets::if_device;
using udp_endpoint_interface=common::network::linux_implementation::sockets::udp_endpoint;

using udp_callback_socket=common::network::linux_implementation::sockets::udp_callback_socket<udp_multicast_socket_implementation, network_device_interface,udp_endpoint_interface>;
using udp_future_socket=common::network::linux_implementation::sockets::udp_future_socket<udp_multicast_socket_implementation, network_device_interface,udp_endpoint_interface>;

int main(int argc, char **argv)
{
	int app=atoi(argv[2]);
	std::string iface=std::string(argv[1]);

	std::future<void> end = promised_end.get_future();
	if(signal(SIGINT, signal_catch) == SIG_ERR)
	{
		std::cout<<"error: signal"<<std::endl;
		return -1;
	}



	udp_callback_socket *sock;
	udp_future_socket *future_sock;

	std::function<void(const MessageUDP&)> cb=[&sock](const MessageUDP &msg)
	{
		std::cout<<"Received data from "<<msg.getUDPEndpoint().getAddr()<<":"<<msg.getUDPEndpoint().getPort()<<" containing:"<<std::endl;
		for (const uint8_t &d:msg.getData())
		{
			std::cout<<(uint16_t)d<<" ";
		}
		std::cout<<std::endl;
	};

	std::string mCastIP =std::string("224.16.32.39");


	std::cout<<"Listening to interface: "<<iface<<std::endl;
	network_device_interface interface(iface);
	std::cout<<"Listening to interface: "<<interface.getAddr()<<" "<<interface.getIndex()<<std::endl;
	std::cout<<"Started"<<std::endl;


	if(app==1)
	{
		sock=new udp_callback_socket(interface, udp_endpoint_interface(mCastIP,20000));
		sock->start();
		sock->async_read(cb);
		end.get();
		delete(sock);

	}
	else if(app==2)
	{
		sock=new udp_callback_socket(interface, udp_endpoint_interface(mCastIP,20000));
		sock->start();
		std::vector<uint8_t> data={1,2,3,4,5,6,7,8,};
		common::network::generics::MessageUDP msg(mCastIP,20000,data);

		sock->write(msg);
		delete(sock);
	}
	else if(app==4)
	{
		future_sock=new udp_future_socket(interface, udp_endpoint_interface(mCastIP,20000));
		MessageUDP msg=future_sock->future_read().get();
		std::cout<<"Received data from "<<msg.getUDPEndpoint().getAddr()<<":"<<msg.getUDPEndpoint().getPort()<<" containing:"<<std::endl;
		for (const uint8_t &d:msg.getData())
		{
			std::cout<<(uint16_t)d<<" ";
		}
		std::cout<<std::endl;
		delete(future_sock);
	}
	else if(app==5)
	{
		future_sock=new udp_future_socket(interface, udp_endpoint_interface(mCastIP,20000));
		std::vector<uint8_t> data={1,2,3,4,5,6,7,8,};
		common::network::generics::MessageUDP msg(mCastIP,20000,data);

		future_sock->future_write(msg).get();
		delete(future_sock);
	}

	std::cout << "Exiting cleanly"<<std::endl;

	return 0;
}

