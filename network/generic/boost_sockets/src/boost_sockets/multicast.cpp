

#include <boost/bind.hpp>


#include<boost/version.hpp>
#if BOOST_VERSION < 104700
#include <buffer_copy.hpp>
#endif

#include "boost_sockets/multicast.hpp"


#define TTL 64

//#define RECEIVE_OUR_DATA true
#define RECEIVE_OUR_DATA false
#if RECEIVE_OUR_DATA==true
#warning RECEIVE_OUR_DATA is true...............................................................................................................................................................................................................................................................................................................................................................
#endif

#ifndef debugLevel
	#define debugLevel lError
#endif

#include "debug_headers/debug.hpp"

namespace common
{
namespace Sockets
{

        //	*************************
        //  Open Socket
        //
        //
		Multicast::Multicast(std::string my_address, std::string multicast_addr, uint16_t mPort):
			super(mPort)
        {
            PDEBUG(std::cout, "Constructor");

            boost::system::error_code ec;



            // Bind to outbound interface
			sock.set_option(boost::asio::ip::multicast::outbound_interface(boost::asio::ip::address_v4::from_string(my_address)),ec);
            if(ec)
            {
                    PERROR(std::cout,"Error Binding Interface...: "<<ec);
            }
            else
            {
                    PDEBUG(std::cout, "Interface Bound OK...");
            }

            // Listening to our transmission
            sock.set_option(boost::asio::ip::multicast::enable_loopback(RECEIVE_OUR_DATA), ec);
            if(ec)
            {
                    PERROR(std::cout,"Error setting loopback...: "<<ec);
            }
            else
            {
                    PDEBUG(std::cout, (RECEIVE_OUR_DATA?"Loopback enabled OK...":"Loopback disabled OK..."));
            }




            // Join the multicast group on correct interface.
            sock.set_option(boost::asio::ip::multicast::join_group(boost::asio::ip::address_v4::from_string(multicast_addr), boost::asio::ip::address_v4::from_string(my_address)), ec);
            if(ec)
            {
                    PERROR(std::cout,"Error Joining Group...: "<<ec);
            }
            else
            {
                    PDEBUG(std::cout, "Joined Group OK...");
            }


        }



        //	*************************
        //  Close Socket
        //
        //  Input:
        //		int multiSocket = socket descriptor
        //
		Multicast::~Multicast()
		{

		}


}
}
