#include <boost_sockets/message.hpp>

namespace common
{
namespace Sockets
{

Message::Message(pType data, std::string address, uint16_t port):
	data(data),
	endpoint(boost::asio::ip::address::from_string(address),port)
{
}
Message::Message(pType data, boost::asio::ip::udp::endpoint endpoint):
	data(data),
	endpoint(endpoint)
{
}


pType &Message::getDataRef()
{
	return data;
}

boost::asio::ip::udp::endpoint &Message::getBoostEndpointRef()
{
	return endpoint;
}


void Message::setAddress(std::string address)
{
	endpoint=boost::asio::ip::udp::endpoint(boost::asio::ip::address::from_string(address),endpoint.port());
}

void Message::setPort(uint16_t port)
{
	endpoint=boost::asio::ip::udp::endpoint(endpoint.address(),port);
}



pType Message::getData() const
{
	return data;
}

std::string Message::getAddress() const
{
	return endpoint.address().to_string();
}

std::uint16_t Message::getPort() const
{
	return endpoint.port();
}

boost::asio::ip::udp::endpoint Message::getBoostEndpoint() const
{
	return endpoint;
}


}
}
