
#include<boost/version.hpp>
#if BOOST_VERSION < 104700
#include <buffer_copy.hpp>
#endif

#include "boost_sockets/unicast.hpp"
#include <boost/bind.hpp>

#ifndef debugLevel
#define debugLevel lError
#endif


#include "debug_headers/debug.hpp"


namespace common
{
namespace Sockets
{


Unicast::Unicast(uint16_t listen_port):
	work(io),
	t([this](){this->io.run();}),
	sock(io),
	source_endpoint()
{
	boost::system::error_code ec;
	PDEBUG(std::cout, "Constructor");

	// Open socket
	sock.open(boost::asio::ip::udp::v4(),ec);
	if(ec)
	{
		PERROR(std::cout,"Error Open Socket...: "<<ec);
	}
	else
	{
		PDEBUG(std::cout, "Socket Open... ");
	}

	// Create the socket so that multiple may be bound to the same address.
	sock.set_option(boost::asio::ip::udp::socket::reuse_address(true), ec);
	if(ec)
	{
		PERROR(std::cout,"Error Reuse Address...: "<<ec);
	}
	else
	{
		PDEBUG(std::cout, "Reuse Address OK...");
	}

	// Bind socket
	sock.bind(endpoint(boost::asio::ip::udp::v4(), listen_port), ec);
	if(ec)
	{
		PERROR(std::cout,"Error Binding Socket...: "<<ec);
	}
	else
	{
		PDEBUG(std::cout, "Socket Bound OK...");
	}

}

Unicast::~Unicast()
{
	async_stop();
	io.stop();
	while(!io.stopped()) usleep(1);
	t.join();
	PDEBUG(std::cout, "Closing Socket... ");
	boost::system::error_code ec;
	sock.close(ec);
	if (ec)
	{
		PERROR(std::cout, "Error closing socket");
	}
	PDEBUG(std::cout, "Close Socket Done...");


}


void Unicast::async_stop()
{
	sock.cancel();
	localBuffer.clear();
}

void Unicast::async_read(function<void(const Message )> func)
{
	async_stop();
	uint16_t max_size=-1;
	localBuffer.resize(max_size);
	std::function<void(const boost::system::error_code& ec, size_t size)> cb_fun=[func, this](const boost::system::error_code& ec, size_t size)
	{
		if(ec == boost::system::errc::success)
		{
			localBuffer.resize(size);
			func( Message(localBuffer,source_endpoint) );
		}
	};

	sock.async_receive_from(boost::asio::buffer(localBuffer), source_endpoint, cb_fun);
}

future<size_t>  Unicast::async_read(Message msg)
{
	async_stop();
	return sock.async_receive_from( boost::asio::buffer(msg.getDataRef()), msg.getBoostEndpointRef(), boost::asio::use_future);
}

void Unicast::read(Message msg)
{
	async_stop();
	sock.receive_from(boost::asio::buffer(msg.getDataRef()), msg.getBoostEndpointRef());
}

size_t Unicast::write(Message msg)
{
	sock.send_to(boost::asio::buffer(msg.getDataRef()),msg.getBoostEndpointRef());
}

future<size_t> Unicast::async_write(Message msg)
{
	return sock.async_send_to(boost::asio::buffer(msg.getDataRef()),msg.getBoostEndpointRef(),boost::asio::use_future);
}

void Unicast::async_write(std::function<void(Message msg)> f, Message msg)
{

	std::function<void(const boost::system::error_code& error,std::size_t bytes_transferred)> cb_fun=[f, msg,this](const boost::system::error_code& ec, std::size_t size)
	{
		if(ec == boost::system::errc::success)
		{
			localBuffer.resize(size);
			f( msg );
		}
	};

	sock.async_send_to(boost::asio::buffer(msg.getDataRef()), msg.getBoostEndpoint(), cb_fun);
}



socket &Unicast::getBoostSocket()
{
	return sock;
}

}
}

