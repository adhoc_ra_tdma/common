#ifndef __BOOSTUNICAST_HPP__
#define __BOOSTUNICAST_HPP__



#include <boost/asio.hpp>
#include <boost/asio/use_future.hpp>

#include <boost_sockets/message.hpp>

#include <future>
#include<cstdint>
#include <functional>


namespace common
{
namespace Sockets
{

using socket=boost::asio::ip::udp::socket;
using endpoint=boost::asio::ip::udp::endpoint;
using io_service=boost::asio::io_service;

using std::thread;
using std::size_t;
using std::function;
using std::future;

class Unicast
{
	public:

		Unicast(uint16_t listen_port);

		~Unicast();

		virtual void async_stop();

		virtual void async_read(function<void(const Message msg)> func);
		virtual future<size_t> async_read(Message msg);
		virtual void read(Message msg);

		virtual size_t write(Message msg);
		virtual future<size_t> async_write(Message msg);
		virtual void async_write(function<void(Message msg)> f, Message msg);

		socket &getBoostSocket();


	private:
		io_service io;
		io_service::work work;
		thread t;
		pType localBuffer;

	protected:

		endpoint source_endpoint;
		socket sock;

		
};
}
}



#endif
