#ifndef __BOOSTMESSAGE_HPP__
#define __BOOSTMESSAGE_HPP__

#include <vector>
#include <boost/asio.hpp>
#include <cstdint>

namespace common
{
namespace Sockets
{

using pType=std::vector<uint8_t>;

class Message
{

	public:


		Message()=default;
		Message(pType data, std::string address, uint16_t port);
		Message(pType data, boost::asio::ip::udp::endpoint endpoint);

		~Message()=default;

		pType &getDataRef();
		boost::asio::ip::udp::endpoint &getBoostEndpointRef();

		void setAddress(std::string address);
		void setPort(uint16_t port);


		pType getData() const;
		std::string getAddress() const;
		uint16_t getPort() const;
		boost::asio::ip::udp::endpoint getBoostEndpoint() const;


	protected:
		pType data;
		boost::asio::ip::udp::endpoint endpoint;

};

}
}

#endif
