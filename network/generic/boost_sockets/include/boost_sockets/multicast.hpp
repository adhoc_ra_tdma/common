#ifndef __BOOSTMULTICAST_HPP__
#define __BOOSTMULTICAST_HPP__

#include <boost/asio.hpp>
#include <boost/function.hpp>

#include <vector>
#include<cstdint>
#include <boost_sockets/unicast.hpp>





namespace common
{
namespace Sockets
{

class Multicast: public Unicast
{

	public:
		Multicast(std::string my_address, std::string multicast_addr, uint16_t listen_port);

		~Multicast();

	private:

	private:
	protected:
		typedef Unicast super;


};
}
}
#endif // MULTICAST_HPP
