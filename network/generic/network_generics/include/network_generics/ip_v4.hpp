#ifndef __GENERIC_IP_V4_HPP__
#define __GENERIC_IP_V4_HPP__

#include <network_interfaces/ip_v4I.hpp>
#include <cstdint>
#include <string>
namespace common
{
namespace network
{
namespace generics
{


using ip_v4I=common::network::interfaces::ip_v4I;
class ipv4: virtual public ip_v4I
{
	public:

		ipv4();

		ipv4(const std::string &ipAddr);
		ipv4(const std::array<uint8_t,4> &ipAddr);
		ipv4(const uint32_t &ipAddr);

		ipv4(const ipv4 &other)=default;
		ipv4 &operator=(const ipv4 &other)=default;

		ipv4(ipv4 &&other)=default;
		ipv4 &operator=(ipv4 &&other)=default;

		virtual ~ipv4()=default;


		virtual void setAddr(const std::string &ipAddr) override;
		virtual std::string getAddr() const override;

		virtual void setBytes(const std::array<uint8_t,4> &ipAddr) override;
		virtual std::array<uint8_t,4> getBytes() const override;

		virtual void setInt(const uint32_t &ipAddr) override;
		virtual uint32_t getInt() const override;

	private:
		uint32_t ipV4;
};




}
}
}
#endif

