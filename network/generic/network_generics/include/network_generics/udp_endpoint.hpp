#ifndef __GENERIC_UDP_ENDPOINT_HPP__
#define __GENERIC_UDP_ENDPOINT_HPP__


#include <network_interfaces/udp_endpointI.hpp>
#include <network_generics/ip_v4.hpp>

#include <string>
#include <cstdint>

namespace common
{

namespace network
{

namespace generics
{



using udp_endpointI=common::network::interfaces::udp_endpointI;

class udp_endpoint: public ipv4, /* implements */ virtual public udp_endpointI
{

	public:
		udp_endpoint();

		udp_endpoint(const udp_endpoint &other);
		udp_endpoint &operator=(const udp_endpoint &other);

		udp_endpoint(const udp_endpointI &other);
		udp_endpoint &operator=(const udp_endpointI &other);

		udp_endpoint(udp_endpoint &&other);
		udp_endpoint &operator=(udp_endpoint &&other);

		udp_endpoint(std::string ipAddr, uint16_t port);

		virtual ~udp_endpoint()=default;

		virtual void setPort(uint16_t port) override;
		virtual uint16_t getPort() const override;

	private:
		uint16_t port;


};
}
}
}
#endif

