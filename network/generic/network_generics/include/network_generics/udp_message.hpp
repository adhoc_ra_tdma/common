#ifndef __GENERIC_UDP_MESSAGE_HPP__
#define __GENERIC_UDP_MESSAGE_HPP__


#include <vector>
#include <string>
#include <cstdint>
#include <network_generics/message.hpp>
#include <network_interfaces/udp_messageI.hpp>
#include <network_generics/udp_endpoint.hpp>

namespace common
{
namespace network
{
namespace generics
{


using common::network::interfaces::MessageUDPI;
class MessageUDP: public Message, virtual public MessageUDPI
{
	public:

		using dType=MessageI::dType;
		using pType=MessageI::pType;

		MessageUDP();
		MessageUDP(const std::string &address, const std::uint16_t &port, const pType &data=pType(0));

		MessageUDP(const MessageUDP &other);
		MessageUDP &operator=(const MessageUDP &other);

		MessageUDP(MessageUDP &&other);
		MessageUDP &operator=(MessageUDP &&other);

		virtual ~MessageUDP()=default;

		virtual void setUDPEndpoint(const udp_endpointI &endpoint) override;
		virtual const udp_endpointI &getUDPEndpoint() const override;
		virtual udp_endpointI &getUDPEndpoint();




	protected:
		udp_endpoint ep;

};
}
}
}

#endif
