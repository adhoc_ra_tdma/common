#ifndef __GENERIC_MESSAGE_HPP__
#define __GENERIC_MESSAGE_HPP__


#include <cstdint>
#include <network_interfaces/messageI.hpp>

namespace common
{
namespace network
{
namespace generics
{


using MessageI=common::network::interfaces::MessageI;
class Message:virtual public MessageI
{
	public:

		using dType=MessageI::dType;
		using pType=MessageI::pType;


		Message();
		Message(const pType &data);

		Message(const Message &other);
		Message &operator=(const Message &other);

		Message(Message &&other);
		Message &operator=(Message &&other);

		virtual ~Message()=default;

		virtual void setData(pType data);

		virtual pType &getData();
		virtual const pType &getData() const;

	protected:
		pType data;
};

}
}
}

#endif
