#include <network_generics/ip_v4.hpp>

//#define debugLevel lDebug
#ifndef debugLevel
#define debugLevel lError
#endif
#include <debug_headers/debug.hpp>

#include <sstream>
namespace common
{
namespace network
{
namespace generics
{

ipv4::ipv4():
	ipV4(0)
{

}

ipv4::ipv4(const std::string &ipAddr)
{
	setAddr(ipAddr);
}

ipv4::ipv4(const std::array<uint8_t, 4> &ipAddr)
{
	setBytes(ipAddr);
}

ipv4::ipv4(const uint32_t &ipAddr)
{
	setInt(ipAddr);
}

void ipv4::setAddr(const std::string &ipAddr)
{
	PDEBUG(std::cout,"Setting IP address: "<<ipAddr);
	std::array<uint16_t, 4> addr;
	std::array<uint8_t, 4> addr_8b;
	char dot;
	std::stringstream ss;
	ss<<ipAddr;
	ss>>((uint16_t &)(addr[0]))>>dot>>((uint16_t &)(addr[1]))>>dot>>((uint16_t &)(addr[2]))>>dot>>((uint16_t &)(addr[3]));
	for(std::size_t it=0;it<addr.size();it++)
	{
		addr_8b[it]=addr[it];
	}
	setBytes(addr_8b);
}

std::string ipv4::getAddr() const
{
	std::array<uint8_t, 4> b=getBytes();
	std::stringstream ss;
	ss<<(uint16_t)(b[0])<<'.'<<(uint16_t)b[1]<<'.'<<(uint16_t)b[2]<<'.'<<(uint16_t)b[3];
	PDEBUG(std::cout,"Getting IP address: "<<ss.str());
	return ss.str();
}


std::array<uint8_t, 4> ipv4::getBytes() const
{
	std::array<uint8_t, 4> ret;

	uint32_t addr=ipV4;
	for(uint8_t &byte:ret)
	{
		byte=(addr&0xFF000000)>>24;
		addr<<=8;
		PDEBUG(std::cout,"Getting byte: "<<(uint16_t)byte);
	}
	PDEBUG(std::cout,"IP: "<<ipV4);
	return ret;
}

void ipv4::setBytes(const std::array<uint8_t, 4> &ipAddr)
{
	uint32_t addr=0;
	for(const uint8_t &byte:ipAddr)
	{
		addr<<=8;
		addr&=0xFFFFFF00;
		addr|=byte;
		PDEBUG(std::cout,"Setting byte: "<<(uint16_t)byte);
	}
	ipV4=addr;
	PDEBUG(std::cout,"IP: "<<ipV4);
}

void ipv4::setInt(const uint32_t &ipAddr)
{
	ipV4=ipAddr;
}

uint32_t ipv4::getInt() const
{
	return ipV4;
}




}
}
}
