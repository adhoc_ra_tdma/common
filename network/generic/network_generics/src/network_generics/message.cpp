#include <network_generics/message.hpp>
namespace common
{
namespace network
{
namespace generics
{



Message::Message():
	data(0)
{

}

Message::Message(const pType &data):
	data(data)
{

}

Message::Message(const Message &other):
	data(other.data)
{
}

Message &Message::operator=(const Message &other)
{
	if(this!=&other)
	{
		data=other.data;
	}
	return *this;

}

Message::Message(Message &&other):
	data(std::move(other.data))
{

}

Message &Message::operator=(Message &&other)
{
	if(this!=&other)
	{
		data=std::move(other.data);
	}
	return *this;
}

void Message::setData(pType data)
{
	this->data=data;
}

Message::pType &Message::getData()
{
	return data;
}

const Message::pType &Message::getData() const
{
	return data;
}




}
}
}
