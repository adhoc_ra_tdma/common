#include <network_generics/udp_message.hpp>
namespace common
{
namespace network
{
namespace generics
{

MessageUDP::MessageUDP()
{

}

MessageUDP::MessageUDP(const std::string &address, const std::uint16_t &port, const pType &data):
	Message(data),
	ep(address,port)
{

}

MessageUDP::MessageUDP(const MessageUDP &other):
	Message(other),
	ep(other.ep)
{
}

MessageUDP &MessageUDP::operator=(const MessageUDP &other)
{
	if(this!=&other)
	{
		ep.setPort(other.ep.getPort());
		ep.setAddr(other.ep.getAddr());
	}
	return *this;

}

MessageUDP::MessageUDP(MessageUDP &&other):
	Message(other),
	ep(other.ep)
{
}

MessageUDP &MessageUDP::operator=(MessageUDP &&other)
{
	if(this!=&other)
	{
		ep.setPort(other.ep.getPort());
		ep.setAddr(other.ep.getAddr());
	}
	return *this;

}

void MessageUDP::setUDPEndpoint(const interfaces::udp_endpointI &endpoint)
{
	ep.setAddr(endpoint.getAddr());
	ep.setPort(endpoint.getPort());
}

const udp_endpointI &MessageUDP::getUDPEndpoint() const
{
	return ep;
}

udp_endpointI &MessageUDP::getUDPEndpoint()
{
	return ep;
}





}
}
}
