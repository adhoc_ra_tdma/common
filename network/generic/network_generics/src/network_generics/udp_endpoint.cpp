#include <network_generics/udp_endpoint.hpp>


namespace common
{

namespace network
{

namespace generics
{

udp_endpoint::udp_endpoint():
	port(0)
{

}

udp_endpoint::udp_endpoint(const udp_endpoint &other):
	ipv4(other),
	port(other.port)
{

}

udp_endpoint &udp_endpoint::operator=(const udp_endpoint &other)
{
	if(this!=&other)
	{
		ipv4::operator=(other);
		port=other.port;
	}
	return *this;
}

udp_endpoint::udp_endpoint(const interfaces::udp_endpointI &other):
	port(other.getPort())
{
	ipv4::setInt(other.getInt());
}

udp_endpoint &udp_endpoint::operator=(const interfaces::udp_endpointI &other)
{
	if(this!=&other)
	{
		ipv4::setInt(other.getInt());
		port=other.getPort();
	}
	return *this;
}

udp_endpoint::udp_endpoint(udp_endpoint &&other):
	ipv4(std::move(other)),
	port(std::move(other.port))
{

}

udp_endpoint &udp_endpoint::operator=(udp_endpoint &&other)
{
	if(this!=&other)
	{
		ipv4::operator=(std::move(other));
		port=std::move(other.port);
	}
	return *this;
}

udp_endpoint::udp_endpoint(std::string ipAddr, uint16_t port):
	port(port)
{
	setAddr(ipAddr);
}

void udp_endpoint::setPort(uint16_t port)
{
	this->port=port;
}

uint16_t udp_endpoint::getPort() const
{
	return port;
}



}
}
}
