#ifndef __DYNAMIC_MAP_MATRIX_VECTOR_IPP__
#define __DYNAMIC_MAP_MATRIX_VECTOR_IPP__

#include <dynamic_map_matrix/vector.hpp>
#include <algorithm>


#ifdef debugLevel
#define WAS_DEFINED_DEBUG_LEVEL
#define PREV_DEBUG_LEVEL debugLevel
#undef debugLevel
#endif

//#define debugLevel lDebug
#ifndef debugLevel
#define debugLevel lError
#endif
#include <debug_headers/debug.hpp>


namespace common
{
namespace dynamic_map_matrix
{

#define TEMPLATE_SIG template <\
		typename _Key,\
		typename T,\
		typename _Compare,\
		typename _Alloc\
		>
#define CLASS_SIG map<_Key,T,_Compare,_Alloc>
/*
TEMPLATE_SIG
void CLASS_SIG::insert(key_type key, mapped_type value)
{
	super::insert(value_type(key,value));
}*/

TEMPLATE_SIG
typename CLASS_SIG::mapped_type &CLASS_SIG::operator()(const key_type &key)
{
	return super::at(key);
}

TEMPLATE_SIG
const typename CLASS_SIG::mapped_type &CLASS_SIG::operator()(const key_type &key) const
{
	return super::at(key);
}

TEMPLATE_SIG
std::string CLASS_SIG::toString() const
{
	std::stringstream output;

	output<<"Size: "<<size()<<std::endl;
	PDEBUG_VERBATIM(std::cout,"Size: "<<size()<<std::endl);

	if(size()==0)
	{
		return output.str();
	}

	std::size_t ordered_number=0;
	for(const value_type &pair:*this)
	{
		output<<ordered_number<<"\t";
		PDEBUG_VERBATIM(std::cout,ordered_number<<"\t");
		ordered_number++;

		key_type f=pair.first;
		output<<IF_CHAR_PRINT_NUMBER(f)<<"\t";
		PDEBUG_VERBATIM(std::cout,key<<"\t");

		output<<IF_CHAR_PRINT_NUMBER(pair.second)<<std::endl;
		PDEBUG_VERBATIM(std::cout,value<<std::endl);

	}

	PDEBUG_VERBATIM(std::cout,ret);
	return std::move(output.str());
}

TEMPLATE_SIG
std::list<typename CLASS_SIG::key_type> CLASS_SIG::getKeys() const
{
	std::list<key_type> keys;
	for(const value_type &pair:*this)
	{
		keys.push_back(pair.first);
	}
	return keys;
}

TEMPLATE_SIG
std::list<typename CLASS_SIG::mapped_type> CLASS_SIG::getValues() const
{
	std::list<mapped_type> values;
	for(const value_type &pair:*this)
	{
		values.push_back(pair.second);
	}
	return values;
}

TEMPLATE_SIG
bool CLASS_SIG::hasKey(key_type key) const
{
	if(!empty())
	{
		return (super::find(key)!=end());
	}
	return false;
}

TEMPLATE_SIG
bool CLASS_SIG::getKey(const mapped_type &value, key_type &position) const
{
	const_iterator it = std::find_if(begin(),end(),[value](const value_type pair)
	{
		return (pair.second==value);
	});

	if(it==end())
	{
		return false;
	}

	position = it->first;
	return true;
}

TEMPLATE_SIG
bool CLASS_SIG::getValue(const key_type &key, mapped_type &value) const
{
	const_iterator it = super::find(key);
	if( it == cend() )
	{
		return false;
	}
	value=it->second;
	return true;
}

TEMPLATE_SIG
bool CLASS_SIG::getOrder(const key_type &key, key_type &position) const
{
	const_iterator it = super::find(key);
	position=std::distance(cbegin(),it);
	return (it!=cend());
}
TEMPLATE_SIG
void CLASS_SIG::serialise(Container &ret) const
{
	key_type s=size();
	common::serialisation::serialise(ret,s);
	if(s!=0)
	{
		for(const value_type &pair:*this)
		{
			common::serialisation::serialise(ret, pair.first);
			common::serialisation::serialise(ret, pair.second);
		}
	}
}

TEMPLATE_SIG
void CLASS_SIG::deserialise(Container &ret)
{
	CLASS_SIG new_class;
	key_type s;
	common::serialisation::deserialise(ret,s);
	if(s!=0)
	{
		for(std::size_t i=0;i<s;i++)
		{
			key_type new_key;
			mapped_type new_value;

			common::serialisation::deserialise(ret, new_key);
			common::serialisation::deserialise(ret, new_value);
			new_class.insert(new_class.end(),value_type(new_key,new_value));
		}
	}
	operator =(std::move(new_class));
}



#undef TEMPLATE_SIG
#undef CLASS_SIG

}
}

// This is actually included!!! Remove what you did to debug header
#include <debug_headers/clear_debug.hpp>
#ifdef WAS_DEFINED_DEBUG_LEVEL
#define debugLevel PREV_DEBUG_LEVEL
#undef WAS_DEFINED_DEBUG_LEVEL
#undef PREV_DEBUG_LEVEL
#include <debug_headers/debug.hpp>
#endif



#endif
