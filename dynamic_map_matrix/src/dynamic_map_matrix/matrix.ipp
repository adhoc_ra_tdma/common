#ifndef __DYNAMIC_MAP_MATRIX_MATRIX_IPP__
#define __DYNAMIC_MAP_MATRIX_MATRIX_IPP__

#include <dynamic_map_matrix/matrix.hpp>

#include <algorithm>

#ifdef debugLevel
#define WAS_DEFINED_DEBUG_LEVEL
#define PREV_DEBUG_LEVEL debugLevel
#undef debugLevel
#endif

//#define debugLevel lDebug
#ifndef debugLevel
#define debugLevel lError
#endif
#include <debug_headers/debug.hpp>






namespace common
{
namespace dynamic_map_matrix
{


#define TEMPLATE_SIG template <\
	typename _Key,\
	typename T,\
	typename _Compare, \
	typename _Alloc \
	>
#define CLASS_SIG matrix<_Key,T,_Compare,_Alloc>

TEMPLATE_SIG
std::pair<std::size_t, std::size_t> CLASS_SIG::size() const
{
	std::pair<std::size_t, std::size_t> s;
	s.first=super::size();
	if(s.first==0)
	{
		s.second=0;
	}
	else
	{
		s.second=begin()->second.size();
	}

	return s;
}

TEMPLATE_SIG
typename CLASS_SIG::element_type &CLASS_SIG::operator()(key_type r_key, key_type c_key)
{
	return super::at(r_key)(c_key);
}

TEMPLATE_SIG
const typename CLASS_SIG::element_type &CLASS_SIG::operator()(key_type r_key, key_type c_key) const
{
	return super::at(r_key)(c_key);
}


/*TEMPLATE_SIG
const typename CLASS_SIG::column_type &CLASS_SIG::operator()(key_type r_key) const
{
	return at(r_key);
}*/

TEMPLATE_SIG
std::string CLASS_SIG::toString() const
{
	std::stringstream output;
	if(super::size()==0)
	{
		output<<super::size()<<"x"<<+0<<std::endl;
		PDEBUG_VERBATIM(std::cout,super::size()<<"x"<<+0<<std::endl);
		return output.str();
	}
	else
	{
		output<<super::size()<<"x"<<begin()->second.size()<<std::endl;
		PDEBUG_VERBATIM(std::cout,super::size()<<"x"<<begin()->second.size()<<std::endl);
	}
	std::list<key_type> col_keys=getColumnKeys();
	output<<"\t\t";
	PDEBUG_VERBATIM(std::cout,"\t\t");
	for(const key_type &key:col_keys)
	{
		output<<IF_CHAR_PRINT_NUMBER(key)<<"  ";
		PDEBUG_VERBATIM(std::cout,IF_CHAR_PRINT_NUMBER(key)<<"  ");
	}
	output<<std::endl;
	PDEBUG_VERBATIM(std::cout,std::endl);
	std::size_t ordered_number=0;
	for(const value_type &r_pair:*this)
	{
		output<<ordered_number<<"\t";
		PDEBUG_VERBATIM(std::cout,IF_CHAR_PRINT_NUMBER(ordered_number)<<"\t");
		key_type f=r_pair.first;
		output<<IF_CHAR_PRINT_NUMBER(f)<<"\t";
		PDEBUG_VERBATIM(std::cout,IF_CHAR_PRINT_NUMBER(r_pair.first)<<"\t");
		for(const typename row_type::value_type &c_pair:r_pair.second)
		{
			output<<IF_CHAR_PRINT_NUMBER(c_pair.second)<<"  ";
			PDEBUG_VERBATIM(std::cout,IF_CHAR_PRINT_NUMBER(c_pair.second)<<"  ");
		}
		output<<std::endl;
		PDEBUG_VERBATIM(std::cout,std::endl);
		ordered_number++;
	}
	std::string ret = std::move(output.str());
	PDEBUG_VERBATIM(std::cout,ret);
	return ret;
}



TEMPLATE_SIG
std::list<typename CLASS_SIG::key_type> CLASS_SIG::getRowKeys() const
{
	std::list<key_type> keys;
	for(const value_type &pair:*this)
	{
		keys.push_back(pair.first);
	}
	return keys;
}


TEMPLATE_SIG
std::list<typename CLASS_SIG::key_type> CLASS_SIG::getColumnKeys() const
{
	std::list<key_type> keys;
	for(const typename mapped_type::value_type &pair:begin()->second)
	{
		keys.push_back(pair.first);
	}
	return keys;
}

TEMPLATE_SIG
bool CLASS_SIG::hasRowKey(key_type r_key) const
{
	return super::hasKey(r_key);
}

TEMPLATE_SIG
bool CLASS_SIG::hasColumnKey(key_type c_key) const
{
	if(!super::empty())
	{
		row_type first_row=begin()->second;
		return first_row.hasKey(c_key);
	}
	return false;
}

TEMPLATE_SIG
bool CLASS_SIG::getRowOrder(key_type r_key, key_type &position) const
{
	return super::getOrder(r_key,position);
}

TEMPLATE_SIG
bool CLASS_SIG::getColumnOrder(key_type c_key, key_type &position) const
{
	if(!super::empty())
	{
		row_type &row=begin()->second;
		return row.getOrder(c_key, position);
	}
	return false;
}

TEMPLATE_SIG
void CLASS_SIG::eraseRow(key_type key)
{
	if(super::erase(key)<1)
	{
		throw std::out_of_range("Tried erase an inexistent column in matrix.");
	}
}


TEMPLATE_SIG
void CLASS_SIG::eraseCol(key_type key)
{
	for(value_type &pair:*this)
	{
		if(pair.second.erase(key)<1)
		{
			throw std::out_of_range("Tried erase an inexistent column in matrix.");
		}
	}
}

TEMPLATE_SIG
void CLASS_SIG::emplaceRow(key_type key, element_type value)
{
	row_type new_row;
	if(!super::empty())
	{
		row_type first_row=begin()->second;
		for(typename row_type::value_type &pair: first_row)
		{
			key_type c_key=pair.first;
			new_row.emplace(c_key,value);
		}
	}
	super::emplace(key,new_row);
}

TEMPLATE_SIG
void CLASS_SIG::emplaceCol(key_type key, element_type value)
{
	if(super::size()==0)
	{
		throw std::out_of_range("Tried to insert a column in a matrix without rows. You must first insert a row");
	}
	for(value_type &pair:*this)
	{
		pair.second.emplace(key,value);
	}
}

TEMPLATE_SIG
void CLASS_SIG::serialise(Container &ret) const
{
	key_type s=super::size();
	common::serialisation::serialise(ret,s);
	if(s!=0)
	{
		s=begin()->second.size();
		common::serialisation::serialise(ret,s);

		std::list<key_type> r_keys=getRowKeys();
		std::list<key_type> c_keys=getRowKeys();

		for(const key_type &k:r_keys)
		{
			common::serialisation::serialise(ret, k);
		}


		for(const key_type &k:c_keys)
		{
			common::serialisation::serialise(ret, k);
		}

		for(const value_type &r_pair:*this)
		{
			for(const typename row_type::value_type &c_pair:r_pair.second)
			{
				common::serialisation::serialise(ret, c_pair.second);
			}
		}
	}
}

TEMPLATE_SIG
void CLASS_SIG::deserialise(Container &ret)
{
	CLASS_SIG new_class;
	key_type nr;
	PDEBUG(std::cout,"Deserialising Matrix");
	common::serialisation::deserialise(ret,nr);
	PDEBUG(std::cout,"\tReceived "<< nr <<" rows");
	if(nr!=0)
	{
		key_type nc;
		common::serialisation::deserialise(ret,nc);

		std::list<key_type> r_keys;
		std::list<key_type> c_keys;
		for(std::size_t i=0;i<nr;i++)
		{
			key_type k;
			common::serialisation::deserialise(ret,k);
			r_keys.push_back(k);
		}

		for(std::size_t i=0;i<nc;i++)
		{
			key_type k;
			common::serialisation::deserialise(ret,k);
			c_keys.push_back(k);
		}

		PDEBUG(std::cout,"\tReceived "<< nr <<" columns");
		for(key_type &rk:r_keys)
		{
			row_type new_row;
			for(key_type &ck:c_keys)
			{
				element_type c_val;
				common::serialisation::deserialise(ret, c_val);

				new_row.emplace_hint(new_row.end(),ck, c_val);
			}

			new_class.emplace_hint(new_class.end(),rk,new_row);
		}
	}
	operator =(std::move(new_class));
}


#undef TEMPLATE_SIG
#undef CLASS_SIG


}
}

// This is actually included!!! Remove what you did to debug header
#include <debug_headers/clear_debug.hpp>
#ifdef WAS_DEFINED_DEBUG_LEVEL
#define debugLevel PREV_DEBUG_LEVEL
#undef WAS_DEFINED_DEBUG_LEVEL
#undef PREV_DEBUG_LEVEL
#include <debug_headers/debug.hpp>
#endif



#endif
