#ifndef __DYNAMIC_MAP_MATRIX_SQUARE_MATRIX_IPP__
#define __DYNAMIC_MAP_MATRIX_SQUARE_MATRIX_IPP__

#include <dynamic_map_matrix/square_matrix.hpp>


#ifdef debugLevel
#define WAS_DEFINED_DEBUG_LEVEL
#define PREV_DEBUG_LEVEL debugLevel
#undef debugLevel
#endif

//#define debugLevel lDebug
#ifndef debugLevel
#define debugLevel lError
#endif
#include <debug_headers/debug.hpp>


namespace common
{
namespace dynamic_map_matrix
{

#define TEMPLATE_SIG template <\
	typename _Key,\
	typename T,\
	typename _Compare, \
	typename _Alloc \
	>
#define CLASS_SIG SquareMatrix<_Key,T, _Compare, _Alloc>

TEMPLATE_SIG
std::size_t CLASS_SIG::size() const
{
	return super::size().first;
}


TEMPLATE_SIG
std::list<typename CLASS_SIG::key_type> CLASS_SIG::getKeys() const
{
	return super::getRowKeys();
}

TEMPLATE_SIG
bool CLASS_SIG::hasKey(key_type r_key) const
{
	return super::hasRowKey(r_key);
}

TEMPLATE_SIG
bool CLASS_SIG::getOrder(key_type r_key, key_type &position) const
{
	return super::getRowOrder(r_key, position);
}

TEMPLATE_SIG
void CLASS_SIG::erase(key_type key)
{
	super::eraseCol(key);
	super::eraseRow(key);
}

TEMPLATE_SIG
void CLASS_SIG::emplace(key_type key, element_type element)
{
	super::emplaceRow(key, element);
	super::emplaceCol(key, element);
}

TEMPLATE_SIG
void CLASS_SIG::serialise(Container &ret) const
{
	key_type s=size();
	common::serialisation::serialise(ret,s);
	if(s!=0)
	{
		std::list<key_type> keys=getKeys();

		for(const key_type &k:keys)
		{
			common::serialisation::serialise(ret, k);
		}
		for(const value_type &r_pair:*this)
		{
			for(const typename row_type::value_type &c_pair:r_pair.second)
			{
				common::serialisation::serialise(ret, c_pair.second);
			}
		}
	}
}

TEMPLATE_SIG
void CLASS_SIG::deserialise(Container &ret)
{
	CLASS_SIG new_class;
	key_type s;
	PINFO(std::cout,"Deserialising Square Matrix");
	common::serialisation::deserialise(ret,s);
	if(s!=0)
	{

		PDEBUG(std::cout,"\tReceived "<< (REMOVE_CHAR_TYPE(key_type))s <<" elements");
		std::list<key_type> keys;
		for(std::size_t i=0;i<s;i++)
		{
			key_type k;
			common::serialisation::deserialise(ret,k);
			keys.push_back(k);
		}


		for(const key_type &rk:keys)
		{

			mapped_type new_column;
			for(const key_type &ck:keys)
			{
				element_type value;
				common::serialisation::deserialise(ret, value);

				PDEBUG(std::cout,"\t\tElement ("<< (REMOVE_CHAR_TYPE(key))rk<<","<<(REMOVE_CHAR_TYPE(key))ck <<") = "<<(REMOVE_CHAR_TYPE(column_value))c_val);
				new_column.emplace_hint(new_column.end(),ck,value);
			}
			new_class.emplace_hint(new_class.end(),rk,new_column);
		}
	}
	operator =(std::move(new_class));
}

#undef TEMPLATE_SIG
#undef CLASS_SIG

}
}

// This is actually included!!! Remove what you did to debug header
#include <debug_headers/clear_debug.hpp>
#ifdef WAS_DEFINED_DEBUG_LEVEL
#define debugLevel PREV_DEBUG_LEVEL
#undef WAS_DEFINED_DEBUG_LEVEL
#undef PREV_DEBUG_LEVEL
#include <debug_headers/debug.hpp>
#endif




#endif
