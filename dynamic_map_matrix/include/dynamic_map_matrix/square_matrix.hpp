#ifndef __DYNAMIC_MAP_MATRIX_SQUARE_MATRIX_HPP__
#define __DYNAMIC_MAP_MATRIX_SQUARE_MATRIX_HPP__

#include <dynamic_map_matrix/matrix.hpp>

#include <map>
#include <list>
#include <vector>
#include <serialisation/serialisationTools.hpp>
#include <stdexcept>
#include <sstream>


namespace common
{
namespace dynamic_map_matrix
{


template <
		typename _Key,
		typename T,
		typename _Compare = std::less<_Key>,
		typename _Alloc = std::allocator< std::pair<const _Key, T> >
		>
class SquareMatrix:
		/* extends */
		private matrix<_Key,T, _Compare, _Alloc>,
		/* implements */
		virtual public common::serialisation::SerialisableI

{
		using Container=common::serialisation::Container;
		using super=matrix<_Key,T,_Compare,_Alloc>;
	public:


		static_assert(std::is_integral<_Key>::value && std::is_unsigned<_Key>::value ,"In Matrix: size_type must be an unsigned integral type.");
		static_assert(std::is_default_constructible<T>::value,"In Matrix: data type (T) must be default constructible.");

		using row_type=typename super::row_type;
		using element_type=typename super::element_type;

		///////////////////////////////////////////////////////
		///		Export selected super methods and types		///
		///////////////////////////////////////////////////////

		using key_type=typename super::key_type;
		using mapped_type=typename super::mapped_type;
		using value_type=typename super::value_type;

		using pointer=typename super::pointer;
		using const_pointer=typename super::const_pointer;
		using reference=typename super::reference;
		using const_reference=typename super::const_reference;
		using iterator=typename super::iterator;
		using const_iterator=typename super::const_iterator;
		using size_type=typename super::size_type;
		using difference_type=typename super::difference_type;
		using reverse_iterator=typename super::reverse_iterator;
		using const_reverse_iterator=typename super::const_reverse_iterator;

		using super::begin;
		using super::end;
		using super::rbegin;
		using super::rend;
		using super::cbegin;
		using super::cend;
		using super::crbegin;
		using super::crend;

		//using super::empty;
		//using super::size;
		//using super::max_size;

		// using super::at; -> Use operator() instead
		// using super::operator []; -> dont want implicit insertion

		//using super::insert;
		//using super::erase;
		using super::swap;
		using super::clear;
		//using super::emplace;
		//using super::emplace_hint;

		//using super::key_comp;
		//using super::value_comp;

		//using super::find; -> using get functions
		//using super::count; -> using hasKey
		//using super::lower_bound; -> using getOrder
		//using super::upper_bound;
		//using super::equal_range;

		//using super::get_allocator;
		///////////////////////////////////////////////////////
		///////////////////////////////////////////////////////
		///////////////////////////////////////////////////////

	public:
		SquareMatrix()=default;

		SquareMatrix(const SquareMatrix &other)=default;
		SquareMatrix &operator=(const SquareMatrix &other)=default;

		SquareMatrix(SquareMatrix &&other)=default;
		SquareMatrix &operator=(SquareMatrix &&other)=default;

		virtual ~SquareMatrix()=default;

		std::size_t size() const;


		using super::operator();

		using super::toString;


		std::list<key_type> getKeys() const;

		bool hasKey(key_type r_key) const;

		bool getOrder(key_type r_key, key_type &position) const;

		void erase(key_type key);

		void emplace(key_type key, element_type element);

		virtual void serialise(Container &ret) const override;
		virtual void deserialise(Container &ret) override;

};

}
}


#include "../../src/dynamic_map_matrix/square_matrix.ipp"
#endif // CONNECTIVITYMATRIX_HPP
