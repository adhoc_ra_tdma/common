#ifndef __DYNAMIC_MAP_MATRIX_MATRIX_HPP__
#define __DYNAMIC_MAP_MATRIX_MATRIX_HPP__


#include <map>
#include <list>
#include <vector>
#include <serialisation/serialisationTools.hpp>
#include <stdexcept>
#include <sstream>

#include <dynamic_map_matrix/vector.hpp>


namespace common
{
namespace dynamic_map_matrix
{

template <
		typename _Key,
		typename T,
		typename _Compare = std::less<_Key>,
		typename _Alloc = std::allocator< std::pair<const _Key, T> >
		>

class matrix:
		/* extends */
		protected map<_Key, map<_Key,T,_Compare,_Alloc>, _Compare, _Alloc >,
		/* imlpements */
		virtual public common::serialisation::SerialisableI
{
		using Container=common::serialisation::Container;
		using super=map<_Key, map<_Key,T,_Compare,_Alloc>, _Compare, _Alloc >;
	public:


		static_assert(std::is_integral<_Key>::value && std::is_unsigned<_Key>::value ,"In Matrix: size_type must be an unsigned integral type.");
		static_assert(std::is_default_constructible<T>::value,"In Matrix: data type (T) must be default constructible.");

		using row_type=typename super::mapped_type;
		using element_type=typename row_type::mapped_type;

		///////////////////////////////////////////////////////
		///		Export selected super methods and types		///
		///////////////////////////////////////////////////////

		using key_type=typename super::key_type;
		using mapped_type=typename super::mapped_type;
		using value_type=typename super::value_type;

		using pointer=typename super::pointer;
		using const_pointer=typename super::const_pointer;
		using reference=typename super::reference;
		using const_reference=typename super::const_reference;
		using iterator=typename super::iterator;
		using const_iterator=typename super::const_iterator;
		using size_type=typename super::size_type;
		using difference_type=typename super::difference_type;
		using reverse_iterator=typename super::reverse_iterator;
		using const_reverse_iterator=typename super::const_reverse_iterator;

		using super::begin;
		using super::end;
		using super::rbegin;
		using super::rend;
		using super::cbegin;
		using super::cend;
		using super::crbegin;
		using super::crend;

		//using super::empty;
		//using super::size;
		//using super::max_size;

		// using super::at; -> Use operator() instead
		// using super::operator []; -> dont want implicit insertion

		//using super::insert;
		//using super::erase;
		using super::swap;
		using super::clear;
		//using super::emplace;
		//using super::emplace_hint;

		//using super::key_comp;
		//using super::value_comp;

		//using super::find; -> using get functions
		//using super::count; -> using hasKey
		//using super::lower_bound; -> using getOrder
		//using super::upper_bound;
		//using super::equal_range;

		//using super::get_allocator;
		///////////////////////////////////////////////////////
		///////////////////////////////////////////////////////
		///////////////////////////////////////////////////////


		matrix()=default;

		matrix(const matrix &other)=default;
		matrix &operator=(const matrix &other)=default;

		matrix(matrix &&other)=default;
		matrix &operator=(matrix &&other)=default;

		virtual ~matrix()=default;

		std::pair<std::size_t,std::size_t> size() const;

		element_type &operator() (key_type r_key, key_type c_key);
		const element_type &operator() (key_type r_key, key_type c_key) const;


		std::string toString() const;



		std::list<key_type> getRowKeys() const;
		std::list<key_type> getColumnKeys() const;

		bool hasRowKey(key_type r_key) const;
		bool hasColumnKey(key_type c_key) const;

		bool getRowOrder(key_type r_key, key_type &position) const;
		bool getColumnOrder(key_type r_key, key_type &position) const;

		void eraseRow(key_type key);
		void eraseCol(key_type key);

		void emplaceRow(key_type key, element_type value);
		void emplaceCol(key_type key, element_type value);


		virtual void serialise(Container &ret) const override;
		virtual void deserialise(Container &ret) override;




	protected:

	private:

};

}
}


#include "../../src/dynamic_map_matrix/matrix.ipp"
#endif // CONNECTIVITYMATRIX_HPP
